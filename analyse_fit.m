function varargout = analyse_fit(varargin)

% Last Modified by GUIDE v2.5 31-May-2012 18:38:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @analyse_fit_OpeningFcn, ...
                   'gui_OutputFcn',  @analyse_fit_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before analyse_fit is made visible.
function analyse_fit_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
DNA_analysis=findobj('Tag','DNA_analysis'); 

if isempty(DNA_analysis)
    [fn,fp] = uigetfile('*.mat');

    load([fp fn]);
    anfit=findobj('tag', 'analyse_fit');
    anfit_data.gaussians=gaussians;
    anfit_data.selected=selected;
    set(anfit,'Userdata',anfit_data);
end
    

    

% --- Outputs from this function are returned to the command line.
function varargout = analyse_fit_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


%gaussian=[frame, x, y, width x, width y, amp, back, total intensity, max of residual, mean of residual, std of residual];
%     1      2  3     4       5        6   7              8           9               10             11
%1 Intensity
%2 mean gaussian residual
%3 max of residual
%4 Gaussian residual std
%5 total intensity
%6 back
%7 Width x+y
%8 WidthX/Width Y
function drawimage(handles)
anfit=findobj('tag', 'analyse_fit');
data_anfit=get(anfit, 'Userdata');


gaussians=data_anfit.gaussians;
selected=data_anfit.selected;

hint=[];
haxes2=findobj(anfit, 'type', 'axes');
haxes2=findobj(haxes2, 'tag', 'axes_histo');
axes(haxes2);cla
%ed=findobj(anfit,'tag','edit_bins');
nrbins=str2num(get(findobj(anfit,'tag','edit_bins'),'String'));
X=get(findobj(anfit,'tag','popup_histo'),'Value');
mr=get(findobj(anfit,'tag','popup_histo'),'String');
if X==1; Y=6; elseif X==2; Y=10; elseif X==3; Y=9; elseif X==4; Y=11; elseif X==5; Y=8; elseif X==6; Y=7; elseif X==7 || X==8; Y=[4 5]; end
if size(selected,1)>0
    molecules=selected;Y=Y+1;
else
    molecules=gaussians;
end

if X<=6
    [hint(:,2),hint(:,1)]=hist(molecules(:,Y),nrbins);
    set(handles.edit_up,'string',num2str(max(molecules(:,Y))));
    set(handles.edit_low,'string',num2str(min(molecules(:,Y))));
elseif X==7
    [hint(:,2),hint(:,1)]=hist([molecules(:,Y(1));molecules(:,Y(2))],nrbins);
elseif X==8
    [hint(:,2),hint(:,1)]=hist([molecules(:,Y(1))./molecules(:,Y(2))],nrbins);    
end

title(['Distribution of' mr(X)],'fontsize',12, 'Fontname', 'candara');
bar(hint(:,1),hint(:,2));
set(haxes2, 'tag', 'axes_histo');
    


function bottom(value)
DNA_analysis=findobj('Tag','DNA_analysis');
set(findobj(DNA_analysis,'tag','push_delete_2'),'visible',value);
set(findobj(DNA_analysis,'tag','axes9'),'visible',value);
set(findobj(DNA_analysis,'tag','pushbutton_analyse'),'visible',value);
set(findobj(DNA_analysis,'tag','push_delete_2'),'visible',value);
    




% --- Executes on selection change in popup_histo.
function popup_histo_Callback(hObject, eventdata, handles)
drawimage(handles)



function edit_bins_Callback(hObject, eventdata, handles)
drawimage(handles)


function edit_low_Callback(hObject, eventdata, handles)
anfit=findobj('tag', 'analyse_fit');
data_anfit=get(anfit, 'Userdata');


DNA_analysis=findobj('Tag','DNA_analysis');

if DNA_analysis>0
    data=DNA_analysis_load_selected_data();
    data.gaussians=data_anfit.gaussians;
    data.selected=data_anfit.selected;
    DNA_analysis_save_selected_data(data)
    DNA_analysis_drawimage
end

function edit_up_Callback(hObject, eventdata, handles)
pushbutton_lim_Callback

% --- Executes on button press in pushbutton_lim.
function pushbutton_lim_Callback(hObject, eventdata, handles)
anfit=findobj('tag', 'analyse_fit');
data_anfit=get(anfit, 'Userdata');

gaussians=data_anfit.gaussians;
selected=data_anfit.selected;

%limit gaussians (or selected)
up=str2num(get(handles.edit_up,'string'));
low=str2num(get(handles.edit_low,'string'));
X=get(handles.popup_histo,'Value');
if X==1; Y=6; elseif X==2; Y=10; elseif X==3; Y=9; elseif X==4; Y=11; elseif X==5; Y=8; elseif X==6; Y=7; else msgbox('please choose another section'); end

%gaussians=gaussians(gaussians(:,Y)>=low,:);
%gaussians=gaussians(gaussians(:,Y)<=up,:);
if selected~=0
    selected=selected(selected(:,Y+1)>=low,:);
    selected=selected(selected(:,Y+1)<=up,:);
else
    gaussians=gaussians(gaussians(:,Y)>=low,:);
    gaussians=gaussians(gaussians(:,Y)<=up,:);
end
data_anfit.gaussians=gaussians;
data_anfit.selected=selected;

%store data in anfit
set(anfit, 'Userdata',data_anfit);


DNA_analysis=findobj('Tag','DNA_analysis');

if DNA_analysis>0
    data=DNA_analysis_load_selected_data();
    data.gaussians=data_anfit.gaussians;
    data.selected=data_anfit.selected;
    DNA_analysis_save_selected_data(data)
    DNA_analysis_drawimage
end
