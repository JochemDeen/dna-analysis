function updategui(cases)

switch cases
    case 'updatelist'
        updatelist();
    case 'loadfiles'
        loadfiles();
    case 'radio_scan'
        radio_scan();
    case 'zoomimage'
        zoom_image();
    case 'updatelistMlocation'
        updatelistMlocation();
    case 'showcontrols'
        showcontrols();
        
end

%Cases
%-----------------------------------------------------------------------------------------------------------------
function updatelist
DNA_analysis=findobj('Tag', 'DNA_analysis');
listbox2=findobj(DNA_analysis,'tag','listbox2');
data=get(listbox2, 'UserData');
listbox=get(listbox2,'string');

%if a new file is selected
if data.newfilename>0

    %increase the size of the listbox by 1
    i=size(listbox,1)+1;
    
    %add the new filename to the list of filenames
    data.list(i).fn=data.newfilename;
    data.list(i).fp=data.newfilepath;
    
    %empty the strings
    data.newfilename=[];
    data.newfilepath=[];
    
    %save the userdata
    set(listbox2, 'Userdata',data);
    
    %create the string
    t2={};
    for x=1:size(data.list,2)
      t2=[t2;data.list(x).fn];
    end
    
    %update the gui
    set(listbox2,'string',t2);
    set(listbox2,'value',i);
       
    DNA_analysis_drawimage
end

function radio_scan()
DNA_analysis=findobj('Tag','DNA_analysis');
on=get(findobj(DNA_analysis,'tag','radio_scan'),'value');

if on==1
    enable_scan_buttons('on')
    
    data=DNA_analysis_load_selected_data();
    scanframes=round(str2double(get(findobj(DNA_analysis,'tag','edit_scanframes'),'string')));
    
    %if no value is set, set a value for scan_frames (the number of frames that belong to a single scan
    if isnan(scanframes)
        if isfield(data.flt,'scan') %check if there is a field scan in the flt, and load that value
            scanframes=data.flt.scan;
        else %otherwise use 1
            scanframes=1;
        end
        set(findobj(DNA_analysis,'tag','edit_scanframes'),'string',num2str(scanframes));
    end

    %load the total number of frames to update the slider
    if isfield(data.flt,'frames')
        frames=data.flt.frames;
    else
        [~,frames]=readCCDim([data.CCDpath data.CCDname], 1,1);
    end
    set(findobj(DNA_analysis,'tag','slider_scan'),'sliderstep',[scanframes/frames 10*scanframes/frames])
    set(findobj(DNA_analysis,'tag','slider_scan'),'value',(scanframes/frames));
    set(findobj(DNA_analysis,'tag','edit_scan'),'string','1');
    
else
    enable_scan_buttons('off')

    set(findobj(DNA_analysis,'tag','edit_scanframes'),'string','1');
end

function zoom_image()

DNA_analysis=findobj('Tag','DNA_analysis');
axes8=findobj(DNA_analysis,'tag','axes8');
imagedata=get(axes8, 'UserData');

[gx1, gy1, op]=ginput(1);
s=imagedata.imsize; % 1Y(1) 2Y(2) 3X(1) 4X(2)
if isfield(imagedata,'zoom') && sum(imagedata.zoom)>1
    extra=imagedata.zoom;
    s=imagedata.zoom;
else
    extra=[0 0 0 0];
end

if op<2
    %do the clicks not exceed the imageframe
    [gx1,gy1]=check_locations(gx1,gy1,s);
    
    axes(axes8);
    line([gx1 gx1], [1 s(2)-s(1)], 'Color','r');
    line([1 s(4)-s(3)], [gy1 gy1], 'Color','r');
    hold on;
    [gx2, gy2, op]=ginput(1);
    if op<2 &gx1~=gx2 &gy1~=gy2
        %do the clicks not exceed the imageframe?
        [gx2,gy2]=check_locations(gx2,gy2,s);
        
        line([gx2 gx2], [1 s(2)-s(1)], 'Color','r');
        line([1 s(4)-s(3)], [gy2 gy2], 'Color','r');
        
        if gx1<gx2; x=[gx1 gx2];else x=[gx2, gx1];end
        if gy1<gy2; y=[gy1 gy2];else y=[gy2, gy1];end
    end
    
    %if there was already a zoom, zoom further
    imagedata.zoom(1)=y(1)+extra(1);imagedata.zoom(2)=y(2)+extra(1);
    imagedata.zoom(3)=x(1)+extra(3);imagedata.zoom(4)=x(2)+extra(3);
    imagedata.zoom=floor(imagedata.zoom);
end
set(axes8, 'UserData',imagedata);

function updatelistMlocation()
DNA_analysis=findobj('Tag', 'DNA_analysis');
listboxMlocation=findobj(DNA_analysis,'Tag', 'listboxMlocation');
Mlocations_Userdata=get(listboxMlocation,'Userdata');

listbox_string=get(listboxMlocation,'string');
if ~isempty(Mlocations_Userdata.newMlocations)
    %get current data
    data=DNA_analysis_load_selected_data();
    selected_locations=data.selected;

    Loc.selected_locations=selected_locations;
    Loc.Mlocations=Mlocations_Userdata.newMlocations;

    if isfield(Mlocations_Userdata,'Mloc')
        i=size(Mlocations_Userdata.Mloc,2)+1;
    else
        i=1;
    end
    %i=size(Mlocations_Userdata,1)+1;
    %if i<=1 || size(Mlocations_Userdata.Mloc(i-1).Mlocation,1)~=size(Mlocation,1) || (size(Mlocations_Userdata.Mloc(i-1).Mlocation,1)==size(Mlocation,1) &&  sum(sum(Mlocations_Userdata.Mloc(i-1).Mlocation-Mlocation))~=0)
            Mlocations_Userdata.Mloc(i)=Loc;
            Mlocations_Userdata.newMlocations=[];
            set(listboxMlocation, 'Userdata',Mlocations_Userdata);
            t2={};
            for x=1:size(Mlocations_Userdata.Mloc,2)
              t2=[t2;['DNA with: ' num2str(size(Mlocations_Userdata.Mloc(x).Mlocations,1)) ' labels']];
            end
            set(listboxMlocation,'string',t2);
            set(listboxMlocation,'value',i);
    %end
else
    i=get(listboxMlocation,'value');
    if i>size(Mlocations_Userdata.Mloc,2);i=size(Mlocations_Userdata.Mloc,2);end
    t2={};
    for x=1:size(Mlocations_Userdata.Mloc,2)
      t2=[t2;['DNA with: ' num2str(size(Mlocations_Userdata.Mloc(x).Mlocations,1)) ' labels']];
    end
    set(listboxMlocation,'string',t2);
    if i>0
        set(listboxMlocation,'value',i);
    end
end
    
        

function showcontrols()
DNA_analysis=findobj('Tag','DNA_analysis');
radiobutton3=findobj(DNA_analysis,'tag','radiobutton3');
radiobutton_analyse=findobj(DNA_analysis,'tag','radiobutton_analyse');

if get(radiobutton3,'value')==1
    enable_buttons('on');
    if get(radiobutton_analyse,'value')
        set(findobj(DNA_analysis,'tag','uipanel_fitting'),'visible','on')
    end
    %set(findobj(DNA_analysis,'tag','tag'),'visible','on')
    %set(findobj(DNA_analysis,'tag','tag'),'visible','on')
else
    enable_buttons('off');
    set(findobj(DNA_analysis,'tag','uipanel_fitting'),'visible','off')
    %set(findobj(DNA_analysis,'tag','tag'),'visible','off')
    %set(findobj(DNA_analysis,'tag','tag'),'visible','off')
end 


%Helper functions
%-----------------------------------------------------------------------------------------------------------------
function enable_scan_buttons(value)
DNA_analysis=findobj('Tag','DNA_analysis');
set(findobj(DNA_analysis,'tag','edit_scanframes'),'enable',value)
set(findobj(DNA_analysis,'tag','text_scan'),'enable',value)
set(findobj(DNA_analysis,'tag','edit_scan'),'visible',value)
%set(findobj(DNA_analysis,'tag','tag'),'visible','off')
%set(findobj(DNA_analysis,'tag','tag'),'visible','off')
set(findobj(DNA_analysis,'tag','slider_scan'),'visible',value)

function enable_buttons(value)
DNA_analysis=findobj('Tag','DNA_analysis');
set(findobj(DNA_analysis,'tag','radio_black'),'visible',value)
set(findobj(DNA_analysis,'tag','radio_white'),'visible',value)
set(findobj(DNA_analysis,'tag','pushbutton_fitdata'),'visible',value)
set(findobj(DNA_analysis,'tag','push_remain'),'visible',value)
set(findobj(DNA_analysis,'tag','push_consolidate'),'visible',value)
set(findobj(DNA_analysis,'tag','edit_consolidate'),'visible',value)
set(findobj(DNA_analysis,'tag','radio_first'),'visible',value)
set(findobj(DNA_analysis,'tag','uipanel_scale'),'visible',value)
set(findobj(DNA_analysis,'tag','text3'),'visible',value)
set(findobj(DNA_analysis,'tag','edit_size'),'visible',value)
set(findobj(DNA_analysis,'tag','edit_markersize'),'visible',value)
set(findobj(DNA_analysis,'tag','text4'),'visible',value)
set(findobj(DNA_analysis,'tag','edit_frames'),'visible',value)
set(findobj(DNA_analysis,'tag','uipanel3'),'visible',value)
set(findobj(DNA_analysis,'tag','edit_3d'),'visible',value)
set(findobj(DNA_analysis,'tag','radiobutton_analyse'),'visible',value)

function [gx,gy]=check_locations(gx,gy,s)
if gx+s(3)-1<s(3); gx=s(3); %gx less than 1?
elseif gx+s(3)-1>s(4); gx=s(4); %gx more than x-size?
elseif gy+s(1)-1<s(1); gy=s(1); %gy less than 1?
elseif gy+s(1)-1>s(2); gy=s(2);  %gy more than y-size?
end
