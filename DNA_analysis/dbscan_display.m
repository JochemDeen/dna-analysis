function varargout = dbscan_display(varargin)
% DBSCAN_DISPLAY MATLAB code for dbscan_display.fig
%      DBSCAN_DISPLAY, by itself, creates a new DBSCAN_DISPLAY or raises the existing
%      singleton*.
%
%      H = DBSCAN_DISPLAY returns the handle to a new DBSCAN_DISPLAY or the handle to
%      the existing singleton*.
%
%      DBSCAN_DISPLAY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DBSCAN_DISPLAY.M with the given input arguments.
%
%      DBSCAN_DISPLAY('Property','Value',...) creates a new DBSCAN_DISPLAY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dbscan_display_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dbscan_display_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dbscan_display

% Last Modified by GUIDE v2.5 26-Aug-2015 14:31:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dbscan_display_OpeningFcn, ...
                   'gui_OutputFcn',  @dbscan_display_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dbscan_display is made visible.
function dbscan_display_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;

guidata(hObject, handles);

[fn,fp] = uigetfile({'*.mat'},'load matlab file');

load([fp fn])
dbscan_display=findobj('tag','dbscan_display');
set(dbscan_display,'userdata',Lox);


function varargout = dbscan_display_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


function pushbutton_DBscan_Callback(hObject, eventdata, handles)
dbscan_display=findobj('tag','dbscan_display');
data=get(dbscan_display,'userdata');
% 
% figure(1);
% cla;
% plot(data(:,1),data(:,2),'rx','MarkerSize',2);
% axis([0 512 0 512])
% 
% edit_density=findobj(dbscan_display,'tag','edit_density');
% k=str2double(get(edit_density,'String'));
% 
% edit_distance=findobj(dbscan_display,'tag','edit_distance');
% Eps=str2double(get(edit_distance,'String'));
% 
% [class,type]=dbscan(data,k,Eps);
% 
% %Clusters=data(class>0,:);
% 
% figure(1)
% hold on
% angle=zeros(max(class),1);
% r_full=zeros(max(class),1);
% 
% for x=1:max(class)
%     Cluster_i=data(class==x,:);
%     miny=min(Cluster_i(:,2));
%     minx=min(Cluster_i(:,1));
%     maxy=max(Cluster_i(:,2));
%     maxx=max(Cluster_i(:,1));
%     rectangle('Position',[minx,miny,maxx-minx,maxy-miny]) 
%     plot(Cluster_i(:,1),Cluster_i(:,2),'yo','MarkerSize',3);
%     
%     %tic
%     iterNum = 5;
%     thDist = 2;
%     thInlrRatio = .1;
%     [t,r] = ransac(Cluster_i',iterNum,thDist,thInlrRatio);
%     %toc
%     angle(x)=t;
%     r_full(x)=r;
%     
%     k1 = -tan(t);
%     b1 = r/cos(t);
% 
%     X=minx:0.1:maxx;
%     Y=k1*X+b1;
%     L=(Y>miny).* (Y<maxy);
%     X=X(L==1);
%     Y=Y(L==1);
%     plot(X,Y,'k')
% end
% hold off
% 
% figure(2);hist(angle,40);

tic
max_angle=detect_angle_molecule(data);
toc
% med_angle=median(angle)
% new_class=class;
% new_class((med_angle(new_class)<(med_angle-10)) || (med_angle(new_class)>(med_angle+10)),:)=-2;

figure(1)
cla
plot(data(:,1),data(:,2),'rx','MarkerSize',2);
axis([0 512 0 512])
hold on
%line([512/2 ((512/2)+50*cos(max_angle-pi/2))], [512/2 ((512/2)+50*sin(max_angle-pi/2))],'color','black');
hold off

R=[cos(max_angle), -sin(max_angle);sin(max_angle),cos(max_angle)];
rotatedata=data*R;

rotatedata(:,1)=rotatedata(:,1)*10;
LRangle=max_angle;
% Clusters=data(class>0,:);
% 
% coeff=pca(Clusters);

expand=10;

y_axis_expand=sin(LRangle)*expand+cos(LRangle)*1;
x_axis_expand=cos(LRangle)*expand+sin(LRangle)*1;

newdata=data;
newdata(:,1)=newdata(:,1)*x_axis_expand;
newdata(:,2)=newdata(:,2)*y_axis_expand;

%%% STEP 2

edit_density2=findobj(dbscan_display,'tag','edit_density2');
k2=str2double(get(edit_density2,'String'));

edit_distance2=findobj(dbscan_display,'tag','edit_distance2');
Eps2=str2double(get(edit_distance2,'String'));

figure(3)
cla
plot(data(:,1),data(:,2),'rx','MarkerSize',2);
axis([0 512 0 512])
% axis([min(rotatedata(:,1)) max(rotatedata(:,1)) min(rotatedata(:,2)) max(rotatedata(:,2))])
hold on
tic
[newclass,newtype]=dbscan(rotatedata,k2,Eps2);
toc
theta=zeros(max(newclass),1);
R=zeros(max(newclass),1);

tic
for x=1:max(newclass)
    Cluster_i=data(newclass==x,:);
    miny=min(Cluster_i(:,2));
    minx=min(Cluster_i(:,1));
    maxy=max(Cluster_i(:,2));
    maxx=max(Cluster_i(:,1));
    %rectangle('Position',[minx,miny,maxx-minx,maxy-miny]) 
    %plot(Cluster_i(:,1),Cluster_i(:,2),'go','MarkerSize',3);
    
    %disp(x)
    %tic
    iterNum = 10;
    thDist = 2;
    thInlrRatio = .1;
    [t,r] = ransac(Cluster_i',iterNum,thDist,thInlrRatio);
    %toc
    theta(x)=t;
    R(x)=r;
    
    k1 = -tan(t);
    b1 = r/cos(t);

    X=minx:0.1:maxx;
    Y=k1*X+b1;
    L=(Y>miny).* (Y<maxy);
    X=X(L==1);
    Y=Y(L==1);
    plot(X,Y,'k')
end
toc
hold off
figure(4);hist(theta,40);

median_theta=median(theta);

molecules=[];
figure(5)
cla
hold on

edit_minlength=findobj(dbscan_display,'tag','edit_minlength');
dist=str2double(get(edit_minlength,'String'));

edit_dangle=findobj(dbscan_display,'tag','edit_dangle');
dangle=str2double(get(edit_dangle,'String'));

min_distance=1.5;
min_molecule_length=dist;
angle_deviation=dangle/180*pi;

y=1;
for i=1:max(newclass)
    %select cluster
    cluster=data(newclass==i,:);
    
    %y=rico*x+offset
    rico = -tan(theta(i));
    offset = R(i)/cos(theta(i));
    
    %A.X+B.Y+C=0
    A=-rico;
    B=1;
    C=-offset;
    
    %en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    distance=abs(A*cluster(:,1)+B*cluster(:,2)+C)/sqrt(A^2+B^2);
    
    %eliminate spots further than a minimal distance from the line
    distance_bool=distance<min_distance;
    cluster=cluster(distance_bool,:);
    
    %Calculate length of molecule
    minx=min(cluster(:,1));
    maxx=max(cluster(:,1));
    miny=min(cluster(:,2));
    maxy=max(cluster(:,2));
    
    length=sqrt((maxx-minx)^2+(maxy-miny)^2);
    
    %include molecule if longer than minimal length and smaller deviation angle deviation
    if length>min_molecule_length && (theta(i)>median_theta-angle_deviation && theta(i)<median_theta+angle_deviation)
        molecules(y).cluster=cluster;
        plot(cluster(:,1),cluster(:,2),'mx','MarkerSize',2);

        y=y+1;
        
        X=minx:0.1:maxx;
        Y=rico*X+offset;
        L=(Y>miny).* (Y<maxy);
        X=X(L==1);
        Y=Y(L==1);
        plot(X,Y,'k')
    end
end
axis([0 512 0 512])

disp(y-1)
hold off


