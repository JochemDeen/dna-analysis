function histogram_sizes_DNAanalysis()
fitter=findobj('tag','fitter');
haxes=findobj(fitter,'tag','axes1');

%enable select button
pushbutton_select=findobj(fitter,'tag','pushbutton_select');
set(pushbutton_select,'enable','on');
%bins= newid('binsize (default=40)','image size',1,{'40'}); %inputdlg

%input dialog asking for the bin_size
bins= inputdlg('binsize (default=40)','image size',1,{'40'}); %inputdlg
binsize=str2double(bins);

tic
%Load userdata
Data=get(fitter,'Userdata');
pixelsize=Data(1).flt.pixelsize;

n=0;
for i=1:size(Data,2)
    locs=Data(i).selected(:,3:4);
    angle(i)=sqrt((360*atan((locs(1,1)-locs(1,2))/(locs(2,1)-locs(2,2)))/(2*pi))^2);
    
    
    a=Data(i).Dataation*pixelsize;
    b=ones(1,size(a,1));
    c=tril(a*b-b'*a');
    d=reshape(c,1,[]);
    d(d==0)=[];
    if size(a,1)==3 %1==1 % %angle(i)>40 && angle(i)<50   &&
        n=n+1;
        %Matrix(n,1:size(d,2))=d;
        Matrix(n,1:size(d,2))=c(3,:)';
    else
        %angle(i)=0;
        %Matrix(i,1:size(d,2))=d+3500;
    end  
end
%x=0:max(max(Matrix))/bins:max(max(Matrix));
xgrad=0:2:90;
Ngrad=hist(angle,xgrad);
figure
bar(xgrad,Ngrad,1)
xlim([0 90])
maxmatrix=max(max(Matrix));
x=0:binsize:maxmatrix;
Data(1).Matrix=[Matrix];
Data(1).binsize=binsize;


Matrix(Matrix==0)=NaN;
Matrix=Matrix(:,2);
N=hist(Matrix',x);
%N(N>0)=1;
Data(1).N=N;

YY=N;
%[~, idx]=max(YY);
%disp(['max at: ', num2str(x(idx))])

%YY=sum(N,2);
[~, idx]=max(YY);
disp(['max at: ', num2str(x(idx))])

set(fitter,'Userdata',Data);
%XX=reshape(Matrix,1,[]);
%XX(XX==0)=[];
%[nelements,centers] = hist(XX,bins);

axes(haxes);cla;
%bar(centers,nelements,1)
bar(x,YY,1)
xlim([0 maxmatrix]);
set(haxes,'tag','axes1');
toc

if get(handles.radio_map,'value')==1;
    base_size=0.34; %nm
    a=Data(1).reference*base_size;
    b=ones(1,size(a,1));
    c=tril(a*b-b'*a');
    d=reshape(c,1,[]);
    d(d==0)=[];
    Matrix(i,1:size(d,2))=d;
    x=0:binsize:max(max(Matrix));
    
    Matrix(Matrix==0)=NaN;
    N=hist(Matrix',x);
    YY=sum(N,2);

    figure
    %bar(centers,nelements,1)
    bar(x,YY,1)
    set(haxes,'tag','axes1');
    toc
end