function analysefit
DNA_analysis=findobj('Tag','DNA_analysis');

%load data
data=DNA_analysis_load_selected_data();

%save gaussians data on anfit
anfit=findobj('tag', 'analyse_fit');
data_anfit=get(anfit, 'Userdata');
data_anfit.gaussians=data.gaussians;
data_anfit.selected=data.selected;
set(anfit, 'Userdata',data_anfit);

hint=[];
haxes2=findobj(anfit, 'tag', 'axes_histo');
axes(haxes2);cla
nrbins=str2double(get(findobj(anfit,'tag','edit_bins'),'String'));
X=get(findobj(anfit,'tag','popup_histo'),'Value');
mr=get(findobj(anfit,'tag','popup_histo'),'String');

if X==1; Y=6; elseif X==2; Y=10; elseif X==3; Y=9; elseif X==4; Y=11; elseif X==5; Y=8; elseif X==6; Y=7; end
% if size(data_anfit.selected,1)>0
%     molecules=data_anfit.selected;Y=Y+1;
% else
    molecules=data_anfit.gaussians;
% end
if X<=6
    [hint(:,2),hint(:,1)]=hist(molecules(:,Y),nrbins);
    set(findobj(anfit,'tag','edit_up'),'string',num2str(max(molecules(:,Y))));
    set(findobj(anfit,'tag','edit_low'),'string',num2str(min(molecules(:,Y))));
elseif X==7
    [hint(:,2),hint(:,1)]=hist([molecules(:,4);molecules(:,5)],nrbins);
elseif X==8
    [hint(:,2),hint(:,1)]=hist([molecules(:,4)./molecules(:,5)],nrbins);    
end

title(['Distribution of' mr(X)])
bar(hint(:,1),hint(:,2));
set(haxes2, 'tag', 'axes_histo');
