function varargout = DNA_analysis_update_cluster_parameters(varargin)
% DNA_ANALYSIS_UPDATE_CLUSTER_PARAMETERS MATLAB code for DNA_analysis_update_cluster_parameters.fig
%      DNA_ANALYSIS_UPDATE_CLUSTER_PARAMETERS, by itself, creates a new DNA_ANALYSIS_UPDATE_CLUSTER_PARAMETERS or raises the existing
%      singleton*.
%
%      H = DNA_ANALYSIS_UPDATE_CLUSTER_PARAMETERS returns the handle to a new DNA_ANALYSIS_UPDATE_CLUSTER_PARAMETERS or the handle to
%      the existing singleton*.
%
%      DNA_ANALYSIS_UPDATE_CLUSTER_PARAMETERS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DNA_ANALYSIS_UPDATE_CLUSTER_PARAMETERS.M with the given input arguments.
%
%      DNA_ANALYSIS_UPDATE_CLUSTER_PARAMETERS('Property','Value',...) creates a new DNA_ANALYSIS_UPDATE_CLUSTER_PARAMETERS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DNA_analysis_update_cluster_parameters_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DNA_analysis_update_cluster_parameters_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DNA_analysis_update_cluster_parameters

% Last Modified by GUIDE v2.5 09-Nov-2015 17:45:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DNA_analysis_update_cluster_parameters_OpeningFcn, ...
                   'gui_OutputFcn',  @DNA_analysis_update_cluster_parameters_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DNA_analysis_update_cluster_parameters is made visible.
function DNA_analysis_update_cluster_parameters_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DNA_analysis_update_cluster_parameters (see VARARGIN)

% Choose default command line output for DNA_analysis_update_cluster_parameters
handles.output = hObject;


% Update handles structure
guidata(hObject, handles);


% UIWAIT makes DNA_analysis_update_cluster_parameters wait for user response (see UIRESUME)
% uiwait(handles.update_cluster_parameters);


% --- Outputs from this function are returned to the command line.
function varargout = DNA_analysis_update_cluster_parameters_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function load_values()

DNA_analysis=findobj('Tag','DNA_analysis');
pushbutton_autolocate=findobj(DNA_analysis,'Tag','pushbutton_autolocate');
data=get(pushbutton_autolocate,'Userdata');

update_cluster_parameters=findobj('Tag','update_cluster_parameters');

edit_stretch_xaxis=findobj(update_cluster_parameters,'Tag','edit_stretch_xaxis');
edit_density=findobj(update_cluster_parameters,'Tag','edit_density');
edit_distance=findobj(update_cluster_parameters,'Tag','edit_distance');
edit_iterations=findobj(update_cluster_parameters,'Tag','edit_iterations');
edit_thDist=findobj(update_cluster_parameters,'Tag','edit_thDist');
edit_thInlrRatio=findobj(update_cluster_parameters,'Tag','edit_thInlrRatio');
edit_min_distance_fromline=findobj(update_cluster_parameters,'Tag','edit_min_distance_fromline');
edit_min_molecule_length=findobj(update_cluster_parameters,'Tag','edit_min_molecule_length');
edit_angle_deviation=findobj(update_cluster_parameters,'Tag','edit_angle_deviation');


set(edit_stretch_xaxis,'string',data.stretch_xaxis);
set(edit_density,'string',data.density);
set(edit_distance,'string',data.distance);
set(edit_iterations,'string',data.iterations);
set(edit_thDist,'string',data.thDist);
set(edit_thInlrRatio,'string',data.thInlrRatio);
set(edit_min_distance_fromline,'string',data.min_distance_fromline);
set(edit_min_molecule_length,'string',data.min_molecule_length);
set(edit_angle_deviation,'string',data.angle_deviation);

function set_values()
DNA_analysis_update_cluster_parameters()

DNA_analysis=findobj('Tag','DNA_analysis');
pushbutton_autolocate=findobj(DNA_analysis,'Tag','pushbutton_autolocate');

update_cluster_parameters=findobj('Tag','update_cluster_parameters');

edit_stretch_xaxis=findobj(update_cluster_parameters,'Tag','edit_stretch_xaxis');
edit_density=findobj(update_cluster_parameters,'Tag','edit_density');
edit_distance=findobj(update_cluster_parameters,'Tag','edit_distance');
edit_iterations=findobj(update_cluster_parameters,'Tag','edit_iterations');
edit_thDist=findobj(update_cluster_parameters,'Tag','edit_thDist');
edit_thInlrRatio=findobj(update_cluster_parameters,'Tag','edit_thInlrRatio');
edit_min_distance_fromline=findobj(update_cluster_parameters,'Tag','edit_min_distance_fromline');
edit_min_molecule_length=findobj(update_cluster_parameters,'Tag','edit_min_molecule_length');
edit_angle_deviation=findobj(update_cluster_parameters,'Tag','edit_angle_deviation');

%cluster detection parameters
data.stretch_xaxis=str2double(get(edit_stretch_xaxis,'string'));
data.density=str2double(get(edit_density,'string')); %k
data.distance=str2double(get(edit_distance,'string')); %Eps
data.iterations=str2double(get(edit_iterations,'string'));
data.thDist=str2double(get(edit_thDist,'string'));
data.thInlrRatio=str2double(get(edit_thInlrRatio,'string'));
data.min_distance_fromline=str2double(get(edit_min_distance_fromline,'string'));
data.min_molecule_length=str2double(get(edit_min_molecule_length,'string'));
data.angle_deviation=str2double(get(edit_angle_deviation,'string'));

set(pushbutton_autolocate,'Userdata',data);


function edit_stretch_xaxis_Callback(hObject, eventdata, handles)
set_values

function edit_density_Callback(hObject, eventdata, handles)
set_values

function edit_distance_Callback(hObject, eventdata, handles)
set_values

function edit_iterations_Callback(hObject, eventdata, handles)
set_values

function edit_thDist_Callback(hObject, eventdata, handles)
set_values

function edit_thInlrRatio_Callback(hObject, eventdata, handles)
set_values

function edit_min_distance_fromline_Callback(hObject, eventdata, handles)
set_values

function edit_min_molecule_length_Callback(hObject, eventdata, handles)
set_values

function edit_angle_deviation_Callback(hObject, eventdata, handles)
set_values
