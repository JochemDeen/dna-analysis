function DNA_analysis_frame_fitting(CCDfile,frame,Threedfr)
DNA_analysis=findobj('Tag','DNA_analysis');
axes8=findobj(DNA_analysis,'tag','axes8');
imagedata=get(axes8,'Userdata');


level=get(findobj(DNA_analysis,'tag','popupmenu_filter'),'value');

image=imread(CCDfile,frame);

if Threedfr>0
    image=image(:,:,Threedfr);
end

if isfield(imagedata,'zoom') && ~isempty(imagedata.zoom)
    zimage=image(imagedata.zoom(1):imagedata.zoom(2)...
                ,imagedata.zoom(3):imagedata.zoom(4));
else zimage=image;
end

if level==5
    psfWidth=str2double(get(findobj(DNA_analysis,'tag','edit_psfwidth'),'string'));
    pfa=str2double(get(findobj(DNA_analysis,'tag','edit_pfa'),'string'));

    Cpos = LocalizerMatlab('localize', psfWidth, 'glrt', pfa, '2DGauss', zimage);
    out=[Cpos(:,4),Cpos(:,5),Cpos(:,3),Cpos(:,3),Cpos(:,2)./(Cpos(:,3).*Cpos(:,3).*2*pi),Cpos(:,6),Cpos(:,2),Cpos(:,7),Cpos(:,8),Cpos(:,9)];
    if ~isempty(imagedata.zoom)
               out(:,1)=out(:,1)+imagedata.zoom(3);
               out(:,2)=out(:,2)+imagedata.zoom(1);
    else
        out(:,1)=out(:,1)+1;
        out(:,2)=out(:,2)+1;
    end
else

    add=str2double(get(findobj(DNA_analysis,'tag','edit_add'),'string'));
    minarea=str2double(get(findobj(DNA_analysis,'tag','edit_minarea'),'string'));
    Ecc=str2double(get(findobj(DNA_analysis,'tag','edit_eccentricity'),'string'));
    noiseout=str2double(get(findobj(DNA_analysis,'tag','edit_noise'),'string'));
    mask=str2double(get(findobj(DNA_analysis,'tag','edit_masksize'),'string'));
    minAmp=str2double(get(findobj(DNA_analysis,'tag','edit_minampl'),'string'));
    maxresi=str2double(get(findobj(DNA_analysis,'tag','edit_maxstrs'),'string'));
    resistd=str2double(get(findobj(DNA_analysis,'tag','edit_stdres'),'string'));
    method=get(findobj(DNA_analysis,'tag','popupmenu_fitmethod'),'value');

    %FILTERING
    sim=H_prefilteringSPT2008(zimage, level, pwd);
    mn=min(im(:));mx=max(im(:));
    if mn>mx*2;mx=mn*mx*0.5;else mx=mx*0.5;end
    figure(1+(N-1)*3)
    mn=min(sim(:));mx=max(sim(:));
    imagesc(sim, [mn, mx]);axis image;axis off; colormap(gray)

    %FIND MOLECULES
    adval=mean(sim(:));
    threshold = H_thresh_triangle_img2008v1(sim,length(min(sim(:)):1:max(sim(:))),1);
    threshold = round(threshold);
    threshold=threshold + adval*add;
    BW=sim;
    BW(BW(:)<=threshold)=0;
    BW(BW(:)>threshold)=1;
    BW=bwmorph(BW, 'open');
    Hcnr=H_findmolecule_morph2008_v1(sim, threshold, minarea, Ecc, noiseout);

    %PLOT FOUND MOLECULES
    if ~isempty(Hcnr)
        figure(2+(N-1)*3)
        mn=min(im(:));mx=max(im(:));
        if mn*2>mx;mx=mn*mx*0.5;else mx=mx*0.5;end
        imagesc(im, [mn, mx]);axis image;colormap(gray);axis off
        hold on
        plot(Hcnr(:,1), Hcnr(:,2), 'og')
        hold off
    else
        msgbox('No peak was found. Try other parameters.')
    end

    %GAUSSIAN FITTING
    if ~isempty(Hcnr)
        codset=H_codset4gausfit_2008v1(Hcnr, mask*1.5);
        cod=[];
        fitim=im;
        for i=1:size(codset, 2)
            cod=[cod; codset(i).cod];
        end
        %figure(2+(N-1)*3)
        %hold on;plot(cod(:,1), cod(:,2), '.g');hold off
        tic
        out=H_multi2DgaussianFIT_2008v1_1(fitim, codset, mask, 2, minAmp, maxresi,resistd, 1, method);
        toc
        if ~isempty(out)
           if ~isempty(maxresi) && ~isempty(resistd)
                out(out(:,8)>maxresi | out(:,10)>resistd,:)=[];
           end
            figure(3+(N-1)*3)
            mn=min(fitim(:));mx=max(fitim(:));
            imagesc(fitim, [mn, 0.7*mx]);axis image;axis off; colormap(bone)
            hold on
            plot(Hcnr(:,1), Hcnr(:,2), 'gx', 'markersize', 3);
            plot(out(:,1), out(:,2), 'ro');hold off
            title(['detected ' num2str(size(out,1)) ' molecules from ' num2str(size(Hcnr,1)) ' molecules'])
           if ~isempty(imagedata.zoom)
               out(:,1)=out(:,1)+imagedata.zoom(3);
               out(:,2)=out(:,2)+imagedata.zoom(1);
           end
        end
    end
end
data=DNA_analysis_load_selected_data();
data.gaussians=[data.gaussians;[ones(size(out,1),1)*begin,out,ones(size(out,1),1)*newscope]];

DNA_analysis_save_selected_data(data)




