function DNA_analysis_add_fit_line(molecule)
%Draw a line and obtain the 1D data
data=DNA_analysis_load_selected_data();

Mlocation_px=DNA_analysis_fit_line(molecule);
    
if ~isempty(Mlocation_px)
    %convert to nm
    if isfield(data.flt, 'pixelsize')
        Mlocation_nm=Mlocation_px*data.flt.pixelsize;
    else
        pixelsize = inputdlg('pixelsize');
        Mlocation_nm=Mlocation_px*pixelsize;
    end

    %Get current Mlocations_userdata
    DNA_analysis=findobj('Tag', 'DNA_analysis');
    listboxMlocation=findobj(DNA_analysis,'Tag', 'listboxMlocation');
    Mlocations_Userdata=get(listboxMlocation,'Userdata');

    %Store the new locations in Mlocations_userdata
    Mlocations_Userdata.newMlocations=Mlocation_nm;
    set(listboxMlocation,'Userdata',Mlocations_Userdata);

    updategui('updatelistMlocation')
    
    %remove gaussians
    removegaussians();

end

function removegaussians()
DNA_analysis=findobj('Tag','DNA_analysis');

%Load data, remove the selected molecules and re-save the data.
data=DNA_analysis_load_selected_data();

%get zoom
zoom=getzoomdata();

%load gaussians
gaussians=select_gaussians(data.gaussians,zoom);
selected=select_gaussians(data.selected,zoom);

%C=ones(size(B,2),1)*A-B'*ones(1,size(A,2))

%compare x-locations
gausx=gaussians(:,2)';
selx=selected(:,3)';
Cx=ones(size(selx,2),1)*gausx-selx'*ones(1,size(gausx,2));
%Cx=gausx*ones(1,size(selx,1))-ones(size(gausx,1),1)*selx';

%compare x-locations
gausy=gaussians(:,3)';
sely=selected(:,4)';
Cy=ones(size(sely,2),1)*gausy-sely'*ones(1,size(gausy,2));

%find all minimal variations of squared distance
Csumx=min(Cx.^2);
Csumy=min(Cy.^2);

%add both minimal variations together
Csum=Csumx+Csumy;

%find index of minimal variations
kindex=find(~Csum);

gaussians(kindex,:)=[];
data.gaussians=gaussians;
data.selected=[];

DNA_analysis_save_selected_data(data)

function zoom=getzoomdata()
DNA_analysis=findobj('Tag','DNA_analysis');

axes8=findobj(DNA_analysis,'tag','axes8');
imagedata=get(axes8,'Userdata');

%check foor zoom function
if isfield(imagedata,'zoom') && ~isempty(imagedata.zoom);
    zoom=imagedata.zoom;
else
    zoom=[1,1,1,1]; %if there is no zoom saved, create empty zoom variable
end


function plotgaussians=select_gaussians(gaussians,zoom)
DNA_analysis=findobj('Tag','DNA_analysis');

%initialize values
plotgaussians=[];

%if the image is scanned, select those that correspond to the correct scan
if get(findobj(DNA_analysis,'tag','radio_scan'),'value')==1
    %how many frames correspond to the same image
    scanframes=round(str2double(get(findobj(DNA_analysis,'tag','edit_scanframes'),'string')));
    
    %what scan are we looking at
    start=round(str2double(get(findobj(DNA_analysis,'tag','edit_scan'),'string')));
    
    %set begin and end frame (between is where the gaussians ought to be
    beginframe=(start-1)*scanframes+1;
    endframe=begin+scanframes-1;
    
    if  ~isempty(gaussians)
        %select the gaussians found between endframe and beginframe
        plotgaussians=gaussians(gaussians(:,1)>=beginframe...
                                    && gaussians(:,1)<endframe,:);
    end
else
    if  ~isempty(gaussians)
        plotgaussians=gaussians;
    end
end

%set the zoom
if  ~isempty(plotgaussians)
    plotgaussians(:,2)=plotgaussians(:,2)-zoom(2)+1;
    plotgaussians(:,3)=plotgaussians(:,3)-zoom(3)+1;
end

