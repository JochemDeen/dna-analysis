function plotgaussians=select_gaussians(gaussians,zoom)
DNA_analysis=findobj('Tag','DNA_analysis');

%initialize values
plotgaussians=[];

%if the image is scanned, select those that correspond to the correct scan
if get(findobj(DNA_analysis,'tag','radio_scan'),'value')==1
    %how many frames correspond to the same image
    scanframes=round(str2double(get(findobj(DNA_analysis,'tag','edit_scanframes'),'string')));
    
    %what scan are we looking at
    start=round(str2double(get(findobj(DNA_analysis,'tag','edit_scan'),'string')));
    
    %set begin and end frame (between is where the gaussians ought to be
    beginframe=(start-1)*scanframes+1;
    endframe=begin+scanframes-1;
    
    if  ~isempty(gaussians)
        %select the gaussians found between endframe and beginframe
        plotgaussians=gaussians(gaussians(:,1)>=beginframe...
                                    && gaussians(:,1)<endframe,:);
    end
else
    if  ~isempty(gaussians)
        plotgaussians=gaussians;
    end
end

%set the zoom
if  ~isempty(plotgaussians)
    plotgaussians(:,2)=plotgaussians(:,2)-zoom(2)+1;
    plotgaussians(:,3)=plotgaussians(:,3)-zoom(3)+1;
end