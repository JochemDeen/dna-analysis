function DNA_analysis_autodetect_molecules()
DNA_analysis=findobj('Tag','DNA_analysis');
data=DNA_analysis_load_selected_data();

%Select gaussian molecules
selected_gaussians=select_gaussians(data.gaussians);

%Detect clusters
[class,type]=detect_clusters(selected_gaussians);

%select molecules
molecules=detect_molecules(class,selected_gaussians);

for i=1:size(molecules,2)
    %add molecule to the list
    molecule=molecules(i).cluster;
    
    if ~isempty(molecule)
        %save the molecule
        save_and_plot_molecules(molecule)
        
        %Fit line and add to list
        DNA_analysis_add_fit_line(molecule)
    end
end

function save_and_plot_molecules(molecule)
%Load data and replace previous selected with current molecule
data=DNA_analysis_load_selected_data();
data.selected=[];
data.selected(:,3)=molecule(:,1);
data.selected(:,4)=molecule(:,2);

%Save selected molecules
DNA_analysis_save_selected_data(data)

%load axes
DNA_analysis=findobj('Tag','DNA_analysis');
axes8=findobj(DNA_analysis,'tag','axes8');

%load imagedata
imagedata=get(axes8,'Userdata');

%check foor zoom function
if isfield(imagedata,'zoom') && ~isempty(imagedata.zoom);
    zoom=imagedata.zoom;
else
    zoom=[1,1,1,1]; %if there is no zoom saved, create empty zoom variable
end

%select axes
axes(axes8);
figure(10);
axis([0 512 0 512]);
hold on

%plot selected molecules
plotselected=set_zoom_ongaussians(data.selected,zoom);
plot(plotselected(:,3),plotselected(:,4),'mx','markersize',2);
hold off


function plotgaussians=set_zoom_ongaussians(gaussians,zoom)
%initialize values
plotgaussians=[];

%set the zoom
if  ~isempty(gaussians)
    plotgaussians(:,3)=gaussians(:,3)-zoom(2)+1;
    plotgaussians(:,4)=gaussians(:,4)-zoom(3)+1;
end



function [class,type]=detect_clusters(gaussians)
%Load cluster parameters
data=get_cluster_parameters();

%Calculate angle of molecules orientation
max_angle=detect_angle_molecule(gaussians);

%Calculate rotational matrix
R=[cos(max_angle), -sin(max_angle);sin(max_angle),cos(max_angle)];
%Rotate gaussians (Molecules are oriented perpendicular to x-axis
rotate_gaussians=gaussians*R;

%Stretch x-axis by 10
stretch=data.stretch_xaxis;
rotate_gaussians(:,1)=rotate_gaussians(:,1)*stretch;

%density scan of rotated gaussians
k=data.density;
Eps=data.distance;
[class,type]=dbscan(rotate_gaussians,k,Eps);


function molecules=detect_molecules(class,selected_gaussians)
%load parameters
data=get_cluster_parameters();
iterNum = data.iterations;
thDist = data.thDist;
thInlrRatio = data.thInlrRatio;
min_distance= data.min_distance_fromline;
min_molecule_length=data.min_molecule_length;
angle_deviation=data.angle_deviation;

%initialization
theta=zeros(max(class),1);
R=zeros(max(class),1);
y=1;

%fit lines with RANSAC algorithm through all clusters
for i=1:max(class)
    %select cluster
    cluster=selected_gaussians(class==i,:); 
    
    %RANSAC fitting
    [t,r] = ransac(cluster',iterNum,thDist,thInlrRatio);
    theta(i)=t;
    R(i)=r;
end

%calculate median angle
median_theta=median(theta);

molecules=[];

for i=1:max(class)
    %select cluster
    cluster=selected_gaussians(class==i,:); 
    
    %y=rico*x+offset
    rico = -tan(theta(i));
    offset = R(i)/cos(theta(i));
    
    %A.X+B.Y+C=0
    A=-rico;
    B=1;
    C=-offset;
    
    %en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    distance=abs(A*cluster(:,1)+B*cluster(:,2)+C)/sqrt(A^2+B^2);
    
    %eliminate spots further than a minimal distance from the line
    distance_bool=distance<min_distance;
    cluster=cluster(distance_bool,:);
    
    %Calculate length of molecule
    minx=min(cluster(:,1));
    maxx=max(cluster(:,1));
    miny=min(cluster(:,2));
    maxy=max(cluster(:,2));
    
    length=sqrt((maxx-minx)^2+(maxy-miny)^2);
    
    %include molecule if longer than minimal length and smaller deviation angle deviation
    if length>min_molecule_length && (theta(i)>median_theta-angle_deviation && theta(i)<median_theta+angle_deviation)
        molecules(y).cluster=cluster;
        y=y+1;
    end
end




function cluster_data=get_cluster_parameters()
DNA_analysis=findobj('Tag','DNA_analysis');
pushbutton_autolocate=findobj(DNA_analysis,'Tag','pushbutton_autolocate');

cluster_data=get(pushbutton_autolocate,'Userdata');



function selected_gaussians=select_gaussians(gaussians)
DNA_analysis=findobj('Tag','DNA_analysis');

axes8=findobj(DNA_analysis,'tag','axes8');
imagedata=get(axes8,'Userdata');

s=imagedata.imsize; % 1Y(1) 2Y(2) 3X(1) 4X(2)

%check foor zoom function
if isfield(imagedata,'zoom') && ~isempty(imagedata.zoom);
    zoom=imagedata.zoom;
else
    zoom=[1,1,1,1]; %if there is no zoom saved, create empty zoom variable
end

if ~isempty(gaussians)
   
    %select gaussians from that frame
    selected_gaussians=select_zoom_gaussians(gaussians,zoom);
    selected_gaussians=[selected_gaussians(:,2),selected_gaussians(:,3)];
end

function plotgaussians=select_zoom_gaussians(gaussians,zoom)
DNA_analysis=findobj('Tag','DNA_analysis');

%initialize values
plotgaussians=[];

%if the image is scanned, select those that correspond to the correct scan
if get(findobj(DNA_analysis,'tag','radio_scan'),'value')==1
    %how many frames correspond to the same image
    scanframes=round(str2double(get(findobj(DNA_analysis,'tag','edit_scanframes'),'string')));
    
    %what scan are we looking at
    start=round(str2double(get(findobj(DNA_analysis,'tag','edit_scan'),'string')));
    
    %set begin and end frame (between is where the gaussians ought to be
    beginframe=(start-1)*scanframes+1;
    endframe=begin+scanframes-1;
    
    if  ~isempty(gaussians)
        %select the gaussians found between endframe and beginframe
        plotgaussians=gaussians(gaussians(:,1)>=beginframe...
                                    && gaussians(:,1)<endframe,:);
    end
else
    if  ~isempty(gaussians)
        plotgaussians=gaussians;
    end
end

%set the zoom
if  ~isempty(plotgaussians)
    plotgaussians(:,2)=plotgaussians(:,2)-zoom(2)+1;
    plotgaussians(:,3)=plotgaussians(:,3)-zoom(3)+1;
end

