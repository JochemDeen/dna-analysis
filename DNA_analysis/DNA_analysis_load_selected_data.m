function data=DNA_analysis_load_selected_data()
DNA_analysis=findobj('Tag','DNA_analysis');
listbox2=findobj(DNA_analysis,'tag','listbox2');
%get selected filename
i=get(listbox2,'value');

%get the filename and path
filedata=get(listbox2,'Userdata');
fn=filedata.list(i).fn;
fp=filedata.list(i).fp;

%load the file
data=loaddata(fp,fn);

function data=loaddata(fp,fn)
load([fp fn]);
data.gaussiansbackup=gaussiansbackup;
data.gaussians=gaussians;
data.CCDname=CCDname;
data.CCDpath=CCDpath;
data.fn=fn;
data.fp=fp;
data.flt=flt;
data.selected=selected;