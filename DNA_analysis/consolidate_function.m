function [lijstje,y]=consolidate_function(xloc,distance)


niet=[];
nogniet=[];
oke=[];
y=0;%generation
done=[];

%xloc=gaussians(:,2:3); 
xloc=[xloc,ones(size(xloc,1),1)]; %starting parameters
mind=0;

while sum((mind<distance))>0 && size(xloc,1)>1
    y=y+1;%generation=generation+1
    aver=[];
    x=pdist(xloc(:,1:2)); %calculate distance
    x=squareform(x); %transform to square matrix
    x=x./(x>0); %get rid of 0 ...
    [mind,I]=min(x); %calculate minimal distance
    weights=xloc(:,3);

    oke=[[1:size(xloc,1)]',I',(mind<distance)'];
    oke(:,4)=(oke(oke(:,2),2)==oke(:,1)); 
    %oke: 
    %1 index of location in original x
    %2 index of nearest location, is it
    %3 closest enough (0 or 1)
    %4 is other one also closest enough (0 or 1)

    oke1=oke((oke(:,3)==1 & oke(:,4)==1),:); % 3 and 4 are 1

    
    for i=1:size(oke1,1)
        aver(i,2)=wmean([xloc(oke1(i,1),2),xloc(oke1(i,2),2)],[weights(oke1(i,1),1),weights(oke1(i,2),1)],2)';
        aver(i,1)=wmean([xloc(oke1(i,1),1),xloc(oke1(i,2),1)],[weights(oke1(i,1),1),weights(oke1(i,2),1)],2)';
        aver(i,3)=weights(oke1(i,1),1) + weights(oke1(i,2),1);
    end
    
    aver=unique(aver,'rows'); %weighted average

    oke0=oke((oke(:,3)==1 & oke(:,4)==0),:);%3 is 1, 4 is 0 something is closest but not mutual.
    nogniet=[xloc(oke0(:,1),1),xloc(oke0(:,1),2),xloc(oke0(:,1),3)];

    oke00=oke((oke(:,3)==0),:); %3 and 4=0, nothing is close enough, never used again
    niet=[niet;xloc(oke00(:,1),1),xloc(oke00(:,1),2),xloc(oke00(:,1),3)];

    xloc=nogniet; %Put the not yet together for next check
    done=[done;aver]; %add average to done list;
    %disp(['Generation ' num2str(y)]);
    %sum(xloc(:,3))+sum(niet(:,3))
end;

%lijstje=[niet(:,1:2);nogniet(:,1:2);aver(:,1:2)];
lijstje=[niet(:,1:2);done(:,1:2);xloc(:,1:2)];
if size(xloc,1)==1
    lijstje=[lijstje;xloc(1,1),xloc(1,2)];
end

%aver(:,2)=wmean([x(oke1(:,1),2),x(oke1(:,2),2)],[weights(oke1(:,1),1),weights(oke1(:,2),1)],2)';
%aver(:,1)=wmean([x(oke1(:,1),1),x(oke1(:,2),1)],[weights(oke1(:,1),1),weights(oke1(:,2),1)],2)';
%aver(:,3)=weights(oke1(:,1),1) + weights(oke1(:,2),1);
    