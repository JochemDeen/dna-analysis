function DNA_analysis_molecule_selection(cases)

switch cases
    case 'findmolecules'
        findmolecules();
    case 'deletemolecules'
        deletemolecules();
    case 'deletemolecules_axes9'
        deletemolecules_axes9();
    case 'fit_line'
        fit_line;
    case 'consolidate'
        consolidate();
    case 'removedualsite'
        removedualsite();
end

%Cases
%-----------------------------------------------------------------------------------------------------------------
function findmolecules()
DNA_analysis=findobj('Tag','DNA_analysis');

%Load data, remove the selected molecules and re-save the data.
data=DNA_analysis_load_selected_data();
data.selected=[];
DNA_analysis_save_selected_data(data)

axes8=findobj(DNA_analysis,'tag','axes8');
imagedata=get(axes8,'Userdata');

s=imagedata.imsize; % 1Y(1) 2Y(2) 3X(1) 4X(2)

%check foor zoom function
if isfield(imagedata,'zoom') && ~isempty(imagedata.zoom);
    zoom=imagedata.zoom;
else
    zoom=[1,1,1,1]; %if there is no zoom saved, create empty zoom variable
end


if ~isempty(data.gaussians)
   
    %select gaussians from that frame
    plotgaussians=select_gaussians(data.gaussians,zoom);
    
    axes(axes8);
    [gx1, gy1, op]=ginput(1);
   
    if op<2
        %do the clicks not exceed the imageframe?
        [gx1,gy1]=check_locations(gx1,gy1,s);
        
        hold on;
        plot(gx1,gy1,'og','markersize',6)
        
        %2nd click
        [gx2, gy2, op]=ginput(1);
        if op<2 &gx1~=gx2 &gy1~=gy2
            %do the clicks not exceed the imageframe?
            [gx2,gy2]=check_locations(gx2,gy2,s);
            
            if gx1<gx2; x=[gx1 gx2];else;x=[gx2, gx1];end
            if gy1<gy2; y=[gy1 gy2];else;y=[gy2, gy1];end
        end
        plot(gx2,gy2,'og','markersize',6)
        
        %create rectangle
        [P11,P12,P21,P22]=create_rectangle(gx1,gx2,gy1,gy2);
        
        %plot lines       
        line([P11(1) P21(1)],[P11(2) P21(2)],'color','green')
        line([P12(1) P22(1)],[P12(2) P22(2)],'color','green')
        
        %create polygon
        x=[P11(1) P12(1) P22(1) P21(1)];
        y=[P11(2) P12(2) P22(2) P21(2)];
        
        %check if gaussians within polygon
        
        in=inpolygon(plotgaussians(:,2), plotgaussians(:,3), x, y);
        
        %create a list of molecules that are in the polygon
        in2=cat(2,in, data.gaussians);
        data.selected=in2(in2(:,1)~=0,:);
        
        if ~isempty(data.selected) %if there are molecules within the selected
            plotselected=data.selected(:,2:end);
            plotselected=select_gaussians(plotselected,zoom);
            plot(plotselected(:,2),plotselected(:,3),'yo','markersize',2);
            
            hold off
            set(axes8,'tag','axes8');
            
            %Plot the selected molecules on the 2nd axes (below)
            axes9=findobj(DNA_analysis,'tag','axes9');    
            axes(axes9);cla;
            
            plot(data.selected(:,3),data.selected(:,4),'rx','markersize',4);
            title([num2str(size(data.selected,1)) ' molecules'],'fontsize',12, 'Fontname', 'candara');         
            set(gca,'YDir','reverse');
            set(axes9,'tag','axes9');
        else 
            %if no molecules, hold off axes8 and reset tag (silly bug)
            hold off
            set(axes8,'tag','axes8');
        end
        
       DNA_analysis_save_selected_data(data)
    end
end

function deletemolecules()
DNA_analysis=findobj('Tag','DNA_analysis');
axes8=findobj(DNA_analysis,'tag','axes8');

imagedata=get(axes8, 'UserData');
s=imagedata.imsize;

if isfield(imagedata,'zoom') && ~isempty(imagedata.zoom);
    zoom=imagedata.zoom;
else
    zoom=[1,1,1,1]; %if there is no zoom saved, create empty zoom variable
end

data=DNA_analysis_load_selected_data();

if ~isempty(data.gaussians)
    op=1;
    %while not right clicking
    while op==1
        %click on axes
        [gx1, gy1, op]=ginput(1);
        
        %add the zoom, if the image is zoomed
        gx1=gx1+zoom(3)-1;gy1=gy1+zoom(1)-1;
        
        %find the closest spot in data.gaussians
        pts=data.gaussians; %points on frame
        if gx1+s(3)-1>=s(3) && gx1+s(3)-1<=s(4) && gy1+s(1)-1>=s(1) && gy1+s(1)-1<=s(2) && op<2
            dr=sqrt((pts(:,2)-gx1).^2+(pts(:,3)-gy1).^2);
            c=find(dr==min(dr(:))); %select closest point
        end
        
        %if left click
        if op==1
        %temporarily clear data.gaussians
        data.gaussians=[];            
            if ~isempty(c)
                
                %empty the closest points
                pts(c(1),:)=[];
                %re-add gaussians
                data.gaussians=cat(1, data.gaussians, pts(:,:));
            else
                %if no spot found, add all gaussians back
                data.gaussians=cat(1, data.gaussians, pts(:,:));
            end
        end
        %save the molecules
        DNA_analysis_save_selected_data(data)
        
        %redraw image after clicking (removes the closest spot)
        DNA_analysis_drawimage
    end %while
end

function deletemolecules_axes9()
DNA_analysis=findobj('Tag','DNA_analysis');
axes9=findobj(DNA_analysis,'tag','axes9');


data=DNA_analysis_load_selected_data();


if ~isempty(data.selected)
    s=[get(axes9,'XLim') get(axes9,'YLim')];

    op=1;
    %while not right clicking
    while op==1
        %click on axes
        [gx1, gy1, op]=ginput(1);

        %find the closest spot in data.selected
        pts=data.selected; %points on frame
        if gx1>=s(1) && gx1<=s(2) && gy1>=s(3) && gy1<=s(4) && op<2
            dr=sqrt((pts(:,3)-gx1).^2+(pts(:,4)-gy1).^2);
            c=find(dr==min(dr(:))); %select closest point
        end
        
        %if left click
        if op==1
            %temporarily clear data
            data.selected=[]; 
            %if closest match found, delete that spot and re-add back
            if ~isempty(c)
                pts(c(1),:)=[];
                data.selected=cat(1, data.selected, pts(:,:));
            else
                data.selected=cat(1, data.selected, pts(:,:));
            end
        end
        DNA_analysis_save_selected_data(data)
        
        %if not empty, replot graph
        if ~isempty(data.selected)
            axes(axes9);cla;
            plot(data.selected(:,3),data.selected(:,4),'rx','MarkerSize',4);
            title([num2str(size(data.selected,1)) ' molecules'],'fontsize',12, 'Fontname', 'candara');
            set(gca,'YDir','reverse');
        else
            axes(axes9);cla
            title('No molecules','fontsize',12, 'Fontname', 'candara');
        end
        
        %Silly bug
        set(axes9,'tag','axes9');
    end %while
end

function consolidate()
DNA_analysis=findobj('Tag','DNA_analysis');

data=DNA_analysis_load_selected_data();
edit_consolidate=findobj(DNA_analysis,'Tag','edit_consolidate');
distance=str2num(get(edit_consolidate,'string'));
edit_scanframes=findobj(DNA_analysis,'Tag','edit_scanframes');
scanframes=str2num(get(edit_scanframes,'string'));
radio_scan=findobj(DNA_analysis,'Tag','radio_scan');



if myIsField (data.flt, 'pixelsize')
    distancepixel=distance/data.flt.pixelsize;
else
    pixelsize = inputdlg('pixelsize');
    distancepixel=distance/pixelsize;
end

gaussiansnew=[];

if get(radio_scan,'value')==1
    frames=data.flt.frames;
    maxscan=frames/scanframes;
    for x =1:maxscan
        %(data.gaussians(:,1)>(x-1)*scanframes & data.gaussians(:,1)<=(x)*scanframes)
        tic
        gaussiansnewadd=[];
        
        [locations,y]=consolidate_function(data.gaussians((data.gaussians(:,1)>(x-1)*scanframes & data.gaussians(:,1)<=(x)*scanframes),2:3),distancepixel);
        disp(['Scan ' num2str(x)]);
        disp([ num2str(y) ' generations']);
        
        gaussiansnewadd(:,2)=locations(:,1);
        gaussiansnewadd(:,3)=locations(:,2);
        gaussiansnewadd(:,1)=locations(:,1)./locations(:,1)*(x-1)*scanframes+1;
        gaussiansnew=[gaussiansnew ;gaussiansnewadd];
        toc
        disp(' ');
    end
else
    tic
    [locations,y]=consolidate_function(data.gaussians(:,2:3),distancepixel);
    disp([ num2str(y) ' generations']);
    gaussiansnew(:,2)=locations(:,1);
    gaussiansnew(:,3)=locations(:,2);
    toc
end

data.gaussians=[];
data.gaussians=gaussiansnew;
DNA_analysis_save_selected_data(data)

function removedualsite()
DNA_analysis=findobj('Tag','DNA_analysis');

data=DNA_analysis_load_selected_data();
edit_consolidate=findobj(DNA_analysis,'Tag','edit_consolidate');
distance=str2num(get(edit_consolidate,'string'));
edit_scanframes=findobj(DNA_analysis,'Tag','edit_scanframes');
scanframes=str2num(get(edit_scanframes,'string'));
radio_scan=findobj(DNA_analysis,'Tag','radio_scan');



if myIsField (data.flt, 'pixelsize')
    distancepixel=distance/data.flt.pixelsize;
else
    pixelsize = inputdlg('pixelsize');
    distancepixel=distance/pixelsize;
end

gaussiansnew=[];

if get(radio_scan,'value')==1
    frames=data.flt.frames;
    maxscan=frames/scanframes;
    for x =1:maxscan
        %(data.gaussians(:,1)>(x-1)*scanframes & data.gaussians(:,1)<=(x)*scanframes)
        tic
        gaussiansnewadd=[];
        
        locations=remove_double_DNAM(data.gaussians((data.gaussians(:,1)>(x-1)*scanframes & data.gaussians(:,1)<=(x)*scanframes),2:3),distancepixel);
        disp(['Scan ' num2str(x)]);
        
        gaussiansnewadd(:,2)=locations(:,1);
        gaussiansnewadd(:,3)=locations(:,2);
        gaussiansnewadd(:,1)=locations(:,1)./locations(:,1)*(x-1)*scanframes+1;
        gaussiansnew=[gaussiansnew ;gaussiansnewadd];
        toc
        disp(' ');
    end
else
    tic
    locations=remove_double_DNAM(data.gaussians(:,2:3),distancepixel);
    gaussiansnew(:,2)=locations(:,1);
    gaussiansnew(:,3)=locations(:,2);
    toc
end

data.gaussians=[];
data.gaussians=gaussiansnew;
DNA_analysis_save_selected_data(data)


%Helper functions
%-----------------------------------------------------------------------------------------------------------------
function [gx,gy]=check_locations(gx,gy,s)
if gx+s(3)-1<s(3); gx=s(3);
elseif gx+s(3)-1>s(4); gx=s(4);
elseif gy+s(1)-1<s(1); gy=s(1);
elseif gy+s(1)-1>s(2); gy=s(2); end
function [P11,P12,P21,P22]=create_rectangle(gx1,gx2,gy1,gy2)

%create rectangle
DNA_analysis=findobj('Tag','DNA_analysis');
a1=0;a2=0;
if gy1<gy2; a1=gy2-gy1;elseif gy1~=gy2;a1=gy1-gy2;end
if gx1<gx2; a2=gx2-gx1;elseif gx1~=gx2;a2=gx1-gx2;end

edit_size=findobj(DNA_analysis,'tag','edit_size');
lengte=str2double(get(edit_size,'String'))/2; %width of rectangle

b2=sqrt(a1^2*lengte^2/(a2^2+a1^2));
b1=sqrt(a2^2*lengte^2/(a1^2+a2^2));

if (gx2<=gx1 && gy2>=gy1) || (gx2>=gx1 && gy2<=gy1)
    P11=[gx1-b2 gy1-b1]; %(x,y)
    P12=[gx1+b2 gy1+b1];
    P21=[gx2-b2 gy2-b1];
    P22=[gx2+b2 gy2+b1];
elseif (gx2<gx1 && gy2<gy1) || (gx2>gx1 && gy2>gy1)
    P11=[gx1-b2 gy1+b1];
    P12=[gx1+b2 gy1-b1];
    P21=[gx2-b2 gy2+b1];
    P22=[gx2+b2 gy2-b1];            
end
function plotgaussians=select_gaussians(gaussians,zoom)
DNA_analysis=findobj('Tag','DNA_analysis');

%initialize values
plotgaussians=[];

%if the image is scanned, select those that correspond to the correct scan
if get(findobj(DNA_analysis,'tag','radio_scan'),'value')==1
    %how many frames correspond to the same image
    scanframes=round(str2double(get(findobj(DNA_analysis,'tag','edit_scanframes'),'string')));
    
    %what scan are we looking at
    start=round(str2double(get(findobj(DNA_analysis,'tag','edit_scan'),'string')));
    
    %set begin and end frame (between is where the gaussians ought to be
    beginframe=(start-1)*scanframes+1;
    endframe=begin+scanframes-1;
    
    if  ~isempty(gaussians)
        %select the gaussians found between endframe and beginframe
        plotgaussians=gaussians(gaussians(:,1)>=beginframe...
                                    && gaussians(:,1)<endframe,:);
    end
else
    if  ~isempty(gaussians)
        plotgaussians=gaussians;
    end
end

%set the zoom
if  ~isempty(plotgaussians)
    plotgaussians(:,2)=plotgaussians(:,2)-zoom(3)+1;
    plotgaussians(:,3)=plotgaussians(:,3)-zoom(1)+1;
end

