function Mlocation=DNA_analysis_fit_line(selected)
%Note changed selected(:,3) to selected(:,1)

%Create matrix with y-values
A=[selected(:,1),ones(size(selected(:,1),1),1)];
B=inv(A'*A)*A'*selected(:,2); %Ordinary least squares for least square

%create x-values for a line through the spots
xx=min(selected(:,1)):0.1:max(selected(:,1));

%draw line using x-values and B(1), linear coefficient and B(2) offset.
yy=B(1)*xx+B(2);
line=[xx;yy];

DNA_analysis=findobj('Tag','DNA_analysis');
axes9=findobj(DNA_analysis,'tag','axes9');
axes(axes9);cla;
plot(selected(:,1),selected(:,2),'rx','Markersize',6);
hold on
plot(xx,yy);

%Get only the value paralel to the line running through all the spots
Mlocation=((selected(:,1)+B(2)/B(1))+selected(:,2)*B(1))/sqrt(1+B(1)^2); %dot product::  A dot B/ |B|

%create new X and Y using matrix transformation
Xmlocation=Mlocation*1/sqrt(1+B(1)^2)-B(2)/B(1);
Ymlocation=Mlocation*B(1)/sqrt(1+B(1)^2);

%plot these as well in the figure
plot(Xmlocation,Ymlocation,'go','Markersize',4);
hold off
set(gca,'YDir','reverse');
set(axes9,'tag','axes9')

% Xpoints=B(1)/sqrt(1+B(1)^2)*(selected(:,3)+B(2)/B(1))-1/sqrt(1+B(1)^2)*selected(:,4);
% Ypoints=1/sqrt(1+B(1)^2)*(selected(:,3)+B(2)/B(1))+B(1)/sqrt(1+B(1)^2)*selected(:,4);
% Mloczeros=zeros(size(Mlocation,1),1);
%figure
%plot(Xpoints*81,Ypoints*81,'rx','Markersize',6);
%hold on
%plot(Mloczeros,Mlocation*81,'go','Markersize',4);
%hold off

%create one dimensional data
Mlocation=sort(abs(Mlocation));
Mlocation=Mlocation-Mlocation(1);
