function [newmolecule1,newmolecule2]=createnewmolecule_DNAM(reference,molecule,all_sites)

all_sites=fliplr(all_sites);
newmolecule1=[];
newmolecule2=[];

for i=1:size(molecule,1)
   newsite=[];
   site=molecule(i);
   if sum(all_sites(1,:)==i)
       
       newindex=(all_sites(2,all_sites(1,:)==i));
       newsite=reference(newindex);
       newmolecule1=[newmolecule1;newsite];
   else
       
       %there exist an i in map smaller than current
       if sum(all_sites(1,:)<i)
           [val,mapindex]=min(abs(all_sites(1,all_sites(1,:)<i)-i));
           val=i-val;
           newindex=(all_sites(2,mapindex));

           diff=molecule(i)-molecule(val);

           newsite=[newsite;reference(newindex)+diff];

       end
       
       %there exist an i in map bigger than current
       if sum(all_sites(1,:)>i)
           val=min((all_sites(1,all_sites(1,:)>i)-i))+i;
           
           newindex=(all_sites(2,all_sites(1,:)==val));

           diff=molecule(val)-molecule(i);

           newsite=[newsite;reference(newindex)-diff];
           
       end
       
       newsite=mean(newsite);
       
       newmolecule2=[newmolecule2;newsite];
   end
    
end
