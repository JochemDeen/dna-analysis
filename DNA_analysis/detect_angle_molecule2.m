function detect_angle_molecule2(data)

maxangle=180;
num_sites_av=zeros(maxangle,1);
num_sites_max=zeros(maxangle,1);

for i=1:maxangle
    angle=(i/360)*(2*pi);
   
    R=[cos(angle), -sin(angle);sin(angle),cos(angle)];
    
    data2=data*R;
    
    figure(1);plot(data2(:,1),data2(:,2),'rx', 'MarkerSize',2);
    axis([0 512 0 512])
    figure(2);hist(data2(:,1),min(data2(:,1)):max(data2(:,1)));
    histogram_sites=hist(data2(:,1),min(data2(:,1)):max(data2(:,1)));
    num_sites_av(i)=median(histogram_sites);
    num_sites_max(i)=max(histogram_sites);

end

figure;
x=1:maxangle;
plot(x,num_sites_av,'r',x,num_sites_max,'k');
[~,i]=max(num_sites_max)
d=1;