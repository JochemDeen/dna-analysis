function DNA_analysis_drawimage()
%disable buttons while updating gui
setenable('off')

%Creation of image
image=createimage();
setsizes(image);

%obtain minimal and maximal values
[mx,mn]=set_min_max(image);

%plot the image
plotimage(image,mx,mn)

%plot the gaussians
plotgaussians();

%2nd program, if present, analyse whatever is here
anfit=findobj('tag', 'analyse_fit');
if ~isempty(anfit)
    analysefit
end

%re-enable buttons
setenable('on')

%Helper functions
%---------------------------------------------------------------------------
function setenable(value)
DNA_analysis=findobj('Tag','DNA_analysis');

pushbutton=findobj(DNA_analysis,'style','pushbutton');
radiobutton=findobj(DNA_analysis,'style','radiobutton');
edit=findobj(DNA_analysis,'style','edit');
slider=findobj(DNA_analysis,'style','slider');
set(pushbutton,'enable',value);
set(radiobutton,'enable',value)
set(edit,'enable',value)
set(slider,'enable',value')
pause(0.01)
function setenableimages(value)
DNA_analysis=findobj('Tag','DNA_analysis');

set(findobj(DNA_analysis,'tag','radiokeepscale'),'enable',value)
set(findobj(DNA_analysis,'tag','editscalemax'),'enable',value)
set(findobj(DNA_analysis,'tag','editscalemin'),'enable',value)
set(findobj(DNA_analysis,'tag','sliderscalemax'),'enable',value)
set(findobj(DNA_analysis,'tag','sliderscalemin'),'enable',value)
set(findobj(DNA_analysis,'tag','edit_frames'),'enable',value);
set(findobj(DNA_analysis,'tag','edit9'),'enable',value);
set(findobj(DNA_analysis,'tag','radio_first'),'enable',value);
function newscope=check_if_3d(CCDfile)
DNA_analysis=findobj('Tag','DNA_analysis');

if size(imread(CCDfile, 2,1),3)>1
    newscope=round(str2double(get(findobj(DNA_analysis,'tag','edit_3d'),'string')));
    set(findobj(DNA_analysis,'tag','edit_3d'),'enable','on')
    set(findobj(DNA_analysis,'tag','text8'),'enable','on')
else
    newscope=0;
    set(findobj(DNA_analysis,'tag','edit_3d'),'enable','off')
    set(findobj(DNA_analysis,'tag','text8'),'enable','off')
end
function [begin, end_frames]=select_the_number_of_frames(max_frames)
DNA_analysis=findobj('Tag','DNA_analysis');


%first frame
edit_first_frame=findobj(DNA_analysis,'tag','edit_first_frame');
begin=str2double(get(edit_first_frame,'string'));

%end of frames
end_frames=str2double(get(findobj(DNA_analysis,'tag','edit_frames'),'string'));

%if the file is a scanned image (moves every x frames), select how many frames correspond to a scan
if get(findobj(DNA_analysis,'tag','radio_scan'),'value')==1
    %how many frames correspond to the same image
    scanframes=round(str2double(get(findobj(DNA_analysis,'tag','edit_scanframes'),'string')));
    
    %what scan are we looking at
    start=round(str2double(get(findobj(DNA_analysis,'tag','edit_scan'),'string')));
    
    %set new begin and end_frames
    if end_frames-begin>scanframes
        end_frames=scanframes;
        set(findobj(DNA_analysis,'tag','edit_scanframes'),'string',num2str(scanframes));
    end

    begin=begin+(start-1)*scanframes;
    end_frames=end_frames+(start-1)*scanframes;
end

%couple of precautions
if end_frames<begin
    end_frames=begin;
end

if begin>max_frames
    begin=max_frames;
    end_frames=max_frames;
end
if end_frames<max_frames
    end_frames=max_frames;
end
function [mx,mn]=set_min_max(image)
DNA_analysis=findobj('Tag','DNA_analysis');
radio_black=findobj(DNA_analysis,'tag','radio_black');
radio_white=findobj(DNA_analysis,'tag','radio_white');
radio_first=findobj(DNA_analysis,'tag','radio_first');
radiobutton_image2=findobj(DNA_analysis,'tag','radiobutton_image2');

image2=get(radiobutton_image2,'value');

%if black or white image, minimal is 0 and maximal is 1
if get(radio_black,'value') || get(radio_white,'value')  
    mn=0;mx=1;
end

%Max and min values come here:
hsc=findobj(DNA_analysis, 'tag', 'radiokeepscale');
hsc=get(hsc, 'value'); %keep scale or not

h=findobj(DNA_analysis, 'Style', 'slider');

hed=findobj(DNA_analysis, 'Style', 'edit');
hmx=findobj(hed, 'Tag', 'editscalemax'); %edit box for max(im)
hmn=findobj(hed, 'Tag', 'editscalemin'); %edit box for min(im)

hsl=findobj(DNA_analysis, 'Style', 'slider');
hslmx=findobj(hsl, 'Tag', 'sliderscalemax'); %slider bar for max(im)
hslmn=findobj(hsl, 'Tag', 'sliderscalemin'); %slier bar for min(im)



if ~isempty(hmx) && get(radio_first,'value')==1 || image2==1
    if hsc %keep image scale or not
        mx=str2double(get(hmx, 'String'));
        mn=str2double(get(hmn, 'String'));
        if mx<mn
            mx=max(image(:));mn=min(image(:));
        end
    else
        mx=max(image(:));mn=min(image(:));
        set(hmx, 'String', num2str(mx))
        set(hmn, 'String', num2str(mn))
        set(hslmx, 'max', mx, 'min', mn+1, 'Value', mx, 'SliderStep', [1/(mx-mn+1),0.1]) %100/(mx-mn+1)
        set(hslmn, 'max', mx-1, 'min', mn, 'Value', mn, 'SliderStep', [1/(mx-mn+1),0.1]) %100/(mx-mn+1)
    end
end
function [plotgaussians,plotselected]=select_gaussians(zoom)
DNA_analysis=findobj('Tag','DNA_analysis');

%initialize values
plotselected=[];
plotgaussians=[];

data=DNA_analysis_load_selected_data();
%if the image is scanned, select those that correspond to the correct scan
if get(findobj(DNA_analysis,'tag','radio_scan'),'value')==1
    %how many frames correspond to the same image
    scanframes=round(str2double(get(findobj(DNA_analysis,'tag','edit_scanframes'),'string')));
    
    %what scan are we looking at
    start=round(str2double(get(findobj(DNA_analysis,'tag','edit_scan'),'string')));
    
    %set begin and end frame (between is where the gaussians ought to be
    beginframe=(start-1)*scanframes+1;
    endframe=begin+scanframes-1;
    
    if  ~isempty(data.gaussians)
        %select the gaussians found between endframe and beginframe
        plotgaussians=data.gaussians(data.gaussians(:,1)>=beginframe...
                                    && data.gaussians(:,1)<endframe,:);
    end
    if  ~isempty(data.selected) %if there are selected molecules
        %select the selected molecules found between endframe and beginframe
        plotselected=data.selected(data.selected(:,1)>=beginframe...
                                    && data.selected(:,1)<endframe,:);
    end
else
    if  ~isempty(data.gaussians)
        plotgaussians=data.gaussians;
    end
    if  ~isempty(data.selected) %if there are selected molecules
        plotselected=data.selected;
    end
end


%set the zoom
if  ~isempty(plotgaussians)
    plotgaussians=plotgaussians(plotgaussians(:,2)>zoom(3),:); %remove gaussians before minimal
    plotgaussians=plotgaussians(plotgaussians(:,2)<zoom(4),:); %remove gaussians above maximal
    
    plotgaussians=plotgaussians(plotgaussians(:,3)>zoom(1),:); %remove gaussians before minimal
    plotgaussians=plotgaussians(plotgaussians(:,3)<zoom(2),:); %remove gaussians above maximal    
    
    plotgaussians(:,2)=plotgaussians(:,2)-zoom(3)+1;
    plotgaussians(:,3)=plotgaussians(:,3)-zoom(1)+1;
end
if  ~isempty(plotselected) %plotting selected gaussians
    plotselected=plotselected(plotselected(:,3)>zoom(3),:); %remove selected gaussians before minimal
    plotselected=plotselected(plotselected(:,3)<zoom(4),:); %remove selected gaussians above maximal
    
    plotselected=plotselected(plotselected(:,4)>zoom(1),:); %remove selected gaussians before minimal
    plotselected=plotselected(plotselected(:,4)<zoom(2),:); %remove selected gaussians above maximal  
       
    
    plotselected(:,3)=plotselected(:,3)-zoom(3)+1;
    plotselected(:,4)=plotselected(:,4)-zoom(1)+1;
end
function setsizes(image)
DNA_analysis=findobj('Tag','DNA_analysis');
axes8=findobj(DNA_analysis,'tag','axes8');
imagedata=get(axes8,'Userdata');

imagedata.imsize=[1, size(image,1), 1, size(image,2)];% 1Y(1) 2Y(2) 3X(1) 4X(2)
set(axes8,'Userdata', imagedata);
function bottom(value)
DNA_analysis=findobj('Tag','DNA_analysis');
set(findobj(DNA_analysis,'tag','push_delete_2'),'visible',value);
set(findobj(DNA_analysis,'tag','axes9'),'visible',value);
set(findobj(DNA_analysis,'tag','pushbutton_analyse'),'visible',value);
set(findobj(DNA_analysis,'tag','push_delete_2'),'visible',value);
%---------------------------------------------------------------------------

%image functions
%---------------------------------------------------------------------------
function image=createimage()
DNA_analysis=findobj('Tag','DNA_analysis');
axes8=findobj(DNA_analysis,'tag','axes8');
imagedata=get(axes8,'Userdata');

radio_black=findobj(DNA_analysis,'tag','radio_black');
radio_white=findobj(DNA_analysis,'tag','radio_white');
radio_first=findobj(DNA_analysis,'tag','radio_first');
radiobutton_image2=findobj(DNA_analysis,'tag','radiobutton_image2');

image2=get(radiobutton_image2,'value');

%load image data
data=DNA_analysis_load_selected_data();


if get(radio_black,'value') %make black background
    image=createimagewithnumber(0);
    
    %disable image tools
    setenableimages('off')
    
    return
elseif get(radio_white,'value') %make white background
    image=createimagewithnumber(1);
    
    %disable image tools
    setenableimages('off')
    return
elseif (image2==1 && ~isfield(data.flt,'image2')) || (get(radio_first,'value')==1 && ~exist(data.CCDpath,'file'))
    %if the function is not set, but both images don't exist
    set(radio_black,'value',1);
    image=createimagewithnumber(0);
    setenableimages('off')
    
    return
end

%enable image tools
setenableimages('on')

%If there is a 2nd image to serve as a background, select the 2nd image, no processing
if image2==1
    CCDfile=data.flt.image2;
    imagedata.CCDname=CCDfile;
    set(axes8,'Userdata',imagedata);
elseif get(radio_first,'value')==1 %the inital image ought to be selected
    CCDfile=[data.CCDpath data.CCDname];
    imagedata.CCDname=data.CCDname;
    set(axes8,'Userdata',imagedata);
end

%Check if the imagesize is 3 dimensional, indicating colored images.
newscope=check_if_3d(CCDfile);

%max nr of frames
max_frames=length(imfinfo(CCDfile));

%what images to display (begin and number of frames)
[begin_frame, end_frame]=select_the_number_of_frames(max_frames);

image=sum_image(CCDfile,begin_frame,end_frame,newscope);
image=uint16(image);
function image=createimagewithnumber(value)
%function to make an image with all ones or zeros

DNA_analysis=findobj('Tag','DNA_analysis');
axes8=findobj(DNA_analysis,'tag','axes8');
imagedata=get(axes8,'Userdata');

data=DNA_analysis_load_selected_data();

%if the file exists, use that for the sizes
if exist(data.CCDname, 'file') %Check if the ccd file exists
    img=imread(data.CCDname);
    
    %create image with all zeros
    image=ones(size(img,1),size(img,2)).*value;
else
    %get minimal size of image based on maximal size of gaussiansbackup
    Y2=round(max(data.gaussiansbackup(:,3)))+1;
    X2=round(max(data.gaussiansbackup(:,2)))+1;
    
    %create image with all zeros
    image=ones(X2,Y2).*value;
end
%trick to make 1->0 and 0->1
value=abs(value-1);

%trick to make the image appear black, have 1 white pixel in a corner if
%there is a zoom, put the 1 pixel at the corner of the zoomed image
if isfield(imagedata,'zoom') && ~isempty(imagedata.zoom); image(imagedata.zoom(1),imagedata.zoom(3))=value;
else image(1,1)=value;end
function image=sum_image(CCDfile,begin_frame,end_frame,newscope)
for i=begin_frame:end_frame
    ime=imread(CCDfile, i);
    if newscope>0 
            imtemp(:,:,i-begin_frame+1)=ime(:,:,newscope);
    else
        imtemp(:,:,i-begin_frame+1)=ime;
    end
end
image=sum(imtemp,3);
divl=2*ceil(max(max(image))/65535);
image=image./divl;
%---------------------------------------------------------------------------

%plotting functions
%---------------------------------------------------------------------------
function plotimage(image,mx,mn)
DNA_analysis=findobj('Tag','DNA_analysis');

text_axes8=findobj(DNA_analysis,'tag','text_axes8');
axes8=findobj(DNA_analysis,'tag','axes8');

%get image data (zoom, name etc..)
imagedata=get(axes8,'Userdata');

%display name
if isfield(imagedata,'CCDname');set(text_axes8,'string',imagedata.CCDname);end

%zoom the image
if isfield(imagedata,'zoom') && ~isempty(imagedata.zoom) % && imagedata.zoom(2)<=imagedata.imsize(2) && imagedata.zoom(4)<=data.imsize(4)  This check needs to be elsewhere
    zimage=image(imagedata.zoom(1):imagedata.zoom(2)...
                ,imagedata.zoom(3):imagedata.zoom(4));
else zimage=image;
end

%clear axis
axes(axes8);cla;

%display image
imagesc(zimage,[mn mx])
axis image;axis off;colormap(bone)

%workaround for silly bug, axes sometimes lose tag
set(axes8,'tag','axes8');
set(axes8,'Userdata',imagedata);
function plotgaussians()
colors=['rx';'gx';'yx']; %needed for plotting multiple color localization data

DNA_analysis=findobj('Tag','DNA_analysis');
axes8=findobj(DNA_analysis,'tag','axes8');
axes9=findobj(DNA_analysis,'tag','axes9');

%get image data (zoom, name etc..)
imagedata=get(axes8,'Userdata');

%check foor zoom function
if isfield(imagedata,'zoom') && ~isempty(imagedata.zoom);
    zoom=imagedata.zoom;
else
    zoom=imagedata.imsize; %if there is no zoom saved, set zoom to standard imagedata
end

[plotgaussians,plotselected]=select_gaussians(zoom);

%axes8 (image axis), plot the gaussians
axes(axes8);
hold on

%start plotting

edit_markersize=findobj(DNA_analysis,'tag','edit_markersize');
markersize=str2double(get(edit_markersize,'string'));

%check if field is multiple frames
if isfield(imagedata, 'multiple') % NEED TO ADD
    for yy=1:3%plot for each color
        temp_plotgaussians(plotgaussians(:,12)==yy);
        if ~isempty(temp_plotgaussians)
            plot(plotgaussians(:,2), plotgaussians(:,3), colors(yy,:),'MarkerSize',markersize);
        end
    end
else
    if ~isempty(plotgaussians)
       plot(plotgaussians(:,2), plotgaussians(:,3), colors(1,:),'MarkerSize',markersize);
    end
end

%stop hold
hold off

%plot selected molecules
if  ~isempty(plotselected) %plotting selected gaussians

    %axes8 (image axis), plot the selected gaussians
    axes(axes8);
    hold on
    plot(plotselected(:,3),plotselected(:,4),'yo','MarkerSize',markersize); 
    hold off
    
    %2nd graph, only for selected molecules, plot selected gaussians
    axes(axes9);cla;
    plot(plotselected(:,3),plotselected(:,4),'rx','MarkerSize',markersize+2);
    title([num2str(size(plotselected,1)) ' molecules'],'fontsize',12, 'Fontname', 'candara');
    bottom('on');
    
    %reverse y-axis so it matches the image better
    set(gca,'YDir','reverse');

else
    %if no molecules , clear the bottom axis
    axes(axes9);cla
    bottom('off');
end

%workaround for silly bug, axes sometimes lose tag
set(axes8,'tag','axes8');
set(axes8,'Userdata',imagedata);
set(axes9,'tag','axes9');
%---------------------------------------------------------------------------