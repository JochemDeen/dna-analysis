function DNA_analysis_save_selected_data(data)
%current directory
cfp=pwd;

DNA_analysis=findobj('Tag','DNA_analysis');
listbox2=findobj(DNA_analysis,'tag','listbox2');
%get selected filename
i=get(listbox2,'value');

%get the filename and path
filedata=get(listbox2,'Userdata');
fn=filedata.list(i).fn;
fp=filedata.list(i).fp;

%get the data from 'data'
CCDpath=data.CCDpath;
CCDname=data.CCDname;
flt=data.flt;
gaussians=data.gaussians;
gaussiansbackup=data.gaussiansbackup;
selected=data.selected;

%go the fp directory and save fn
cd(fp);
eval(['save ' fn '  CCDpath CCDname flt gaussians gaussiansbackup selected'])

%go back to current directory
cd(cfp);