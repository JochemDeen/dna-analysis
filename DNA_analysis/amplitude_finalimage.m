function amplitude=amplitude_finalimage()
psfWidth=1.6;
pfa=25;

[fn,fp] = uigetfile({'*.tif';'*.TIF'},'load bleaching files');
cd(fp)

file=[fp fn];
frames=length(imfinfo(file));

% im_begin=imread(file, 2,1);
im_end=imread(file, 2,frames);

Cpos = LocalizerMatlab('localize', psfWidth, 'glrt', pfa, '2DGauss',im_end);

if ~isempty(Cpos)
%     Lox=[Cpos(:,5),Cpos(:,4)];
    intensity=Cpos(:,2);
    amplitude=mean(intensity);
else
    amplitude=0;
end

