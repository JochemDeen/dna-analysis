function max_angle=detect_angle_molecule(data)

%rotate until a maximum of 180 degrees
maxangle=180;

%with a step of 1 degree
angle_step=1;

%initialize
num_sites_max=zeros(maxangle/angle_step,1);

for i=1:angle_step:maxangle
    
    %calculate angle in radian
    angle=(i/180)*(pi);
   
    %create rotational matrix
    R=[cos(angle), -sin(angle);sin(angle),cos(angle)];
    
    %rotate data
    data2=data*R;
    
    %create histogram of data over the entire x axis
    histogram_sites=hist(data2(:,1),min(data2(:,1)):max(data2(:,1)));
    
    %calculate maximal value of histogram
    num_sites_max(i)=max(histogram_sites);

end

%calculate index for maximal x-axis values
[~,index]=max(num_sites_max);

%pick the angle from index of maximal value
angles=1:angle_step:maxangle;
max_angle_degrees=angles(index);

%convert to radian
max_angle=(max_angle_degrees/180)*pi;