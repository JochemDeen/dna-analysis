function X=create_Xaxis_DNAM(totalsites,gaussian_width,step)

%Make totallist corrected with maximal stretch factor
%totalsites=[reference;sites];

XMIN=floor(min([totalsites;0])-round(gaussian_width*5));
XMAX=ceil(max(totalsites)++round(gaussian_width*5));

X=XMIN:step:XMAX;