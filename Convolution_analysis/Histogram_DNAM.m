function Histogram_DNAM(cases)
switch cases
    case 'make_histogram'
        make_histogram();
    case 'select_molecules'
        select_molecules();
end

function make_histogram()
fitter=findobj('tag','fitter');

%enable select button
pushbutton_select=findobj(fitter,'tag','pushbutton_select');
set(pushbutton_select,'enable','on');
%bins= newid('binsize (default=40)','image size',1,{'40'}); %inputdlg

%input dialog asking for the bin_size
bins= inputdlg('binsize (default=40)','image size',1,{'40'}); %inputdlg
binsize=str2double(bins);

tic
%Load userdata
Data=get(fitter,'Userdata');

n=0;

%create list of all fragment sizes found in the DNA molecules
for i=1:size(Data,2)
    a=Data(i).Mlocation;
    b=ones(1,size(a,1));
    c=tril(a*b-b'*a');
    d=reshape(c,1,[]);
    d(d==0)=[];
    Matrix(n,1:size(d,2))=c(3,:)';
end

%set x axis using max value and bin size
maxmatrix=max(max(Matrix));
x=0:binsize:maxmatrix;
Data(1).Matrix=[Matrix];
Data(1).binsize=binsize;

%convert matrix
Matrix(Matrix==0)=NaN;
Matrix=Matrix(:,2);

%Make histogram of matrix
N=hist(Matrix',x);
%N(N>0)=1;
Data(1).N=N;

YY=N;
%[~, idx]=max(YY);
%disp(['max at: ', num2str(x(idx))])
%YY=sum(N,2);

%found out what the maximal value is
[~, idx]=max(YY);
disp(['max at: ', num2str(x(idx))])

set(fitter,'Userdata',Data);
%XX=reshape(Matrix,1,[]);
%XX(XX==0)=[];
%[nelements,centers] = hist(XX,bins);


%plot the results
haxes=findobj(fitter,'tag','axes1');
axes(haxes);cla;
%bar(centers,nelements,1)
bar(x,YY,1)
xlim([0 maxmatrix]);
set(haxes,'tag','axes1');
toc

radio_map=get(fitter,'tag','radio_map');

%if a reference is loaded make a histogram for how a reference would look.
if get(radio_map,'value')==1;
    base_size=0.34; %nm
    a=Data(1).reference*base_size;
    b=ones(1,size(a,1));
    c=tril(a*b-b'*a');
    d=reshape(c,1,[]);
    d(d==0)=[];
    Matrix(i,1:size(d,2))=d;
    x=0:binsize:max(max(Matrix));
    
    Matrix(Matrix==0)=NaN;
    N=hist(Matrix',x);
    YY=sum(N,2);

    figure
    %bar(centers,nelements,1)
    bar(x,YY,1)
end


function select_molecules
fitter=findobj('tag','fitter');
Data=get(fitter,'Userdata');

%select molecules
haxes=findobj(fitter,'tag','axes1');
axes(haxes)
[gx1, gy1, op]=ginput(1);
column=round(gx1/Data(1).binsize)+1;


%select molecules
[r,c]=find(Data(1).N(column,:)>0);
Matrix=Data(1).Matrix;
Molecules=Data(1).Matrix(c,:);
[~,index]=min(abs(Molecules-gx1)');
sites=diag(Molecules(:,index));
loc1=mean(sites);
M1=[0; loc1];
for i=1:size(c,2)
    Datatemp(i)=Data(c(i));
end

%save data
pushbutton_select=findobj(fitter,'tag','pushbutton_select');
set(pushbutton_select,'Userdata',Datatemp);


%Load method and axes1
popup_fit=findobj(fitter,'tag','popup_fit');
method_val=get(popup_fit,'value'); %1, Convolution; 2, MinSquare
axes1=findobj(fitter,'tag','axes1');

%load reference
Reference=Data(1).reference; 

%Load sites
Sites=[];
for i=1:size(Datatemp,2)
    Sites=[Sites;Datatemp(i).Mlocation];
end

%total molecules
total_mols=size(Datatemp,2);


pushbutton_stop=findobj(fitter,'tag','pushbutton_stop');

for i=1:total_mols
    if strcmp(get(pushbutton_stop,'visible'),'off')
        break
    end
    
    %set variable for current plot
    loaded=i;
    set(axes1,'Userdata',loaded);
    
    Compare_DNAM(method_val,Reference,Sites(i));
    update_GUI_DNAM('plot_line')
end
update_GUI_DNAM('enable_plot_buttons')
plotter_DNAM('start_plotter')

