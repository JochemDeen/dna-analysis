function Compare_DNAM(method_value,Reference,Molecule)

%Method comparison
method_comparison={'Convolution','MinSquare','Subtraction'};

fitter=findobj('tag','fitter');
Data=get(fitter,'Userdata');
pushbutton_stop=findobj(fitter,'tag','pushbutton_stop');

%Stretch factors
    edit_stretchstp=findobj(fitter,'tag','edit_stretchstp');
stretchstep=str2double(get(edit_stretchstp,'string'));
    edit_min=findobj(fitter,'tag','edit_min');
min_stretch=str2double(get(edit_min,'string'));
    edit_max=findobj(fitter,'tag','edit_max');
max_stretch=str2double(get(edit_max,'string'));

stretch_total=min_stretch:stretchstep:max_stretch;
stretch_total_nr=size(stretch_total,2);

%Gaussian width
edit_width=findobj(fitter,'tag','edit_width');
gaussian_width=str2double(get(edit_width,'string'));

%reference width
edit_ref=findobj(fitter,'tag','edit_ref');
reference_width=str2double(get(edit_ref,'string'));

%line function
method_line={'Gaussian'; 'Block'};
popup_data=findobj(fitter,'tag','popup_data');
popup_ref=findobj(fitter,'tag','popup_ref');
val_mol=get(popup_data,'value');
val_ref=get(popup_ref,'value');

%convert Reference to nanometer
base_size=0.34; %nm/bp
Reference=Reference*base_size;

% Molecule=Reference(64:77)*1.45;
% Molecule=Molecule-min(Molecule);

%Get offset boundaryies
edit_offset_low=findobj(fitter,'tag','edit_offset_low');
offset_low=str2double(get(edit_offset_low,'string'));
edit_offset_up=findobj(fitter,'tag','edit_offset_up');
offset_up=str2double(get(edit_offset_up,'string'));
edit_stepsize=findobj(fitter,'tag','edit_stepsize');
step=str2double(get(edit_stepsize,'string'));

%Create X axis for alignment (note Reference will get stretched)
Xref=create_Xaxis_DNAM(Reference.*max_stretch,gaussian_width,step);

Xdata=create_Xaxis_DNAM(Molecule,gaussian_width,step);


%initialize cutsize
cutsize=100000/step;
%cutsize=5000/step;

%offset
% //TODO set correct offset values!
if ~isnan(offset_low) && -offset_low>floor(size(Xdata,2)/2);low=offset_low/step+size(referenceline,2);else low=1;end
if ~isnan(offset_up);up=offset_up/step+size(referenceline,2);else up=size(Xref,2); end


%Data for waitbar
axes1=findobj(fitter,'tag','axes1');
run=get(axes1,'Userdata');
total_mols=size(Data,2);

%Run for both orientations
for D=-1:2:1
    %orientation
    Molecule_fit=Molecule*D-min(D*Molecule);
    
    %waitbar
    h = waitbar(0,['Fitting molecule ' num2str(run) ' of ' num2str(total_mols) ' ( 0 / ' num2str(stretch_total_nr) ' - direction ' num2str(D) ')']);

    %Create a function for the molecule
    Molecule_line=createline_DNAM(method_line{val_mol},Molecule_fit,Xdata,1,gaussian_width);
    
    %Stretch the molecule
    i=0;
    for stretch=min_stretch:stretchstep:max_stretch
        i=i+1;


        %Comparison
        if size(Xref,2)<cutsize
            
            %Create a function for the referenceline
            Reference_line=createline_DNAM(method_line{val_ref},Reference,Xref,stretch,reference_width);
        
            DNA_comp(i,:)=compare(method_comparison{method_value},Molecule_line,Reference_line); %moves Line relative to referenceline, starting with only last of Line and first of referenceline overlapping
        
        elseif size(Xdata,2)<cutsize
            
            %Reference is computed in function
            
            DNA_comp(i,:)=comparebig(method_comparison{method_value},method_line{val_ref},Molecule_line,Reference,Xref,stretch,reference_width,cutsize);
            low=1;
            up=size(DNA_comp,2);
            
        else 
            errordlg('Error in comparing. Molecule and data too big. Consider resetting the cutsize, or use a smaller molecule') 
            set(pushbutton_stop,'visible','off')
            break
        end
            
        waitbar(i/stretch_total_nr,h,['Fitting molecule ' num2str(run) ' of ' num2str(total_mols) ' ( ' num2str(i) ' / ' num2str(stretch_total_nr) ' - direction ' num2str(D) ')']);
        if strcmp(get(pushbutton_stop,'visible'),'off')
            break
        end
    end
    close(h)
    
    %Save the maximal values and the row
    
    %colmax =   maximal score for all offset 
    %rowI =     corresponding stretch index
    [colmax,rowI]=max(DNA_comp(:,low:up)); %get maximal value for all rows (stretch factors), with columns between limit (offset)
    
    %save all offset values in scorerow D=1 is -1 and D=3 is +1 orientation
    scorerow(:,D+2)=colmax;
    
    %rowmax =   maximal offset score
    %colI =     corresponding index (offset index)
    [rowmax(D+2),colI(D+2)]=max(colmax);%rowindex=stretch, columnindex=offset
    
    %select stretch index corresponding to max(max(score))
    rowI2(D+2)=rowI(colI(D+2)); %rowindex=stretch, columnindex=offset
end

if rowmax(3)>rowmax(1); fullscore=scorerow(:,3);score=rowmax(3);QWX=3; D=1; Data(run).flt.conv=rowmax(3); else QWX=1; D=-1; Mloc(run).flt.conv=rowmax(1);fullscore=scorerow(:,1); score=rowmax(1); end

%save offset score
%get the index value
Oindex=colI(QWX);

%transform to actual value: conv(fliplr(B),A)-->offset=size(B)-size(A)/2-index
Osteps=size(Xref,2)-floor(size(Xdata,2)/2)-Oindex; %note need to input limiting value!

%convert to nm
O=Osteps*step; %Final offset value

%disp(['score: ', num2str(score), ' offset: ', num2str(O)]);
S=stretch_total(rowI2(QWX)); %Final stretch value


%Save data
Data(run).flt.Stretch=S;
Data(run).flt.Direction=D;
Data(run).flt.Offset=O;
Data(run).flt.relativeto='sequence';
Data(run).flt.score=score;
%calculate SNR
background=median(fullscore);
noise=mean((fullscore-background).^2);
Data(run).flt.snr=(score-background)^2/noise;

Mlocation2=Molecule*D-min(D*Molecule);
Mlocation2=flipud(Mlocation2);
Mlocation2=Mlocation2+O;
Mlocation2=Mlocation2*S;
Data(run).Mlocation2=Mlocation2; %(Mlocation2 is a list of enzyme locations IN bp!!!!)
set(fitter,'Userdata',Data);

% if get(findobj(fitter,'tag','radio_rndm'),'value')==1
%         rndmsites=findobj('Tag','randomsites');
%         set(rndmsites,'Userdata',Data);
%     else
%         set(fitter,'Userdata',Data);
%     end

function DNA_comp=compare(method,Molecule_line,Reference_line)
reference=sqrt(Reference_line);
data=sqrt(Molecule_line);
switch method
    case 'Convolution'
        %if size(Reference_line,1)<size(Molecule_line,1)
        %DNA_comp=conv(fliplr(reference),data,'same');
        DNA_comp=conv(fliplr(reference),data,'same');
    case 'MinSquare'
        DNA_comp=xcorr(data,reference);
end
        
        
function score=comparebig(method,methodline,Molecule_line,Reference,Xref,stretch,reference_width,cutsize)
switch method
    case 'Convolution'
        
        %because only the exact overlap can be used, the real cut size is
        %less than the set cut size, by the size of the data.
        realcutsize=cutsize-size(Molecule_line,2);
        
        %calculate number of cuts
        %cuts=floor(size(Xref,2)/realcutsize);
        cuts=floor((size(Xref,2)-size(Molecule_line,2))/realcutsize);

        
        %cut X axis, reference
        Xreftemp=Xref(1,(end-cutsize):end);
        
        %Create a function based on cut X-axis
        tempref=createline_DNAM(methodline,Reference,Xreftemp,stretch,reference_width);
        
        %calculate convolution score for this piece
        %scoreend=conv(fliplr(tempref),Molecule_line,'same');
        scoreend=compare(method,Molecule_line,tempref);
        
        %calculate end of this piece, remove molecule_line, because that
        %will be calculated in middle
        ending=size(tempref,2)-floor(size(Molecule_line,2)/2);
        
        %create end, beginning removed
        scoreend=scoreend(1:ending);
        
        scoremiddle=[];
        for i=2:cuts
            %cut reference
            %tempref=Reference_line(1,(end-cutsize*i):(end-cutsize*(i-1)-1));
            
            %cut X axis, reference
            Xreftemp=Xref(1,(end-realcutsize*(i-1)-cutsize+1):(end-realcutsize*(i-1)-1));

            %Create a function based on cut X-axis
            tempref=createline_DNAM(methodline,Reference,Xreftemp,stretch,reference_width);
            
            %calculate convolution score for this piece
            %scoremiddle_temp=conv(fliplr(tempref),Molecule_line,'same');
            scoremiddle_temp=compare(method,Molecule_line,tempref);
            
            %beginning is the half of molecule length
            beginning=ceil(size(Molecule_line,2)/2);
            
            %ending is size of cut reference - half of molecule length
            ending=size(tempref,2)-floor(size(Molecule_line,2)/2);
            
            %add score to growing middle score
            scoremiddle=[scoremiddle, scoremiddle_temp(beginning:ending)];
        end
        %cut reference
        %tempref=Reference_line(1,1:end-1);
        %cut X axis, reference
        Xreftemp=Xref(1,1:(end-realcutsize*cuts)-1);

        %Create a function based on cut X-axis
        tempref=createline_DNAM(methodline,Reference,Xreftemp,stretch,reference_width);
        
        %calculate convolution score for this piece
        %scorebegin=conv(fliplr(tempref),Molecule_line,'same');
        scorebegin=compare(method,Molecule_line,tempref);
        
        %calculate beginning, remove molecule_line because that will be calculated in the middle section 
        beginning=ceil(size(Molecule_line,2)/2);
        
        scorebegin=scorebegin(beginning:end);

        %score=[scorebegin,scoremiddle,scoreend];
        %needs to be established in reverse
        score=[scoreend, scoremiddle, scorebegin];
        
        
        
    case 'MinSquare'
        errordlg('not finished for big molecules');
end

function line=createbigline_DNAM(methodline,Reference,Xref,stretch,reference_width)
minval=min(Xref);
maxval=max(Xref);
sites=Reference(Reference>=minval && Reference<=maxval);

line=createline_DNAM(methodline,sites,Xref,stretch,reference_width);