function varargout = viewProfiles(varargin)
% VIEWPROFILES MATLAB code for viewProfiles.fig
% This function takes a 2D matrix of values, and allows the user to use a
% slider to flip through the data and see individual horizontal and
% vertical cuts of the data.

% Z-data (your 2-D matrix) should be loaded first. Pushing the 'Choose Z-Data'
% button will bring a popup menu asking you for the variable name in the 
% workspace. Type the variable name and the plot loads. By default, columns are
% the x-values and rows the y-columns. If you have row vectors or x and y
% values, buttons exist so you can use them as the axis for your plot.

% The function myIsField.m is needed for error checking

%      VIEWPROFILES, by itself, creates a new VIEWPROFILES or raises the existing
%      singleton*.
%
%      H = VIEWPROFILES returns the handle to a new VIEWPROFILES or the handle to
%      the existing singleton*.


% Last Modified by GUIDE v2.5 24-May-2012 18:51:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @viewProfiles_OpeningFcn, ...
                   'gui_OutputFcn',  @viewProfiles_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before viewProfiles is made visible.
function viewProfiles_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to viewProfiles (see VARARGIN)

% Choose default command line output for viewProfiles
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes viewProfiles wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = viewProfiles_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function xselector_Callback(hObject, eventdata, handles) 
% hObject    handle to xselector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

cxindex = round(get(hObject, 'Value'));
cyindex = round(get(handles.yselector, 'Value'));
usrvar = handles.zdata;
ypnts = length(usrvar(:,1));
xpnts = length(usrvar(1,:));
cxvalue = handles.xdata(cxindex);
cyvalue = handles.ydata(cyindex);
set(handles.xdisplay, 'String', num2str(cxvalue));

axes(handles.imageplot)  
imagesc(handles.xdata, handles.ydata,usrvar)
set(gca,'YDir', 'normal')

maxz = max(max(handles.zdata));
x4cyline = cxvalue*ones(1,ypnts);
y4cxline = cyvalue*ones(1,xpnts);
z4cyline = (maxz+1)*ones(1,xpnts);
z4cxline = (maxz+1)*ones(1,ypnts);
line(handles.xdata, y4cxline, z4cyline,'Color','k');
line(x4cyline, handles.ydata, z4cxline,'Color','k');
    
plot(handles.constantXplot, handles.ydata,usrvar(:,cxindex));
axes(handles.constantXplot)
xlim([min(handles.ydata),max(handles.ydata)])
guidata(hObject, handles);


% --- Executes on slider movement.
function yselector_Callback(hObject, eventdata, handles)
% hObject    handle to yselector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

cyindex = round(get(hObject, 'Value'));
cxindex = round(get(handles.xselector, 'Value'));
usrvar = handles.zdata;
ypnts = length(usrvar(:,1));
xpnts = length(usrvar(1,:));
cxvalue = handles.xdata(cxindex);
cyvalue = handles.ydata(cyindex);
set(handles.ydisplay, 'String', num2str(cyvalue));

axes(handles.imageplot)
imagesc(handles.xdata, handles.ydata,usrvar)
set(gca,'YDir', 'normal')

maxz = max(max(handles.zdata));
x4cyline = cxvalue*ones(1,ypnts);
y4cxline = cyvalue*ones(1,xpnts);
z4cyline = (maxz+1)*ones(1,xpnts);
z4cxline = (maxz+1)*ones(1,ypnts);
line(handles.xdata, y4cxline, z4cyline,'Color','k');
line(x4cyline, handles.ydata, z4cxline,'Color','k');
    
plot(handles.constantYplot, handles.xdata,usrvar(cyindex,:));
axes(handles.constantYplot)
xlim([min(handles.xdata),max(handles.xdata)])
guidata(hObject, handles);



% --- Executes on button press in chooseZdata.
function chooseZdata_Callback(hObject, eventdata, handles)
% hObject    handle to chooseZdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Reads in user data

set(handles.yselector, 'Value', 1);
set(handles.xselector, 'Value', 1);

zdataname = inputdlg('Enter the z-data variabe');
ws_vars = evalin('base', 'who');

if isequal(str2double(zdataname{1}),0)
    
    %do nothing
    
elseif ismember(zdataname{1},ws_vars)
  usrvar = evalin('base', zdataname{1});
  
  if ~isequal(length(size(usrvar)),2)
      
      beep
      errordlg('Variable dimensions are incorrect')
      
  else
    %Done error checking, now initialize values
    handles.zdata = usrvar;
    ypnts = length(usrvar(:,1));
    xpnts = length(usrvar(1,:));
    set(handles.yselector, 'Value', 1);
    set(handles.yselector, 'Max', ypnts);
    set(handles.yselector, 'Min', 1);
    set(handles.yselector, 'SliderStep', [1/ypnts, 1/ypnts*ceil(ypnts/10)]);
    set(handles.xselector, 'Max', xpnts);
    set(handles.xselector, 'Value', 1);
    set(handles.xselector, 'Min', 1)
    set(handles.xselector, 'SliderStep', [1/xpnts, 1/xpnts*ceil(xpnts/10)]);
    set(handles.ydisplay, 'String', '1');
    set(handles.xdisplay, 'String', '1');

    cxvalue = 1.5;  %1.5 just so we can see them when plot 1st opens
    cyvalue = 1.5;
    % make a default x and y data until the user imports their own.
    handles.xdata = 1:length(usrvar(1,:));
    handles.ydata = 1:length(usrvar(:,1));
    
    %Plot the 2-D matrix
    axes(handles.imageplot)
    imagesc(usrvar)
    set(gca,'YDir', 'normal')
    
    %Make the constant vectors for making lines across the plot
    maxz = max(max(handles.zdata));
    x4cyline = cxvalue*ones(1,ypnts);
    y4cxline = cyvalue*ones(1,xpnts);
    %The z values are greater than the max value of z or else it may be
    %underneath the plot and not visible.
    z4cyline = (maxz+1)*ones(1,xpnts);
    z4cxline = (maxz+1)*ones(1,ypnts);
    line(handles.xdata, y4cxline, z4cyline,'Color','k');
    line(x4cyline, handles.ydata, z4cxline,'Color','k');
    
    plot(handles.constantYplot, usrvar(1,:));
    axes(handles.constantYplot)
    xlim([1,xpnts])
    plot(handles.constantXplot, usrvar(:,1));
    axes(handles.constantXplot)
    xlim([1,ypnts])
    
    
  end  

else
    beep
    errordlg('Variable does not exist')
    
end

guidata(hObject, handles);



function ydisplay_Callback(hObject, eventdata, handles)
% hObject    handle to ydisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ydisplay as text
%        str2double(get(hObject,'String')) returns contents of ydisplay as a double



function xdisplay_Callback(hObject, eventdata, handles)
% hObject    handle to xdisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xdisplay as text
%        str2double(get(hObject,'String')) returns contents of xdisplay as a double



% --- Executes on button press in chooseXdata.
function chooseXdata_Callback(hObject, eventdata, handles)
% hObject    handle to chooseXdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~myIsField(handles, 'zdata')
    
    beep
    errordlg('Z-data must be imported before X or Y data!')
 
else    
    xdataname = inputdlg('Enter the x-data variabe as 1 by n vector');
    ws_vars = evalin('base', 'who');


    if isequal(str2double(xdataname{1}),0)
    %do nothing
    

    elseif ismember(xdataname{1},ws_vars)
        usrx = evalin('base', xdataname{1});
  
        if ~isequal(size(usrx),size(handles.xdata))
      
         beep
         errordlg('Variable dimensions are incorrect')
      
        else
      
            handles.xdata = usrx;
            %Get the data indexes from the sliders and set the z-data
            cxindex = round(get(handles.xselector, 'Value'));
            cyindex = round(get(handles.yselector, 'Value'));
            usrvar = handles.zdata;
            ypnts = length(usrvar(:,1));
            xpnts = length(usrvar(1,:));
            
            %get the actual values of the x and y data
            cxvalue = handles.xdata(cxindex);
            cyvalue = handles.ydata(cyindex);
            
            %update the image plot
            axes(handles.imageplot)
            imagesc(handles.xdata, handles.ydata,usrvar)
            set(gca,'YDir', 'normal')
            
            %setup the slice lines to be above the image plot so we see
            %them
            maxz = max(max(handles.zdata));
            x4cyline = cxvalue*ones(1,ypnts);
            y4cxline = cyvalue*ones(1,xpnts);
            z4cyline = (maxz+1)*ones(1,xpnts);
            z4cxline = (maxz+1)*ones(1,ypnts);
            line(handles.xdata, y4cxline, z4cyline,'Color','k');
            line(x4cyline, handles.ydata, z4cxline,'Color','k');
            %Update the 1-D plot
            plot(handles.constantYplot, handles.xdata,usrvar(cyindex,:));
            axes(handles.constantYplot)
            xlim([min(handles.xdata),max(handles.xdata)])
            set(handles.xdisplay, 'String', num2str(handles.xdata(cxindex)));
        end
    else
    beep
    errordlg('Variable does not exist')
    
    end
end
      
      
guidata(hObject, handles);


% --- Executes on button press in chooseYdata.
function chooseYdata_Callback(hObject, eventdata, handles)
% hObject    handle to chooseYdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


if ~myIsField(handles, 'zdata')
    
    beep
    errordlg('Z-data must be imported before X or Y data!')
    
else
    ydataname = inputdlg('Enter the y-data variabe as 1 by n vector');
    ws_vars = evalin('base', 'who');
    
    
    if isequal(str2double(ydataname{1}),0)
        %do nothing
   

    elseif ismember(ydataname{1},ws_vars)
        usry = evalin('base', ydataname{1});
  
        if ~isequal(size(usry),size(handles.ydata))
      
            beep
            errordlg('Variable dimensions are incorrect')
      
        else
      
            handles.ydata = usry;
      
            cxindex = round(get(handles.xselector, 'Value'));
            cyindex = round(get(handles.yselector, 'Value'));
            usrvar = handles.zdata;
            ypnts = length(usrvar(:,1));
            xpnts = length(usrvar(1,:));
            cxvalue = handles.xdata(cxindex);
            cyvalue = handles.ydata(cyindex);

            axes(handles.imageplot)
            imagesc(handles.xdata, handles.ydata,usrvar)
            set(gca,'YDir', 'normal')

            maxz = max(max(handles.zdata));
            x4cyline = cxvalue*ones(1,ypnts);
            y4cxline = cyvalue*ones(1,xpnts);
            z4cyline = (maxz+1)*ones(1,xpnts);
            z4cxline = (maxz+1)*ones(1,ypnts);
            line(handles.xdata, y4cxline, z4cyline,'Color','k');
            line(x4cyline, handles.ydata, z4cxline,'Color','k');
    
            plot(handles.constantXplot, handles.ydata,usrvar(:,cxindex));
            axes(handles.constantXplot)
            xlim([min(handles.ydata),max(handles.ydata)])
            set(handles.ydisplay, 'String', num2str(handles.ydata(cyindex)));
        end
    else
        beep
        errordlg('Variable does not exist')
    
    end
end
      
      
guidata(hObject, handles);
% --- Executes during object creation, after setting all properties.
function yselector_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to yselector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function xselector_CreateFcn(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to xselector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function xdisplay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xdisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function ydisplay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ydisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end