function [Sites,Reference]=createsites_DNAM()
fitter=findobj('tag','fitter');

Data=get(fitter,'Userdata');

if get(findobj(fitter,'tag','radio_rndm'),'value')==1
    Sites=Randomizer_sites();
else
    Sites=[];
    for i=1:size(Data,2)
        Sites(i).Molecule=Data(i).Mlocation;
    end
end

%base_size=0.34; %nm
Reference=Data(1).reference;%*base_size; 



function [sites]=Randomizer_sites()

randomsites

rndmsites=findobj('Tag','randomsites');
edit_nrmolecules=findobj(rndmsites,'tag','edit_molecules');
edit_size=findobj(rndmsites,'tag','edit_size');
edit_spreadsize=findobj(rndmsites,'tag','edit_spreadsize');
edit_sites=findobj(rndmsites,'tag','edit_sites');
edit_spreadsites=findobj(rndmsites,'tag','edit_spreadsites');

sites(STR2NUM(get(edit_nrmolecules,'string'))).molecule=[];
for i=1:str2num(get(edit_nrmolecules,'string'))
   Mol_length=random('normal', STR2NUM(get(edit_size,'string')),STR2NUM(get(edit_spreadsize,'string')));
   number_sites=round(random('normal',STR2NUM(get(edit_sites,'string')),STR2NUM(get(edit_spreadsites,'string'))));
   sites(i).molecule=rand(number_sites,1)*Mol_length;
   sites(i).molecule=sort(sites(i).molecule);
end