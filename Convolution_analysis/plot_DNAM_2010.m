function varargout = plot_DNAM_2010(varargin)
% Plotter to create a histogram from data fitted with fit_DNAM_2010
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @plot_DNAM_2010_OpeningFcn, ...
                   'gui_OutputFcn',  @plot_DNAM_2010_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% Mlocation2 is a list of enzyme locations IN bp!!!!
% Mlocation is original list of enzyme IN nm!!!!!!!! Convert to bp by: 
%
% Mlocation=Mlocation*Direction-min(Direction*Mlocation);
% Mlocation=Mlocation+Offset;
% Mlocation=Mlocation*Stretch;


% --- Executes just before plot_DNAM_2010 is made visible.
function plot_DNAM_2010_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to plot_DNAM_2010 (see VARARGIN)

% Choose default command line output for plot_DNAM_2010
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes plot_DNAM_2010 wait for user response (see UIRESUME)
% uiwait(handles.plotter);


% --- Outputs from this function are returned to the command line.
function varargout = plot_DNAM_2010_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


function draw_histo_figure_reference
fitter=findobj('tag','fitter');
Data=get(fitter,'Userdata');

plotter=findobj('tag','plotter');
edit_bin=findobj(plotter,'tag','edit_bin');
bin_size=str2double(get(edit_bin,'string')); %get value for bin size (in nm)


Totalmol=[];
for i=1:size(Data,2)
    Totalmol=[Totalmol;Data(i).Mlocation2];
end
if strcmp(Data(1).flt.relativeto,'sequence')
    %base_size=0.34; %nm
    M1=Data(1).reference;%*base_size; 
%    Totalmol=[Totalmol;M1];
end

Xvector=create_Xaxis_DNAM(M1,Totalmol,0,bin_size);


histM1=hist(M1,Xvector);
histM1_norm=histM1;
histM1_norm(histM1_norm>1)=1; %Make all peakes size of 1

histdata=hist(Totalmol,Xvector);
histdata_real=histdata.*histM1_norm; %Only real peakes (i.e. peakes that are also in the reference) remain, the rest is set to 0
histremain=histdata-histdata_real;  %remaining peakes
histdata_realnorm=histdata_real./(size(Data,2)*histM1); %Normalize peakes with respect to the reference
%histdata_realnorm(histdata_realnorm==Inf)=0; %quietly ignore divide by 0, should never happen
histdata_realnorm(isnan(histdata_realnorm))=0; %quietly ignore 0 divided by 0

hismol=histdata_realnorm-histremain;
hismol2=histdata_realnorm*size(Data,2)-histremain;
toomuch=hismol(hismol>1);
error=sum(histremain)+sum(toomuch)-size(toomuch,2);
correct=sum(histdata_realnorm)-sum(toomuch)+size(toomuch,2);

perc_correct=100*(sum(histdata_real)-sum(toomuch)+size(toomuch,2))/sum(histdata);

text_data=findobj(plotter,'tag','text_data');
set(text_data,'string',['Covered (norm): ' num2str(correct,4) char(10) 'Incorrect: ' num2str(error,4) char(10) 'Correct: ' num2str(perc_correct,2) ' %' char(10) 'Locations:' num2str(sum(histdata))]); %#ok<NOPRT,NOPRT>
%Totalmol=sort(Totalmol);

haxes=findobj(plotter, 'tag', 'axes1');
axes(haxes);cla
%[hismol(:,2),hismol(:,1)]=hist(Totalmol,nrbins);
%bar(hismol(:,1),hismol(:,2));
bar(Xvector,hismol2);
title(['Concensus map of ' num2str(size(Data,2)) ' molecules' ],'fontsize',12, 'Fontname', 'candara');
xlabel('Distance (nm)');
axis([min(Xvector) max(Xvector) min(hismol2) max(hismol2)])
set(haxes, 'tag', 'axes1');

haxes=findobj(plotter, 'tag', 'axes2');
axes(haxes);cla
bar(Xvector,histM1_norm,'red');
axis off
set(haxes, 'tag', 'axes2');

edit_threshold=findobj(plotter, 'tag', 'edit_threshold');
threshold=str2double(get(edit_threshold,'string'));
histdata_thr=histdata;
histdata_thr(histdata_thr<threshold)=0;
histdata_thr(histdata_thr>1)=1;

haxes=findobj(plotter, 'tag', 'axes3');
axes(haxes);cla
bar(Xvector,histdata_thr,'green');
axis off
set(haxes, 'tag', 'axes3');






function draw_histo_figure_self
fitter=findobj('tag','fitter');
Data=get(fitter,'Userdata');

plotter=findobj('tag','plotter');
edit_bin=findobj(plotter,'tag','edit_bin');
bin_size=str2double(get(edit_bin,'string')); %get value for bin size (in nm)
push_difference=findobj(plotter,'tag','push_difference');
set(push_difference,'visible','off');

Xvector=Data(1).xmin:bin_size:Data(1).xmax; %Vector on which to project the histogram

Totalmol=[];
for i=1:size(Data,2)
    Totalmol=[Totalmol;Data(i).Mlocation2];
end

Totalmol=sort(Totalmol);
histdata=hist(Totalmol,Xvector);

haxes=findobj(plotter, 'tag', 'axes1');
axes(haxes);cla
bar(Xvector,histdata);
title(['Concensus map of ' num2str(size(Data,2)) ' molecules' ],'fontsize',12, 'Fontname', 'candara');
xlabel('Distance (nm)');
axis([Data.xmin Data.xmax 0 max(histdata)])
set(haxes, 'tag', 'axes1');

edit_threshold=findobj(plotter, 'tag', 'edit_threshold');
threshold=str2double(get(edit_threshold,'string'));
histdata_thr=histdata;
histdata_thr(histdata_thr<threshold)=0;
histdata_thr(histdata_thr>1)=1;

haxes=findobj(plotter, 'tag', 'axes3');
axes(haxes);cla
bar(Xvector,histdata_thr,'green');
axis off
set(haxes, 'tag', 'axes3');


function edit_bin_Callback(hObject, eventdata, handles)
plot_DNAM_2010
fitter=findobj('tag','fitter');
Data=get(fitter,'Userdata');
% if strcmp(Data(1).flt.relativeto,'sequence')
%     draw_histo_figure_reference
% else
%     draw_histo_figure_self
% end


% --- Executes on button press in push_save.
function push_save_Callback(hObject, eventdata, handles)
% hObject    handle to push_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fitter=findobj('tag','fitter');
if get(findobj(fitter,'tag','radio_rndm'),'value')==1
    rndmsites=findobj('Tag','randomsites');
    Data=get(rndmsites,'Userdata');
else
    Data=get(fitter,'Userdata');
end

[Filename, Pathname]=uiputfile('*.mat','Save the alligned molecules','HIST_');
cd(Pathname);
for x=1:size(Data,2)
    data(x).reference=Data(x).reference;
    data(x).Mlocation2=Data(x).Mlocation2;
    data(x).flt=Data(x).flt;
    data(x).Mlocation=Data(x).Mlocation;
end
eval(['save ' Filename ' data']);


% --- Executes on button press in push_difference.
function push_difference_Callback(hObject, eventdata, handles)
% hObject    handle to push_difference (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fitter=findobj('tag','fitter');
if get(findobj(fitter,'tag','radio_rndm'),'value')==1
    rndmsites=findobj('Tag','randomsites');
    Data=get(rndmsites,'Userdata');
else
    Data=get(fitter,'Userdata');
end

plotter=findobj('tag','plotter');
edit_bin=findobj(plotter,'tag','edit_bin');
gaussian_width=str2double(get(edit_bin,'string')); %get value for bin size (in nm)

M1=Data(1).reference;%*base_size; 

step=1;
X=create_Xaxis_DNAM(M1,[],gaussian_width,step);

referenceline=sum(exp(-(ones(size(M1,1),1)*X-M1*ones(1,size(X,2))).^2/(2*gaussian_width^2))); 

remain=zeros(size(Data,2),size(X,2));
for i=1:size(Data,2)
    M2=Data(i).Mlocation2;
    
    Newline=sum(exp(-(ones(size(M2,1),1)*X-M2*ones(1,size(X,2))).^2/(2*gaussian_width^2)));
    remain(i,:)=Newline-referenceline;
    remain(remain<0)=0;
    real(i,:)=Newline-remain(i,:);
end

%It should only be summed over multiple molecules, if only 1 molecule, sum is the same as the 1 molecule
if size(real,1)>1
    summed_remain=sum(remain);
    summed_real=sum(real);
else
    summed_remain=remain;
    summed_real=real;
end

referenceline=(referenceline./max(referenceline)).*max(summed_real);

SUM_remain=sum(summed_remain);
SUM_correct=sum(summed_real);
correct=SUM_correct/(SUM_remain+SUM_correct)*100
incorrect=SUM_remain/(SUM_remain+SUM_correct)*100
figure
Line=plot(X,summed_remain,'red',X,referenceline,'--blue',X,summed_real,'green');
legend('Remaining', 'Reference (norm.)','Correct');
set(Line,'LineSmoothing','on');
xlabel('Distance (bp)');
axis([min(X) max(X) 0 max([summed_remain, summed_real])])

if sum(strcmp(fieldnames(Data(1)),'Consensus'))
    Mlocations_list=Data(1).Consensus;
    Consensus=sum(exp(-(ones(size(Mlocations_list,1),1)*X-Mlocations_list*ones(1,size(X,2))).^2/(2*gaussian_width^2)));
    hold on
    Line2=plot(X,Consensus,'Magenta');
    set(Line2,'LineSmoothing','on');
end



% --- Executes on button press in push_export.
function push_export_Callback(hObject, eventdata, handles)
% hObject    handle to push_export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fitter=findobj('tag','fitter');
if get(findobj(fitter,'tag','radio_rndm'),'value')==1
    rndmsites=findobj('Tag','randomsites');
    Data=get(rndmsites,'Userdata');
else
    Data=get(fitter,'Userdata');
end

datum=datevec(now);
outfile=['DNAlocations_' num2str(size(Data,2)) '_mol_' num2str(datum(3)) '-'  num2str(datum(2)) '-' num2str(datum(1)) '.txt'];
[Filename, Pathname]=uiputfile([Data.fp '*.txt'],'Save the locations',outfile);
cd(Pathname);
txtfile=fopen(Filename,'w');

if (isfield(Data(1),'Consensus')) %
    add='';
    add2='';
    for i=1:size(Data,2)
        add=[add num2str(size(Data(i).Mlocation2,1)) ' ,']; %adds , times amount of DNA molecules such that consensus is above right column
        add2=[add2 ' , '];
    end
    add=[add num2str(size(Data(1).Consensus,1))];
    fprintf(txtfile, ['DNA Locations on ' num2str(size(Data,2)) ' molecules' add2 'Consensus']);
    fprintf(txtfile, '\n %s', add );
else
    add='';
    for i=1:size(Data,2)
        add=[add size(Data(i).Mlocation2,1) ', ']; %adds , times amount of DNA molecules such that consensus is above right column
    end
fprintf(txtfile, ['DNA Locations on ' num2str(size(Data,2)) ' molecules']);
fprintf(txtfile, '\n %s',add);
end

u=1;
stoppedcolumn=0;
fclose(txtfile);
txtfile=fopen(Filename,'a');
if (isfield(Data(1),'Consensus'));totalmols=size(Data,2)+1; else totalmols=size(Data,2);end
i=[];
while sum(stoppedcolumn)~=totalmols
    
    if u<=size(Data(1).Mlocation2,1)
        base=num2str(Data(1).Mlocation2(u));
    else
        stoppedcolumn(1)=1;
        base=' ';
    end
    stringDNA=base;
    
    if size(Data,2)>1
        for i=2:size(Data,2);
            if u<=size(Data(i).Mlocation2,1)
                add=[' , ' num2str(Data(i).Mlocation2(u))];
            else
                stoppedcolumn(i)=1;
                add=', ';
            end
            stringDNA=[stringDNA add];
        end
    end
    
    
    if (isfield(Data(1),'Consensus'))
        i=size(Data,2)+1;
        if u<=size(Data(1).Consensus,1)
            add=[' , ' num2str(Data(1).Consensus(u))];
        else
            stoppedcolumn(i)=1;
            add=',  ';
        end
        stringDNA=[stringDNA add];
    end
        
    fprintf(txtfile, '\n %s',stringDNA);
    u=u+1;    
end
fclose(txtfile);





% --- Executes on button press in push_image.
function push_image_Callback(hObject, eventdata, handles)
% hObject    handle to push_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Data=getplotDNAM_Data();
datum=datevec(now);
outfile=['DNAmap_' num2str(size(Data,2)) '_mol_' num2str(datum(3)) '-'  num2str(datum(2)) '-' num2str(datum(1)) '.PNG'];
% if myIsField(Data,'fp') && exist(Data(1).fp, 'dir')
%   cd(Data(1).fp)
% else
%     Data(1).fp='C:\';
%     cd('C:\');
% end

fp=pwd;
[Filename, Pathname]=uiputfile([fp '*.PNG'],'Save the figure',outfile);

plotter=findobj('tag','plotter');

if get(findobj(plotter,'tag','quickload'),'value')==1;
    step=10;
    ysize=4;
else
    step=1;
    ysize=8;
end

edit_bin=findobj(plotter,'tag','edit_bin');
gaussian_widthx=str2double(get(edit_bin,'string')); %get value for bin size (in nm)
gaussian_widthy=ysize*gaussian_widthx;%width in y 8 times larger then x
amplitude=0.5;

%X_size=round(((Data(1).xmax-Data(1).xmin)/4));

complete=0;
h = waitbar(0,'constructing map');
tic

%I think this should always run... remove if?
%if sum(strcmp(fieldnames(Data(1)),'SP')) || strcmp(Data(1).flt.relativeto,'sequence')  
xlocation=Data(1).reference; 
Totalmol=xlocation;

%how many molecules?
totalsize=size(Data,2)+1;
if sum(strcmp(fieldnames(Data(1)),'Consensus'))
    Totalmol=[Totalmol;Data(1).Consensus];
    totalsize=totalsize+1;
end

for i=1:size(Data,2)
     Totalmol=[Totalmol;Data(i).Mlocation2];
end


%create x-axis
X=create_Xaxis_DNAM(Totalmol,gaussian_widthx,step);
xmin=min(X);
xmax=max(X);

%initialize image
[x,y]=meshgrid(xmin-gaussian_widthx*2:step:xmax+gaussian_widthx*2,1:step:gaussian_widthy*5);
X_size=size(x,2);
Y_size=size(x,1);
ylocation=y(round(Y_size/2),1);
image=zeros(Y_size,X_size);
min_x=min(x(:));

%create full image
ylocation_vector=ones(size(xlocation,1),1)*ylocation/step;
xlocation_vector=(xlocation-min_x)./step+1;
gaussians=[xlocation_vector,ylocation_vector];
image=map_create_gaussian_image(gaussians,image,gaussian_widthx/step,gaussian_widthy/step);

%add waitbar
complete=1;
waitbar(complete/totalsize,h,'constructing map');

%     for i=1:size(xlocation,1)
%         image=image+amplitude*exp(-((0.5*(x-xlocation(i)).^2/gaussian_widthx^2)+(0.5*(y-ylocation).^2/gaussian_widthy^2)));
%         complete=complete+1;
%         waitbar(complete/X_waitbar,h,'constructing map');
%     end

%normalize image
image=image/max(image(:))*1.5;
%image=image/median(image(:));


%draw a line under the image
line=ones(round(Y_size/4),X_size).*0.8;
image=[image;line];
%end

% X=create_Xaxis_DNAM([0;5000],0,step);
% xmin=min(X);
% xmax=max(X);

%[x,y]=meshgrid(xmin:step:xmax,1:step:gaussian_widthy*5);

% [x,y]=meshgrid(xmin-gaussian_widthx*2:step:xmax+gaussian_widthx*2,1:step:gaussian_widthy*5);
% 
% X_size=size(x,2);
% Y_size=size(x,1);
% ylocation=y(round(Y_size/2),1);


%if there is a concencus image
if sum(strcmp(fieldnames(Data(1)),'Consensus'))
    xlocation=Data(1).Consensus;
    
    imagec=zeros(Y_size,X_size);
    
    ylocation_vector=ones(size(xlocation,1),1)*ylocation/step;
    xlocation_vector=(xlocation-min_x)./step+1;
    gaussians=[xlocation_vector,ylocation_vector];
    
    imagec=map_create_gaussian_image(gaussians,imagec,gaussian_widthx,gaussian_widthy);
%     for i=1:size(Data(1).Consensus,1)
%         imagec=imagec+amplitude*exp(-((0.5*(x-xlocation(i)).^2/gaussian_widthx^2)+(0.5*(y-ylocation).^2/gaussian_widthy^2)));
%         complete=complete+1;
%         waitbar(complete/X_waitbar,h,'constructing map');
%     end 

    %normalize image
    imagec=imagec/max(imagec(:))*1.5;
    %imagec=imagec/median(imagec(:));

    %add concencus image to full image
    image=[image;imagec];
    
    %add another line
    line=ones(round(Y_size/4),X_size).*0.8;
    image=[image;line];
    
    %add waitbar
    complete=complete+1;
    waitbar(complete/totalsize,h,'constructing map');
end
    


%  if complete==0
%      Totalmol=0;
%      for i=1:size(Data,2)
%           Totalmol=[Totalmol;Data(i).Mlocation2];
%      end
%      X_waitbar=size(Totalmol,1);
%  end

%image for the data
image_d=zeros(Y_size,X_size);
for i=1:size(Data,2)
    xlocation=Data(i).Mlocation2;
    
    %initialize data image
    image_data=zeros(Y_size,X_size);
    
    ylocation_vector=ones(size(xlocation,1),1)*ylocation/step;
    xlocation_vector=(xlocation-min_x)./step+1;
    gaussians=[xlocation_vector,ylocation_vector];
    
    image_data=map_create_gaussian_image(gaussians,image_data,gaussian_widthx/step,gaussian_widthy/step);

%     for u=1:size(xlocation,1)
%         image_data=image_data+amplitude*exp(-((0.5*(x-xlocation(u)).^2/gaussian_widthx^2)+(0.5*(y-ylocation).^2/gaussian_widthy^2)));
%         complete=complete+1;
%         waitbar(complete/X_waitbar,h,'constructing map');
%     end

    %normalize image
    image_data=image_data/max(image_data(:))*1.5;
%     image_data=image_data/median(image_data(:));
    
    %add image to full image
    image=[image;image_data];
    
    %add waitbar
    complete=complete+1;
    waitbar(complete/totalsize,h,'constructing map');
    
end


%close waitbar
close(h)

%how long did it take?
toc

%plot image
figure;imagesc(image,[0,1]);colormap(bone);
image=image*256;

%save image
if Pathname>0
    cd(Pathname);
    map = gray(256);
    imwrite(image,map,Filename,'PNG');
end


% --- Executes on button press in push_consensus.
function push_consensus_Callback(hObject, eventdata, handles)
% hObject    handle to push_consensus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fitter=findobj('tag','fitter');

Data=getplotDNAM_Data();

plotter=findobj('tag','plotter');
edit_bin=findobj(plotter,'tag','edit_bin');
gaussian_width=str2double(get(edit_bin,'string')); %get value for bin size (in nm)
edit_threshold=findobj(plotter, 'tag', 'edit_threshold');
threshold=str2double(get(edit_threshold,'string')); %Get threshold value

%M1=Data(1).reference*base_size;
step=0.1;
sites=[];
included=0;

for i=1:size(Data,2) %Summ ALL the gaussians
    molecule=Data(i).Mlocation2;
    num_sites=Data(i).flt.numbersites;
    if  min(molecule)>-gaussian_width && max(molecule)<5000+gaussian_width && max(molecule)-min(molecule)>3500
        if size(molecule,1)<12 && num_sites>3;%Data(i).flt.Stretch>1.65 && Data(i).flt.Stretch<1.76 %&& size(molecule,1)<10
            included=included+1;
             sites=[sites;Data(i).Mlocation2];
        end
    end
end

disp(['used: ' num2str(included)]);
X=create_Xaxis_DNAM([0,5000],gaussian_width,step);

Newline=zeros(size(Data,2),size(X,2));
for i=1:size(Data,2) %Summ ALL the gaussians
        M2=Data(i).Mlocation2;
    num_sites=Data(i).flt.numbersites;
    if  min(M2)>-gaussian_width && max(M2)<5000+gaussian_width 
        if size(M2,1)<15 && num_sites>4;%Data(i).flt.Stretch>1.65 && Data(i).flt.Stretch<1.76 %&& size(molecule,1)<10


            Newline(i,:)=sum(exp(-(ones(size(M2,1),1)*X-M2*ones(1,size(X,2))).^2/(2*gaussian_width^2)));
        end
    end
end


% ydata=[640,650,824,2220,2306,2879,3023,3036,4781]';
% amplitude=-45;
% original=sum(amplitude*exp(-(ones(size(ydata,1),1)*X-ydata*ones(1,size(X,2))).^2/(2*gaussian_width^2)));
% %original=original.*-1;
% 
% Newline=[Newline;original];

if size(Newline,1)>1 %If only 1 molecule then...) (though it would be useless to create a consensus of only 1 molecule...)
    SLine=sum(Newline);
else
    SLine=Newline;
end

SLined=diff(SLine); %(derivative)
Xd=X(1:end-1);

SLinedp=SLined;
SLinedp(SLinedp<0)=-1; %Create the jump of -2
SLinedp(SLinedp>0)=1; %Create the jump of -2

SLinedpd=diff(SLinedp); %2nd derivative

Mlist=find(SLinedpd==-2); %Find the jump of -2
figure(100);cla;plot(SLine);
Mlist2=Mlist(SLine(Mlist)>threshold); %The amplitude of the peak needs to be above the threshold
Mlocations_list=X(Mlist2)'; %Convert to an actual x-value
Data(1).Consensus=Mlocations_list;

saveData(Data);

Consensus=sum(exp(-(ones(size(Mlocations_list,1),1)*X-Mlocations_list*ones(1,size(X,2))).^2/(2*gaussian_width^2)));
figure(200);cla
Line=plot(X,Consensus,'green');
%legend('Remaining', 'Reference (norm.)','Correct');
set(Line,'LineSmoothing','on');
xlabel('Distance (nm)');
axis([min(X) max(X) 0 max(Consensus)])



% --- Executes on button press in quickload.
function quickload_Callback(hObject, eventdata, handles)


% --- Executes on button press in pushbutton_histogram.
function pushbutton_histogram_Callback(hObject, eventdata, handles)
%fragmentmatching_DNAM()
real_histogram();

function real_histogram()
fitter=findobj('tag','fitter');

Data=getplotDNAM_Data();

plotter=findobj('tag','plotter');
edit_bin=findobj(plotter,'tag','edit_bin');
bin=str2double(get(edit_bin,'string')); %get value for bin size (in nm)

Reference=Data(1).Reference;
minref=0;
maxref=max(Reference);

%M1=Data(1).reference*base_size;
sites=[];
nrlabels=[];
stretch=[];
total_num=[];
totalsites=[];
fullsites=[];
Qualityscore=[];

full_sites=[]; %contains all sites
full_included=[]; %contains all sites included in the alignment
full_residual=[]; %contains all sites not included in the alignment
full_included_residual=[]; %combination of two above

correct=0;incorrect=0;
Qualityscoregood=[];Qualityscorebad=[];

score=[];
inc=0;

numbersites=[];
efficiency=[];
falsepos=[];
all_sizes=[];

for i=1:size(Data,2) %Summ ALL the gaussians
    molecule=Data(i).Mlocation2;
    num_sites=Data(i).flt.numbersites;
    totalsites=[totalsites,num_sites];
    nrlabels=[nrlabels;size(molecule,1)];
    molecule_size=molecule(end)-molecule(1);

        
    if  min(molecule)>minref-bin && max(molecule)<maxref+bin %&& molecule_size>35000
        stretch=[stretch;Data(i).flt.Stretch];
        score=[score;Data(i).flt.Score];
        Quality=Data(i).flt.Qualityscore;
        Qualityscore=[Qualityscore;Quality];

        offset=Data(i).flt.Offset;
        if abs(offset-1000000)<10000
            correct=correct+1;
            Qualityscoregood=[Qualityscoregood;Quality];
                    

            endref=Data(i).flt.sites(2,end);
            beginref=Data(i).flt.sites(2,1);
            
            enddata=Data(i).flt.sites(1,end);
            begindata=Data(i).flt.sites(1,1);
            
            numbersites=[numbersites; Data(i).flt.numbersites];
            
            efficiency_single=numbersites/(beginref-endref)*100;
            efficiency=[efficiency; efficiency_single];
            
            falsepos_single=1000*((begindata-enddata)-numbersites)/molecule_size;
            falsepos=[falsepos;falsepos_single];
            
        else
               all_sizes=[all_sizes;molecule_size];

            incorrect=incorrect+1;
            Qualityscorebad=[Qualityscorebad;Quality];
        end
            
        if   Data(i).flt.Score>0; %size(molecule,1)>3 && size(molecule,1)<18 && num_sites>4 &&% && Data(i).flt.Stretch>1.59 && Data(i).flt.Stretch<1.72 %&& size(molecule,1)<10      size(molecule,1)>7 && size(molecule,1)<13 &&
            full_sites=[full_sites;molecule];
            inc=inc+1;
            
            all_sites=Data(i).flt.sites;
            
            if true %change for include if true SW analysis
                [included_molecule,residual_molecule]=createnewmolecule_DNAM(Reference,molecule,all_sites);


                full_included=[full_included;included_molecule];
                full_residual=[full_residual;residual_molecule];
                
                full_included_residual=[full_included_residual;sort([full_included;full_residual])];
            end
            
        end
    end
end

disp(mean(numbersites));
disp(mean(efficiency));
disp(mean(falsepos));
disp(mean(all_sizes));

%plot number of sites included in alignment
x_numberofsites=0:size(Reference,1);[totalsiteshist,totalsitescenter]= hist(totalsites,20);
figure(80); bar(totalsitescenter,totalsiteshist,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
text(min(totalsitescenter),0.8*max(totalsiteshist), ['Number of sites average.: ' num2str(mean(totalsites))]);
axis([min(totalsitescenter) max(totalsitescenter) 0 max(totalsiteshist)])

%number of molecules
disp(['mols: ' num2str(inc)])
disp(['included: ' num2str(size(total_num,1))]);

%plot number of labels
xlabels=20;
[histlabels,centerlabels]=hist(nrlabels,xlabels);
%histlabels=histlabels./sum(histlabels);
figure(20);bar(centerlabels,histlabels,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
text(1.1*min(centerlabels),0.8*max(histlabels), ['number of labels.: ' num2str(mean(nrlabels))]);
disp(['mean: ' num2str(mean(nrlabels))]);

%plot histogram of stretch
[histstretch,centersstretch]=hist(stretch,20);
figure(30);bar(centersstretch,histstretch,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
text(min(centersstretch),0.8*max(histstretch), ['stretching average: ' num2str(mean(stretch))]);


%plot histogram of score
[histscore,centersscore]=hist(score,40);
figure(40);bar(centersscore,histscore,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
text(1.1*min(centersscore),0.8*max(histscore), ['Score average: ' num2str(mean(score))]);


%plot histogram of Qualityscore
xcenters=0:4:52;
[histQualityscore]=hist(Qualityscore,xcenters);
figure(50);bar(xcenters,histQualityscore,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
text(1*min(xcenters),0.8*max(histQualityscore), ['QS av: ' num2str(mean(Qualityscore))]);
axis([min(xcenters) max(xcenters) 0 max(histQualityscore)])

%plot histogram of Qualityscoregood
[histQualityscoregood]=hist(Qualityscoregood,xcenters);
figure(60);bar(xcenters,histQualityscoregood,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
text(1*min(xcenters),0.8*max(histQualityscoregood), ['QS good av: ' num2str(mean(Qualityscoregood))]);
axis([min(xcenters) max(xcenters) 0 max(histQualityscoregood)])
disp(['good: ', num2str(correct)])

%plot histogram of Qualityscorebad
[histQualityscorebad]=hist(Qualityscorebad,xcenters);
figure(70);bar(xcenters,histQualityscorebad,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
text(1*min(xcenters),0.8*max(histQualityscorebad), ['QS bad av: ' num2str(mean(Qualityscorebad))]);
axis([min(xcenters) max(xcenters) 0 max(histQualityscorebad)])
disp(['bad: ', num2str(incorrect)])




%plot histogram of alignment by offset
Xmin=round(min(full_sites))-bin;
Xmax=round(max(full_sites))+bin;

X=Xmin:bin:Xmax;
hist_total=hist(full_sites,X);
figure10=figure(10);%'color','white');
bar(X,hist_total,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
mx=round(max(hist(full_sites,X))/10)*10;
axis([minref-bin maxref+bin 0 mx]);
sizeallsites=size(full_sites,1);

%plot histogram of alignment by sequence matching

Xmin=round(min(full_included_residual))-bin;
Xmax=round(max(full_included_residual))+bin;

X=Xmin:bin:Xmax;
new_hist_total=hist(full_included_residual,X);

figure11=figure(11);%'color','white');
bar(X,new_hist_total,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
mx=round(max(hist(full_included_residual,X))/10)*10;
axis([minref-bin maxref+bin 0 mx]);


%only aligned molecules
Xmin=round(min(full_included))-bin;
Xmax=round(max(full_included))+bin;

X=Xmin:bin:Xmax;
new_hist_total=hist(full_included,X);

figure12=figure(12);%'color','white');
bar(X,new_hist_total,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
mx=round(max(hist(full_included,X))/10)*10;
axis([minref-bin maxref+bin 0 mx]);


% 
% Xmin=round(min(newsites3))-bin;
% Xmax=round(max(newsites3))+bin;
% 
% X=Xmin:bin:Xmax;
% new_hist_total=hist(newsites3,X);
% 
% figure13=figure(13);%'color','white');
% bar(X,new_hist_total,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
% mx=round(max(hist(newsites3,X))/10)*10;
% axis([-bin 5000+bin 0 mx]);
% 
% sizenewsites3=size(newsites3,1)
% perc3=sizenewsites3/sizeallsites

%axis off

%print(figure10,'hist_HR_noaxis','-dpng','-r600');
%saveas(figure10,'histo.png');
