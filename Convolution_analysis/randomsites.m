function varargout = randomsites(varargin)
% RANDOMSITES M-file for randomsites.fig
%      RANDOMSITES, by itself, creates a new RANDOMSITES or raises the existing
%      singleton*.
%
%      H = RANDOMSITES returns the handle to a new RANDOMSITES or the handle to
%      the existing singleton*.
%
%      RANDOMSITES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RANDOMSITES.M with the given input arguments.
%
%      RANDOMSITES('Property','Value',...) creates a new RANDOMSITES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before randomsites_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to randomsites_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help randomsites

% Last Modified by GUIDE v2.5 20-Sep-2012 15:54:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @randomsites_OpeningFcn, ...
                   'gui_OutputFcn',  @randomsites_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before randomsites is made visible.
function randomsites_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to randomsites (see VARARGIN)

% Choose default command line output for randomsites
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes randomsites wait for user response (see UIRESUME)
% uiwait(handles.randomsites);


% --- Outputs from this function are returned to the command line.
function varargout = randomsites_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit_molecules_Callback(hObject, eventdata, handles)
% hObject    handle to edit_molecules (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_molecules as text
%        str2double(get(hObject,'String')) returns contents of edit_molecules as a double


% --- Executes during object creation, after setting all properties.
function edit_molecules_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_molecules (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_sites_Callback(hObject, eventdata, handles)
% hObject    handle to edit_sites (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_sites as text
%        str2double(get(hObject,'String')) returns contents of edit_sites as a double


% --- Executes during object creation, after setting all properties.
function edit_sites_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_sites (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_spreadsites_Callback(hObject, eventdata, handles)
% hObject    handle to edit_spreadsites (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_spreadsites as text
%        str2double(get(hObject,'String')) returns contents of edit_spreadsites as a double


% --- Executes during object creation, after setting all properties.
function edit_spreadsites_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_spreadsites (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_size_Callback(hObject, eventdata, handles)
% hObject    handle to edit_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_size as text
%        str2double(get(hObject,'String')) returns contents of edit_size as a double


% --- Executes during object creation, after setting all properties.
function edit_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_spreadsize_Callback(hObject, eventdata, handles)
% hObject    handle to edit_spreadsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_spreadsize as text
%        str2double(get(hObject,'String')) returns contents of edit_spreadsize as a double


% --- Executes during object creation, after setting all properties.
function edit_spreadsize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_spreadsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
