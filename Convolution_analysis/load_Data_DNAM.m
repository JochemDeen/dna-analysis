function Data=load_Data_DNAM()
fitter=findobj('tag','fitter');
pushbutton_select=findobj(fitter,'tag','pushbutton_select');
tempdata=get(pushbutton_select,'Userdata');

if strcmp(get(pushbutton_select,'enable'),'on') && ~isempty(tempdata)
    Data=tempdata;
elseif get(findobj(fitter,'tag','radio_rndm'),'value')==1
    rndmsites=findobj('Tag','randomsites');
    Data=get(rndmsites,'Userdata');
else
    Data=get(fitter,'Userdata');
end
