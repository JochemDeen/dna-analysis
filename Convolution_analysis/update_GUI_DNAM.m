function update_GUI_DNAM(cases)

switch cases
    case 'buttons'
        buttons()
    case 'plot_line'
        plot_line()
    case 'enable_plot_buttons'
        enable_plot_buttons();
    case 'draw_score'
        draw_score()
        
end


function buttons()
fitter=findobj('tag','fitter');
pushbutton_select=findobj(fitter,'tag','pushbutton_select');
set(pushbutton_select,'enable','off');

radio_map=findobj(fitter,'tag','radio_map');
radio_rndm=findobj(fitter,'tag','radio_rndm');

push_prev=findobj(fitter,'tag','push_prev');
push_next=findobj(fitter,'tag','push_next');
pushbutton_stop=findobj(fitter,'tag','pushbutton_stop');
Push_showplot=findobj(fitter,'tag','Push_showplot');
pushbutton_SW=findobj(fitter,'tag','pushbutton_SW');
pushbutton_score=findobj(fitter,'tag','pushbutton_score');

set(push_prev,'visible','off');
set(push_next,'visible','off');
set(pushbutton_stop,'visible','on');
set(Push_showplot,'enable','off');
set(pushbutton_SW,'enable','off');
set(pushbutton_score,'enable','off');



if get(radio_map,'value')==0 && get(radio_rndm,'value')==0 
        msgbox('Select a reference map', 'Select reference','error');
        set(pushbutton_stop,'visible','off');
        return
end


function enable_plot_buttons()
fitter=findobj('tag','fitter');
push_prev=findobj(fitter,'tag','push_prev');
push_next=findobj(fitter,'tag','push_next');
Push_showplot=findobj(fitter,'tag','Push_showplot');
pushbutton_SW=findobj(fitter,'tag','pushbutton_SW');
pushbutton_score=findobj(fitter,'tag','pushbutton_score');

Data=get(fitter,'Userdata');

mols=size(Data,2);

%enable buttons
set(push_prev,'visible','on');
if mols>1;set(push_prev,'enable','on');
else set(push_prev,'enable','off'); end
set(push_next,'visible','on');
set(push_next,'enable','off');
set(Push_showplot,'enable','on');
set(pushbutton_SW,'enable','on');
set(pushbutton_score,'enable','on');


function draw_score
fitter=findobj('tag','fitter');
axes1=findobj(fitter,'tag','axes1');

%Load data
Data=load_Data_DNAM();

%get current data number to be processed
i=get(axes1,'Userdata');
data=Data(i);

%Load reference
Reference=Data(1).reference;

%Draw line
[Moleculeline,Referenceline,~]=createlines(data,Reference); %data, ref , X

%method
popup_fit=findobj(fitter,'tag','popup_fit');
method_value=get(popup_fit,'value'); %1, Convolution; 2, MinSquare
method_comparison={'Convolution','MinSquare'};

score=compare(method_comparison{method_value},Moleculeline,Referenceline);

figure; plot(score);

function DNA_comp=compare(method,Molecule_line,Reference_line)
        switch method
            case 'Convolution'
                DNA_comp=conv(fliplr(Molecule_line),Reference_line);
            case 'MinSquare'
                DNA_comp=xcorr(Molecule_line,Reference_line);
        end


function plot_line()
fitter=findobj('tag','fitter');
axes1=findobj(fitter,'tag','axes1');


%Load data
Data=load_Data_DNAM();

%get current data number to be processed
i=get(axes1,'Userdata');
data=Data(i);

%Load reference
Reference=Data(1).reference;

%Draw line
[Moleculeline,Referenceline,X]=createlines(data,Reference);

%Plot 
haxes=findobj(fitter,'tag','axes1');
axes(haxes);cla;
Line=plot(X,Moleculeline,X,Referenceline);
legend(['Molecule ' num2str(i)], 'Reference');
set(Line,'LineSmoothing','on');
set(haxes,'tag','axes1');
set(axes1,'Userdata',i)

%set label
if true
    xlabel('Distance (bp)');
else
    xlabel('Distance (nm)');
end

%load stretching parameters
stretch=data.flt.Stretch;
offset=data.flt.Offset; %offset in nanometer
direction=data.flt.Direction; %+1 for normal direction, -1 for reverse
score=data.flt.score;
snr=data.flt.snr;

text_fitdata=findobj(fitter,'tag','text_fitdata');
set(text_fitdata,'string',['Molecule: ' num2str(i) char(10) 'Stretch: ' num2str(stretch) char(10) 'Direction: ' num2str(direction) char(10) 'Offset: ' num2str(offset) char(10) 'Score: ' num2str(score) char(10) 'SNR: ' num2str(snr)]);
axis([min(X) max(X) 0 max([Moleculeline,Referenceline])])



%if random sites
% if get(findobj(fitter,'tag','radio_rndm'),'value')==1
%     rndmsites=findobj('Tag','randomsites');
%     Data=get(rndmsites,'Userdata');
% else
%     Data=get(fitter,'Userdata');
% end



function [Moleculeline,Referenceline,X]=createlines(data,Reference)
fitter=findobj('tag','fitter');

%Gaussian width
edit_width=findobj(fitter,'tag','edit_width');
gaussian_width=str2double(get(edit_width,'string'));

%reference width
edit_ref=findobj(fitter,'tag','edit_ref');
reference_width=str2double(get(edit_ref,'string'));


%load stretching parameters
stretch=data.flt.Stretch;
offset=data.flt.Offset; %offset in nanometer
direction=data.flt.Direction; %+1 for normal direction, -1 for reverse

%load molecule
Molecule=data.Mlocation;
% Molecule=Reference(64:77)*0.34*1.45;
% Molecule=Molecule-min(Molecule);
% 
Molecule=Molecule*direction-min(direction*Molecule);
Molecule=Molecule+offset;

if true %Convert to  nm or basepair?
    %Convert to basepair
    base_size=0.34; %nm/bp
    convert_fromnm=1/(base_size*stretch);  %bp/nm
    convert_frombp=1; %1bp=1bp
else
    %convert to nanometer
    convert_frombp=0.34*stretch; %1bp=0.34 * stretch nm
    convert_fromnm=1 %1 nm = 1 nm
end

%Convert to nm or basepair
Molecule=Molecule.*(convert_fromnm);
Reference=Reference.*convert_frombp;
gaussian_width_corrected=gaussian_width.*convert_fromnm; %from nanometer to... 
reference_width_corrected=reference_width.*convert_fromnm; %from nanometer to ...

%Read stepsize
edit_stepsize=findobj(fitter,'tag','edit_stepsize');
step_nm=str2double(get(edit_stepsize,'string')); %in nanometer
step_plot=step_nm*convert_fromnm; %convert from nanometer to ...

%initialize cutsize
cutsize=100000/step_plot;

%Create X axis for alignment
X=create_Xaxis_DNAM([Reference;Molecule],gaussian_width,step_plot);
if size(X,2)>cutsize
    X=create_Xaxis_DNAM(Molecule,gaussian_width,step_plot);
end
    
%line function
method_line={'Gaussian'; 'Block'};
popup_data=findobj(fitter,'tag','popup_data');
popup_ref=findobj(fitter,'tag','popup_ref');
val_mol=get(popup_data,'value');
val_ref=get(popup_ref,'value');

%Create the lineplots
Moleculeline=createline_DNAM(method_line{val_mol},Molecule,X,1,gaussian_width_corrected);
Referenceline=createline_DNAM(method_line{val_ref},Reference,X,1,reference_width_corrected);