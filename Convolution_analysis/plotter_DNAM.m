function plotter_DNAM(cases)

switch cases
    case 'start_plotter'
        start_plotter()
        
        
end

function start_plotter()
plot_DNAM_2010

plotter=findobj('tag','plotter');

push_difference=findobj(plotter,'tag','push_difference');
set(push_difference,'visible','on');
make_plot();


function make_plot()
plotter=findobj('tag','plotter');

fitter=findobj('tag','fitter');

Data=load_Data_DNAM();

%Data=get(fitter,'Userdata');
% if get(findobj(fitter,'tag','radio_rndm'),'value')==1
%     rndmsites=findobj('Tag','randomsites');
%     Data=get(rndmsites,'Userdata');
% else
%     Data=get(fitter,'Userdata');
% end



Totalmol=[];
base_size=0.34; %nm

%Load all molecules
for i=1:size(Data,2)
%load stretching parameters
    stretch=Data(i).flt.Stretch;
    offset=Data(i).flt.Offset; %offset in nanometer
    direction=Data(i).flt.Direction; %+1 for normal direction, -1 for reverse
    
    %load molecule
    Molecule=Data(i).Mlocation;
    Molecule=Molecule*direction-min(direction*Molecule);
    Molecule=Molecule+offset;
    
    %convert to bp
    convert_stretched=1/(base_size*stretch);  %bp/nm
    Molecule=Molecule./(convert_stretched);

    
    Totalmol=[Totalmol;Molecule];
end

%load reference
Reference=Data(1).reference;

%Create X axis
temp_Totalmol=[Totalmol;Reference];
edit_bin=findobj(plotter,'tag','edit_bin');
bin_size=str2double(get(edit_bin,'string')); %get value for bin size (in nm)
Xvector=min(temp_Totalmol):bin_size:max(temp_Totalmol);

%Create histogram from reference
histReference=hist(Reference,Xvector);
histReference_norm=histReference;
histReference_norm(histReference_norm>1)=1; %Make all peakes size of 1

%Create histogram from data
histdata=hist(Totalmol,Xvector);

%Compare which histograms are in both Data and reference 
%real means those that are in the reference
%remain are the remaining molecules
histdata_real=histdata.*histReference_norm; %Only real peakes (i.e. peakes that are also in the reference) remain, the rest is set to 0
histremain=histdata-histdata_real;  %remaining peakes
histdata_realnorm=histdata_real./(size(Data,2)*histReference); %Normalize peakes with respect to the reference (HistM1?)
histdata_realnorm(histdata_realnorm==Inf)=0; %quietly ignore divide by 0
histdata_realnorm(isnan(histdata_realnorm))=0; %quietly ignore 0 divided by 0

%How many molecules are correct compared to the total
hismol=histdata_realnorm-histremain;
hismol2=histdata_realnorm*size(Data,2)-histremain;
toomuch=hismol(hismol>1);
error=sum(histremain)+sum(toomuch)-size(toomuch,2);
correct=sum(histdata_realnorm)-sum(toomuch)+size(toomuch,2);

%Display the percentage correct based on the histogram
perc_correct=100*(sum(histdata_real)-sum(toomuch)+size(toomuch,2))/sum(histdata);

text_data=findobj(plotter,'tag','text_data');
set(text_data,'string',['Covered (norm): ' num2str(correct,4) char(10) 'Incorrect: ' num2str(error,4) char(10) 'Correct: ' num2str(perc_correct,2) ' %' char(10) 'Locations:' num2str(sum(histdata))]); %#ok<NOPRT,NOPRT>
%Totalmol=sort(Totalmol);
