function line=createline_DNAM(method,sites,X,stretch,width)
switch method
    case 'Gaussian'
        line=Gaussian(sites,X,stretch,width);
    case 'Block'
        line=Block(sites,X,stretch,width);
        
end

function line=Gaussian(sites,X,stretch,width)
%determine the step size
step=X(2)-X(1);

%full bin is 10*width/steps
bin_size=10*width/step;

sites=sites.*stretch;
sites=selectsites(sites,X);
line=zeros(1,size(X,2));
if size(sites,1)>1
    for i=1:size(sites,1)
        %calculate minimal x value and maximal x value
        [~, minindex]=min((X-sites(i)).^2);
        minx=round(minindex-bin_size/2); %index -10 steps
        maxx=round(minindex+bin_size/2); %index +10 steps
        %ceil(sites(i)+5*width);
        if maxx>size(X,2)
            maxx=size(X,2);
        end
        
        if minx<1
            minx=1;
        end            
               
        %create x vector
        tempx=X(minx:maxx);
        
        %calculate line
        templine=exp(-(tempx-sites(i)).^2/(2*width^2)); 
        
        %create index for position of xvalues on X-line
        %minx_i=minx-X(1)+1;
        %maxx_i=maxx-X(1)+1;
        
        %line(minx_i:maxx_i)=line(minx_i:maxx_i)+templine;
        line(minx:maxx)=line(minx:maxx)+templine;
    end
    %line=sum(exp(-(ones(size(sites,1),1)*X-sites*ones(1,size(X,2))).^2/(2*width^2))); 
elseif size(sites,1)>0
    line=exp(-(ones(size(sites,1),1)*X-sites*ones(1,size(X,2))).^2/(2*width^2)); 
end

function sites=selectsites(sites,X)
minval=X(1);
maxval=X(end);

sites=sites(sites>=minval);
sites=sites(sites<=maxval);

function line=Block(sites,X,stretch,width)
sites=sites*stretch;
diff=abs(ones(size(sites,1),1)*X-sites*ones(1,size(X,2)));

line_temp=diff;
line_temp(diff>width)=0;
line_temp(diff<=width)=1;
line=sum(line_temp);
line(line>1)=1;