function varargout = fit_DNAM_2010(varargin)
% DNA least square fitter
%Mlocations is the structure Data(i).Mlocation containing
%size(Data,2) molecules.

% Last Modified by GUIDE v2.5 06-Apr-2016 13:23:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @fit_DNAM_2010_OpeningFcn, ...
                   'gui_OutputFcn',  @fit_DNAM_2010_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% Mlocation2 is a list of enzyme locations IN bp!!!!
% Mlocation is original list of enzyme IN nm!!!!!!!! Convert to bp by: 
%
% Mlocation=Mlocation*Direction-min(Direction*Mlocation);
% Mlocation=Mlocation+Offset;
% Mlocation=Mlocation*Stretch;

% --- Executes just before fit_DNAM_2010 is made visible.
function fit_DNAM_2010_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output=hObject;
guidata(hObject, handles);

function varargout = fit_DNAM_2010_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


function push_load_fit_Callback(hObject, eventdata, handles)
hSPI=findobj('Tag', 'main');
data=get(hSPI, 'UserData');
cfp=pwd;
cd(data.currentpath)
[fn, fp]=uigetfile('HIST_*.mat');
load([fp fn]);
fitter=findobj('tag','fitter');
set(fitter,'Userdata',Data);

push_prev=findobj(fitter,'tag','push_prev');
push_next=findobj(fitter,'tag','push_next');
Push_showplot=findobj(fitter,'tag','Push_showplot');

set(push_prev,'visible','on');
set(push_next,'visible','on');
set(push_prev,'enable','on');
set(Push_showplot,'enable','on');

axes1=findobj(fitter,'tag','axes1');
loaded=1;
set(axes1,'Userdata',loaded);

update_GUI_DNAM('plot_line')
function push_load_Callback(hObject, eventdata, handles)
[fn, fp]=uigetfile('*.txt','select data file');
if fp>0
Reference=load([fp fn]);
fitter=findobj('tag','fitter');
Data=get(fitter,'Userdata');
Data(1).reference=Reference;
set(fitter,'Userdata',Data);
set(handles.radio_map,'visible','on');
set(handles.radio_map,'value',1);
set(handles.radiobutton_adjust,'visible','off');
set(handles.radiobutton_adjust,'value',0);
end
radio_map_Callback(hObject, eventdata, handles)
function push_prev_Callback(hObject, eventdata, handles)
fitter=findobj('tag','fitter');
axes1=findobj(fitter,'tag','axes1');
loaded=get(axes1,'Userdata');
if loaded~=1
    loaded=loaded-1;
end
if loaded==1
    set(hObject,'Enable','off');
end
set(axes1,'Userdata',loaded);

push_next=findobj(fitter,'tag','push_next');
set(push_next,'Enable','on')

update_GUI_DNAM('plot_line')
function push_next_Callback(hObject, eventdata, handles)
fitter=findobj('tag','fitter');
Data=load_Data_DNAM();
axes1=findobj(fitter,'tag','axes1');
loaded=get(axes1,'Userdata');
if loaded~=size(Data,2)
    loaded=loaded+1;
    if loaded==size(Data,2)
        set(hObject,'Enable','off');
    end
end
set(axes1,'Userdata',loaded);

push_prev=findobj(fitter,'tag','push_prev');
set(push_prev,'Enable','on')

update_GUI_DNAM('plot_line')
function radio_map_Callback(hObject, eventdata, handles)
if get(handles.radio_map,'value')==1;
    set(handles.text_ref,'visible','on');
    set(handles.edit_ref,'visible','on');
    set(handles.popup_ref,'visible','on');
    set(handles.radiobutton_adjust,'visible','off');
    set(handles.radiobutton_adjust,'value',0);
else
    set(handles.text_ref,'visible','off');
    set(handles.edit_ref,'visible','off');
    set(handles.popup_ref,'visible','off');
    set(handles.radiobutton_adjust,'visible','on');
end
function radio_rndm_Callback(hObject, eventdata, handles)
fitter=findobj('tag','fitter');
if get(findobj(fitter,'tag','radio_rndm'),'value')==1
    randomsites
end


function Push_showplot_Callback(hObject, eventdata, handles)
plotter_DNAM('start_plotter')
  
function pushbutton_Go_Callback(hObject,evendata,handles)
fitter=findobj('tag','fitter');

update_GUI_DNAM('buttons')


[Sites,Reference]=createsites_DNAM();

popup_fit=findobj(fitter,'tag','popup_fit');
method_val=get(popup_fit,'value'); %1, Convolution; 2, MinSquare
axes1=findobj(fitter,'tag','axes1');

total_mols=size(Sites,2);
Data.total_mols=total_mols;

pushbutton_stop=findobj(fitter,'tag','pushbutton_stop');

for i=1:total_mols
    if strcmp(get(pushbutton_stop,'visible'),'off')
        return
    end
    
    %set variable for current plot
    loaded=i;
    set(axes1,'Userdata',loaded);
    
    Compare_DNAM(method_val,Reference,Sites(i).Molecule);
    update_GUI_DNAM('plot_line')
end


update_GUI_DNAM('enable_plot_buttons')
plotter_DNAM('start_plotter')
function pushbutton_stop_Callback(hObject, eventdata, handles)
fitter=findobj('tag','fitter');
pushbutton_stop=findobj(fitter,'tag','fitter');

set(pushbutton_stop,'visible','off');
function pushbutton_histogram_Callback(hObject, eventdata, handles)
Histogram_DNAM('make_histogram');

function pushbutton_select_Callback(hObject, eventdata, handles)
Histogram_DNAM('select_molecules');


function popup_fit_Callback(hObject, eventdata, handles)


function popup_data_Callback(hObject, eventdata, handles)
update_GUI_DNAM('plot_line')


function popup_ref_Callback(hObject, eventdata, handles)
update_GUI_DNAM('plot_line')


% --- Executes on button press in pushbutton_SW.
function pushbutton_SW_Callback(hObject, eventdata, handles)
fitter=findobj('tag','fitter');
axes1=findobj(fitter,'tag','axes1');


%Load data
Data=load_Data_DNAM();

%get current data number to be processed
i=get(axes1,'Userdata');
data=Data(i);

%collect stretch data and molecule data
stretch=data.flt.Stretch;
Molecule=data.Mlocation;

%Start SWfitter
DNA_SWfitter;

%Save new molecule in SWFitter
Data=[];
Data.Mlocation=Molecule;
SWfitter=findobj('Tag','SWfitter');
set(SWfitter,'Userdata',Data);

%set current stretch value
edit_stretch=findobj(SWfitter,'Tag','edit_stretch');
set(edit_stretch,'string',num2str(stretch));


% --- Executes on button press in pushbutton_score.
function pushbutton_score_Callback(hObject, eventdata, handles)
update_GUI_DNAM('draw_score')
