fitter=findobj('tag','fitter');
pushbutton_select=findobj(fitter,'tag','pushbutton_select');
set(pushbutton_select,'enable','off');
Data=get(fitter,'Userdata');
Mloc=Data;
error=0;

if size(Mloc,2)==1 && get(handles.radio_map,'value')==0
    msgbox('Select more molecules or load a reference map', 'Select reference','error');
elseif get(handles.radio_map,'value')==0 && get(findobj(fitter,'tag','radio_rndm'),'value')==1;
    msgbox('Select a reference to use the random function', 'Select reference','error');
else
set(handles.push_prev,'visible','off');
set(handles.push_next,'visible','off');
set(handles.pushbutton_stop,'visible','on');
set(handles.Push_showplot,'enable','off');



%Setting fitting data
edit_width=findobj(fitter,'tag','edit_width');
edit_stepsize=findobj(fitter,'tag','edit_stepsize');
gaussian_width=str2double(get(edit_width,'string'));
edit_stretchstp=findobj(fitter,'tag','edit_stretchstp');
stretchstep=str2double(get(edit_stretchstp,'string'));
step=str2double(get(edit_stepsize,'string'));
edit_offset_low=findobj(fitter,'tag','edit_offset_low');
edit_offset_up=findobj(fitter,'tag','edit_offset_up');
offset_low=str2double(get(edit_offset_low,'string'));
offset_up=str2double(get(edit_offset_up,'string'));


%If random, else normall...
if get(findobj(fitter,'tag','radio_rndm'),'value')==1
    Sites=Randomizer(hObject,handles);
end
    %Find the molecule with most labels (reference = off)
if get(handles.radio_map,'value')==0 && get(findobj(fitter,'tag','radio_rndm'),'value')==0;
    totallist=[];
    sizemolecule=zeros(1,size(Mloc,2));
    for i=1:size(Mloc,2)
        sizemolecule(i)=size(Mloc(i).Mlocation,1);
        Mloc(i).label=i;
        if myIsField (Mloc(i).flt, 'pixelsize')
            pixelsize=Mloc(i).flt.pixelsize;
        else  
            if strcmp(char(Mloc(i).flt.objective),'60x'); objective=60;
            elseif strcmp(char(Mloc(i).flt.objective),'100x'); objective=100;
            elseif strcmp(char(Mloc(i).flt.objective),'40x');objective=40;end
            pixelsize=48*100/objective;
        end
        totallist=[totallist;Mloc(i).Mlocation*pixelsize];
    end
    %Data.totalsize=sum(sizemolecule); %How much enzyme location, useful for preallocating matrices
    [a,fr]=max(sizemolecule);
    Data(fr).flt.Stretch=1;
    Data(fr).flt.Direction=1;
    Data(fr).flt.Offset=0;
    for i=1:size(Mloc,2)
        Data(i).flt.relativeto=fr;
    end

    %Create the reference molecule, which is (right now) the first molecule in Data.
    if myIsField (Mloc(fr).flt, 'pixelsize')
        pixelsize=Mloc(fr).flt.pixelsize;
    else  
        if strcmp(char(Mloc(fr).flt.objective),'60x'); objective=60;
        elseif strcmp(char(Mloc(fr).flt.objective),'100x'); objective=100;
        elseif strcmp(char(Mloc(fr).flt.objective),'40x');objective=40;end
        pixelsize=48*100/objective;
    end
    M1=Mloc(fr).Mlocation*pixelsize;
    Data(fr).Mlocation2=M1; %save reference data IN NM!!

    set(fitter,'Userdata',Data);
    XMIN=-40; XMAX=max(totallist)*str2double(get(handles.edit_max,'string'))+gaussian_width/2;
    X=XMIN:step:XMAX;
    referenceline=sum(exp(-(ones(size(M1,1),1)*X-M1*ones(1,size(X,2))).^2/(2*gaussian_width^2))); 
    Mloc(fr)=[];

    %With reference sequence data
elseif get(handles.radio_map,'value')==1;
   base_size=0.34; %nm
   M1=Data(1).reference*base_size; 
   fr='sequence';

   if get(findobj(fitter,'tag','radio_rndm'),'value')==1;
   totallist=M1;
   for i=1:size(Sites,2)
       totallist=[totallist;Sites(i).molecule/str2double(get(handles.edit_max,'string'))];
   end
   XMIN=-round(gaussian_width*3);XMAX=max(totallist)++round(gaussian_width*3);
   else

       
   totallist=M1;
   for i=1:size(Mloc,2)
       Mloc(i).label=i;
    if myIsField (Mloc(i).flt, 'pixelsize')
        pixelsize=Mloc(i).flt.pixelsize;
    else   
        if strcmp(char(Mloc(i).flt.objective),'60x'); objective=60;
        elseif strcmp(char(Mloc(i).flt.objective),'100x'); objective=100;
        elseif strcmp(char(Mloc(i).flt.objective),'40x');objective=40;end
        pixelsize=48*100/objective;
    end

    totallist=[totallist;Mloc(i).Mlocation*pixelsize/str2double(get(handles.edit_max,'string'))];
   end
   
   XMIN=-round(gaussian_width*3); XMAX=max(totallist)++round(gaussian_width*3);
   end
   
   X=XMIN:step:XMAX;
   edit_ref=findobj(fitter,'tag','edit_ref');
   widthref=str2double(get(edit_ref,'string'));
   if get(handles.popup_ref,'value')==1;
       referenceline=sum(exp(-(ones(size(M1,1),1)*X-M1*ones(1,size(X,2))).^2/(2*widthref^2))); 
   elseif get(handles.popup_ref,'value')==2;
       diff=abs(ones(size(M1,1),1)*X-M1*ones(1,size(X,2)));
       reference=diff;
       reference(diff>widthref)=0;
       reference(diff<=widthref)=1;
       referenceline=sum(reference);
       referenceline(referenceline>1)=1;
   end
end 



if get(handles.radiobutton_adjust,'value')==1
    adjustref=3;
else adjustref=1;
end

for Arun=1:adjustref
    
    if get(findobj(fitter,'tag','radio_rndm'),'value')==1
        run=size(Sites,2);
    else
        run=size(Mloc,2);
    end
    
for i=1:run
    if strcmp(get(handles.pushbutton_stop,'visible'),'off')
        break
    end
    fitter=findobj('tag','fitter');
        
    %Create second set of data
    if get(findobj(fitter,'tag','radio_rndm'),'value')==1
        M2=Sites(i).molecule;
        mols=size(Sites,2);
    else
        if myIsField (Mloc(i).flt, 'pixelsize')
            pixelsize=Mloc(i).flt.pixelsize;
        else  
            if strcmp(char(Mloc(i).flt.objective),'60x'); objective=60;
            elseif strcmp(char(Mloc(i).flt.objective),'100x'); objective=100;
            elseif strcmp(char(Mloc(i).flt.objective),'40x');objective=40;end
            pixelsize=48*100/objective;
        end
        M2=Mloc(i).Mlocation*pixelsize;
        mols=size(Mloc,2);
    end;
    
    popup_fit=findobj(fitter,'tag','popup_fit');
    val=get(popup_fit,'value');
    if val==1%convolution
        stretch=str2double(get(handles.edit_min,'string')):stretchstep:str2double(get(handles.edit_max,'string'));
        stretch_max=str2double(get(handles.edit_max,'string'));
        if ~isnan(offset_low);low=offset_low/step+size(referenceline,2);else low=1;end
        if ~isnan(offset_up);up=offset_up/step+size(referenceline,2);else up=2*size(X,2)-1; end

        for D=-1:2:1
        M2disp=D*M2-min(D*M2);
        h = waitbar(0,['RUN ' num2str(Arun) ' of ' num2str(adjustref) '. Fitting molecule ' num2str(i) ' of ' num2str(mols) ' ( 0 / ' num2str(size(stretch,2)) ' - direction ' num2str(D) ')']);
       
        for str=1:size(stretch,2)
            Line=sum(exp(-(ones(size(M2disp,1),1)*X-M2disp/stretch(str)*ones(1,size(X,2))).^2/(2*gaussian_width^2)));
            if get(handles.popup_data,'value')==2;
                Line(Line>0)=Line/Line;
            end
            DNA_conv(str,:)=conv(fliplr(Line),referenceline); %moves Line relative to referenceline, starting with only last of Line and first of referenceline overlapping
            waitbar(str/size(stretch,2),h,['RUN ' num2str(Arun) ' of ' num2str(adjustref) '. Fitting molecule ' num2str(i) ' of ' num2str(mols) ' ( ' num2str(str) ' / ' num2str(size(stretch,2)) ' - direction ' num2str(D) ')']);
            if strcmp(get(handles.pushbutton_stop,'visible'),'off')
               break
            end
        end
        close(h)
        %figure; imagesc(DNA_conv);colormap bone;
        
      
        [colmax,rowI]=max(DNA_conv(:,low:up));
        [rowmax(D+2),colI(D+2)]=max(colmax);%rowindex=stretch, columnindex=offset
        rowI2(D+2)=rowI(colI(D+2)); %rowindex=stretch, columnindex=offset
        end
        if rowmax(3)>rowmax(1); QWX=3; D=1; Mloc(i).flt.conv=rowmax(3); else QWX=1; D=-1; Mloc(i).flt.conv=rowmax(1); end
        S=stretch(rowI2(QWX)); 
        O=(colI(QWX)+low-1-size(referenceline,2))*step;       
    elseif val==2 %min square search
        stretch=str2double(get(handles.edit_min,'string')):stretchstep:str2double(get(handles.edit_max,'string'));
        stretch_max=str2double(get(handles.edit_max,'string'));
        m=size(X,2); %size of vector
        columns=3*m-2; %max=3*m-2
        startpoint=round((columns-m)/2);
        Lineref=zeros(1,columns);
        DNA_data=zeros(1,columns);
        
        Lineref(startpoint:startpoint+m-1)=referenceline;

        for D=-1:2:1
        M2=D*M2-min(D*M2);
        h = waitbar(0,['RUN ' num2str(Arun) ' of ' num2str(adjustref) '. Calculating fit 0 / ' num2str(size(stretch,2)) ' (with direction ' num2str(D) ')']);

                
        for str=1:size(stretch,2)
           Line=sum(exp(-(ones(size(M2,1),1)*X-M2/stretch(str)*ones(1,size(X,2))).^2/(2*gaussian_width^2))); 
           for offs=1:columns-m+1
               DNA_data=zeros(1,round(columns));
               DNA_data(offs:round(offs+m-1))=Line;
               error(str,offs)=sum((Lineref-DNA_data).^2);
           end
           if strcmp(get(handles.pushbutton_stop,'visible'),'off')
               break
           end
           waitbar(str/size(stretch,2),h,['RUN ' num2str(Arun) ' of ' num2str(adjustref) '. Calculating fit ' num2str(str) ' / ' num2str(size(stretch,2)) ' (with direction ' num2str(D) ')']);
        end
        close(h)
        
        [colmin,rowI2]=min(error);
        [rowmin(D+2),colI(D+2)]=min(colmin);
        rowI2(D+2)=rowI2(colI(D+2)); %rowindex=stretch, columnindex=offset
        end
        if rowmin(3)<rowmin(1); i=3; D=1; else i=1; D=-1;end
        S=stretch(rowI2(i)); 
        O=colI(i)-startpoint;
    end


    %save fitted data

    %if get(handles.radio_map,'value')==1 || i<fr; x=i;else x=i+1;end  %This is just because the reference molecule has been removed from the pool
    x=Mloc(i).label;
    Data(x).flt.Stretch=S;
    Data(x).flt.Direction=D;
    Data(x).flt.Offset=O;
    Data(x).flt.relativeto=fr;
    M2=(D*M2-min(D*M2))/S; %create a new location list relative to molecule fr (including direction)
    %M2=S*M2;
    Data(x).Mlocation2=(M2+O); %(Mlocation2 is a list of enzyme locations IN NM!!!!) 
    if get(findobj(fitter,'tag','radio_rndm'),'value')==1
        rndmsites=findobj('Tag','randomsites');
        set(rndmsites,'Userdata',Data);
    else
        set(fitter,'Userdata',Data);
    end
    M2=M2+O;

    X2=min(M2)-round(gaussian_width*3):step:max(M2)+round(gaussian_width*3);
    Newline=sum(exp(-(ones(size(M2,1),1)*X2-M2*ones(1,size(X2,2))).^2/(2*gaussian_width^2)));
    haxes=findobj(fitter,'tag','axes1');
    axes(haxes);cla;
    Line=plot(X2,Newline,X,referenceline);
    legend(['Molecule ' num2str(i)], 'Reference');
    set(Line,'LineSmoothing','on');
    set(haxes,'tag','axes1');
    xlabel('Distance (nm)');
    set(handles.text_fitdata,'string',['Molecule: ' num2str(i) char(10) 'Stretch: ' num2str(S) char(10) 'Direction: ' num2str(D) char(10) 'Offset: ' num2str(O)]); %#ok<NOPRT,NOPRT>
    axis([min([M1;M2])-round(gaussian_width*3) max([M1;M2]) 0 max([Newline,referenceline])])
end
if adjustref>1
maxconv=[];
    for o=1:size(Mloc,2)
        maxconv=[maxconv;Mloc(o).flt.conv];
    end
    ref_convself=conv(referenceline,referenceline);
    ref_convself=max(ref_convself);
    [~,mx]=max(maxconv);
    %figure(500+Arun);plot(maxconv/ref_convself,'rx');
    figure(500);hold on;plot(Arun,max(maxconv)/ref_convself,'rx');hold off
    [M1,referenceline,Mloc]=createref(M1,referenceline,Mloc,Data,mx,XMIN,XMAX);
    Mloc(mx)=[];
end
end

    sizemolecule=zeros(1,mols);
    totallist=[];
    for i=1:size(Data,2)
        totallist=[totallist;Data(i).Mlocation2];
    end
    if strcmp(Data(1).flt.relativeto,'sequence')
        base_size=0.34; %nm
        M1=Data(1).reference*base_size; 
        totallist=[totallist;M1];
    end
    Data(1).xmax=max(totallist);
    Data(1).xmin=min(totallist);

    if get(findobj(fitter,'tag','radio_rndm'),'value')==1
        rndmsites=findobj('Tag','randomsites');
        set(rndmsites,'Userdata',Data);
    else
        set(fitter,'Userdata',Data);
    end

    if mols>1
        set(handles.push_prev,'visible','on');
        set(handles.push_prev,'enable','on');
    else 
        set(handles.push_prev,'visible','on');
        set(handles.push_prev,'enable','off');
    end
    set(handles.push_next,'visible','on');
    set(handles.push_next,'enable','off');
    set(handles.Push_showplot,'enable','on');
    handles.loaded=size(Mloc,2);
    guidata(hObject, handles)
    if strcmp(Data(1).flt.relativeto,'sequence')
        draw_histo_figure_reference
    else
        draw_histo_figure_self
    end
end

function [M1,referenceline,Mloc]=createref(M1,referenceline,Mloc,Data,mx,XMIN,XMAX)
fitter=findobj('tag','fitter');
edit_width=findobj(fitter,'tag','edit_width');
edit_stepsize=findobj(fitter,'tag','edit_stepsize');
step=str2double(get(edit_stepsize,'string'));
gaussian_width=str2double(get(edit_width,'string'));  

%Data=get(fitter,'Userdata');
M2=Mloc(mx).Mlocation2;

X=XMIN:step:XMAX;
Newline(1,:)=referenceline;%sum(exp(-(ones(size(M1,1),1)*X-M1*ones(1,size(X,2))).^2/(2*gaussian_width^2)));
Newline(2,:)=sum(exp(-(ones(size(M2,1),1)*X-M2*ones(1,size(X,2))).^2/(2*gaussian_width^2)));
referenceline=sum(Newline);
%%%OLD METHOD
%fr=Data(1).flt.relativeto;
%XMIN=-40; %XMAX=max([M1;M2])*str2double(get(handles.edit_max,'string'))+gaussian_width/2;
%X=XMIN:0.1:XMAX;
%threshold=0.95; %peaks are above 1

%Newline(1,:)=sum(exp(-(ones(size(M1,1),1)*X-M1*ones(1,size(X,2))).^2/(2*gaussian_width^2)));
%Newline(2,:)=sum(exp(-(ones(size(M2,1),1)*X-M2*ones(1,size(X,2))).^2/(2*gaussian_width^2)));
%SLine=sum(Newline);
%SLined=diff(SLine); %(derivative)
%Xd=X(1:end-1);

%SLinedp=SLined;
%SLinedp(SLinedp<0)=-1; %Create the jump of -2
%SLinedp(SLinedp>0)=1; %Create the jump of -2

%SLinedpd=diff(SLinedp); %2nd derivative

%Mlist=find(SLinedpd==-2); %Find the jump of -2
%Mlist2=Mlist(SLine(Mlist)>threshold); %The amplitude of the peak needs to be above the threshold
%M1=X(Mlist2)'; %Convert to an actual x-value


%X=XMIN:step:XMAX;

%referenceline=sum(exp(-(ones(size(M1,1),1)*X-M1*ones(1,size(X,2))).^2/(2*gaussian_width^2))); 

function draw_histo_figure_reference %work in progress
plot_DNAM_2010
fitter=findobj('tag','fitter');

if get(findobj(fitter,'tag','radio_rndm'),'value')==1
    rndmsites=findobj('Tag','randomsites');
    Data=get(rndmsites,'Userdata');
else
    Data=get(fitter,'Userdata');
end

plotter=findobj('tag','plotter');
edit_bin=findobj(plotter,'tag','edit_bin');
bin_size=str2double(get(edit_bin,'string')); %get value for bin size (in nm)
push_difference=findobj(plotter,'tag','push_difference');
set(push_difference,'visible','on');

Xvector=Data(1).xmin:bin_size:Data(1).xmax;

Totalmol=[];
for i=1:size(Data,2)

    Totalmol=[Totalmol;Data(i).Mlocation2];
end
if strcmp(Data(1).flt.relativeto,'sequence')
    base_size=0.34; %nm
    M1=Data(1).reference*base_size; 
%    Totalmol=[Totalmol;M1];
end

histM1=hist(M1,Xvector);
histM1_norm=histM1;
histM1_norm(histM1_norm>1)=1; %Make all peakes size of 1

histdata=hist(Totalmol,Xvector);
histdata_real=histdata.*histM1_norm; %Only real peakes (i.e. peakes that are also in the reference) remain, the rest is set to 0
histremain=histdata-histdata_real;  %remaining peakes
histdata_realnorm=histdata_real./(size(Data,2)*histM1); %Normalize peakes with respect to the reference
histdata_realnorm(histdata_realnorm==Inf)=0; %quietly ignore divide by 0
histdata_realnorm(isnan(histdata_realnorm))=0; %quietly ignore 0 divided by 0

hismol=histdata_realnorm-histremain;
hismol2=histdata_realnorm*size(Data,2)-histremain;
toomuch=hismol(hismol>1);
error=sum(histremain)+sum(toomuch)-size(toomuch,2);
correct=sum(histdata_realnorm)-sum(toomuch)+size(toomuch,2);

perc_correct=100*(sum(histdata_real)-sum(toomuch)+size(toomuch,2))/sum(histdata);

text_data=findobj(plotter,'tag','text_data');
set(text_data,'string',['Covered (norm): ' num2str(correct,4) char(10) 'Incorrect: ' num2str(error,4) char(10) 'Correct: ' num2str(perc_correct,2) ' %' char(10) 'Locations:' num2str(sum(histdata))]); %#ok<NOPRT,NOPRT>
%Totalmol=sort(Totalmol);

haxes=findobj(plotter, 'tag', 'axes1');
axes(haxes);cla
bar(Xvector,hismol);
title(['Concensus map of ' num2str(size(Data,2)) ' molecules' ],'fontsize',12, 'Fontname', 'candara');
xlabel('Distance (nm)');
axis([Data.xmin Data.xmax min(hismol2) max(hismol2)])
set(haxes, 'tag', 'axes1');

haxes=findobj(plotter, 'tag', 'axes1');
axes(haxes);cla
%[hismol(:,2),hismol(:,1)]=hist(Totalmol,nrbins);
%bar(hismol(:,1),hismol(:,2));
bar(Xvector,hismol2);
title(['Concensus map of ' num2str(size(Data,2)) ' molecules' ],'fontsize',12, 'Fontname', 'candara');
xlabel('Distance (nm)');
axis([Data.xmin Data.xmax min(hismol2) max(hismol2)])
set(haxes, 'tag', 'axes1');

haxes=findobj(plotter, 'tag', 'axes2');
axes(haxes);cla
bar(Xvector,histM1_norm,'red');
axis off
set(haxes, 'tag', 'axes2');

edit_threshold=findobj(plotter, 'tag', 'edit_threshold');
threshold=str2double(get(edit_threshold,'string'));
histdata_thr=histdata;
histdata_thr(histdata_thr<threshold)=0;
histdata_thr(histdata_thr>1)=1;

haxes=findobj(plotter, 'tag', 'axes3');
axes(haxes);cla
bar(Xvector,histdata_thr,'green');
axis off
set(haxes, 'tag', 'axes3');

function draw_histo_figure_self
plot_DNAM_2010
fitter=findobj('tag','fitter');
Data=get(fitter,'Userdata');

plotter=findobj('tag','plotter');
edit_bin=findobj(plotter,'tag','edit_bin');
bin_size=str2double(get(edit_bin,'string')); %get value for bin size (in nm)
push_difference=findobj(plotter,'tag','push_difference');
set(push_difference,'visible','off');

Xvector=Data(1).xmin:bin_size:Data(1).xmax; %Vector on which to project the histogram

Totalmol=[];
for i=1:size(Data,2)
    Totalmol=[Totalmol;Data(i).Mlocation2];
end

Totalmol=sort(Totalmol);
histdata=hist(Totalmol,Xvector);

haxes=findobj(plotter, 'tag', 'axes1');
axes(haxes);cla
bar(Xvector,histdata);
title(['Concensus map of ' num2str(size(Data,2)) ' molecules' ],'fontsize',12, 'Fontname', 'candara');
xlabel('Distance (nm)');
axis([Data.xmin Data.xmax 0 max(histdata)])
set(haxes, 'tag', 'axes1');

edit_threshold=findobj(plotter, 'tag', 'edit_threshold');
threshold=str2double(get(edit_threshold,'string'));
histdata_thr=histdata;
histdata_thr(histdata_thr<threshold)=0;
histdata_thr(histdata_thr>1)=1;

haxes=findobj(plotter, 'tag', 'axes3');
axes(haxes);cla
bar(Xvector,histdata_thr,'green');
axis off
set(haxes, 'tag', 'axes3');



function error=H_line3(fit,M2,D,XMIN,XMAX,Linereffit,var) %Result is squared summed error function
%fit(1)=Stretch
%fit(2)=offset
%D=Direction
%XMIN=minimal X
%XMAX=maximal X
%M1=xdata
fitter=findobj('tag','fitter');
edit_width=findobj(fitter,'tag','edit_width');
edit_width2=findobj(fitter,'tag','edit_width2');
if var==1 %Low res
    gaussian_width=str2double(get(edit_width,'string'));
elseif var==2 %High res
    gaussian_width=str2double(get(edit_width2,'string'));
end
S=fit(1);O=fit(2);
M2=D*M2-min(D*M2);
X=XMIN:XMAX;
Line2=sum(exp(-(ones(size(M2,1),1)*X-S*M2*ones(1,size(X,2))+O).^2/(2*gaussian_width^2)))-0.5; 
error=sum((Line2-Linereffit).^2);



function draw_line(hObject,handles)
fitter=findobj('tag','fitter');
if get(findobj(fitter,'tag','radio_rndm'),'value')==1
    rndmsites=findobj('Tag','randomsites');
    Data=get(rndmsites,'Userdata');
else
    Data=get(fitter,'Userdata');
end

edit_width=findobj(fitter,'tag','edit_width');
gaussian_width=str2double(get(edit_width,'string'));
edit_ref=findobj(fitter,'tag','edit_ref');
widthref=str2double(get(edit_ref,'string'));

if strcmp(Data(1).flt.relativeto,'sequence')
    base_size=0.34; %nm
    M1=Data(1).reference*base_size; 
else
    fr=Data(1).flt.relativeto;
    M1=Data(fr).Mlocation2;
    Data(fr)=[];
end
X=Data(1).xmin-round(gaussian_width*3):1:Data(1).xmax+round(gaussian_width*3);

if ~exist('Data(1).xmin','var')
    Totalmol=[];
    for i=1:size(Data,2)
         Totalmol=[Totalmol;Data(i).Mlocation2];
    end
    xmin=min(Totalmol);
    xmax=max(Totalmol);
    X=xmin-round(gaussian_width*3):1:xmax+round(gaussian_width*3);
end


referenceline=sum(exp(-(ones(size(M1,1),1)*X-M1*ones(1,size(X,2))).^2/(2*widthref^2))); 

M2=Data(handles.loaded).Mlocation2;
Newline=sum(exp(-(ones(size(M2,1),1)*X-M2*ones(1,size(X,2))).^2/(2*gaussian_width^2)));

haxes=findobj(fitter,'tag','axes1');
axes(haxes);cla;
Line=plot(X,Newline,X,referenceline);
legend(['Molecule ' num2str(handles.loaded)], 'Reference');
set(Line,'LineSmoothing','on');
set(haxes,'tag','axes1');
xlabel('Distance (nm)');
set(handles.text_fitdata,'string',['Molecule: ' num2str(handles.loaded) ' of ' num2str(size(Data,2)) char(10) 'Stretch: ' num2str(Data(handles.loaded).flt.Stretch) char(10) 'Direction: ' num2str(Data(handles.loaded).flt.Direction) char(10) 'Offset: ' num2str(Data(handles.loaded).flt.Offset)]); %#ok<NOPRT,NOPRT>
axis([min([M1;M2])-round(gaussian_width*3) max([M1;M2]+round(gaussian_width*3)) 0 max([Newline,referenceline])])
set(haxes,'XTick',[round((min([M1;M2])-round(gaussian_width*3))/100)*100:50:round((max([M1;M2]+round(gaussian_width*3)))/100)*100])
guidata(hObject, handles)
