function varargout = DNA_SWfitter(varargin)
% DNA_SWFITTER MATLAB code for DNA_SWfitter.fig
%      DNA_SWFITTER, by itself, creates a new DNA_SWFITTER or raises the existing
%      singleton*.
%
%      H = DNA_SWFITTER returns the handle to a new DNA_SWFITTER or the handle to
%      the existing singleton*.
%
%      DNA_SWFITTER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DNA_SWFITTER.M with the given input arguments.
%
%      DNA_SWFITTER('Property','Value',...) creates a new DNA_SWFITTER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DNA_SWfitter_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DNA_SWfitter_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DNA_SWfitter

% Last Modified by GUIDE v2.5 10-Dec-2015 14:23:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DNA_SWfitter_OpeningFcn, ...
                   'gui_OutputFcn',  @DNA_SWfitter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DNA_SWfitter is made visible.
function DNA_SWfitter_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DNA_SWfitter (see VARARGIN)

% Choose default command line output for DNA_SWfitter
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DNA_SWfitter wait for user response (see UIRESUME)
% uiwait(handles.SWfitter);


% --- Outputs from this function are returned to the command line.
function varargout = DNA_SWfitter_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%Load reference map
function pushbutton1_Callback(hObject, eventdata, handles)
[fn, fp]=uigetfile('*.txt','select data file');
if fp>0
    Reference=load([fp fn]);
    SWfitter=findobj('Tag','SWfitter');
    text_refloaded=findobj(SWfitter,'Tag','text_refloaded');
    set(text_refloaded,'Userdata',Reference);
    set(text_refloaded,'visible','on');
end




% --- Executes on button press in pushbutton_Go.
function pushbutton_Go_Callback(hObject, eventdata, handles)
SWfitter=findobj('Tag','SWfitter');
%Check if reference map is loaded
text_refloaded=findobj(SWfitter,'Tag','text_refloaded');
text_refloaded_visible=get(text_refloaded,'visible');


if strcmp(text_refloaded_visible,'off')
    errordlg('load a reference map first');
else
    %load data
    Data=get(SWfitter,'Userdata');
    
    %iterate over all molecules
    h=waitbar(0,'alignment');
    for i=1:size(Data,2)
        waitbar(i/size(Data,2),h,'alignment');
        disp(['fitting: ' num2str(i)]);
        Molecule=Data(i).Mlocation;
        alignmaps(Molecule,i);
    end
    close(h);
    
    save_to_file();
    
    pushbutton_plot=findobj(SWfitter,'Tag','pushbutton_plot');
    set(pushbutton_plot,'enable','on');
    plot_DNAM_2010
end

function [SP,method]=getSP()
SWfitter=findobj('Tag','SWfitter');

%Load parameters

%localization inaccuracy
edit_sigma=findobj(SWfitter,'Tag','edit_sigma');
sigma=str2double(get(edit_sigma,'String'));

%fitting method
popup_method=findobj(SWfitter,'Tag','popup_method');
method_val=get(popup_method,'Value');
methodstring=get(popup_method,'String');
method=char(methodstring(method_val));

%consecutive false positives
edit_delta=findobj(SWfitter,'Tag','edit_delta');
delta=str2double(get(edit_delta,'String'));

%mean fragment length
edit_tau=findobj(SWfitter,'Tag','edit_tau');
tau=str2double(get(edit_tau,'String'));

%false positives per kbp
edit_zeta=findobj(SWfitter,'Tag','edit_zeta');
zeta=str2double(get(edit_zeta,'String'));

%labeling efficiency
edit_theta=findobj(SWfitter,'Tag','edit_theta');
theta=str2double(get(edit_theta,'String'));

%stretching variation
edit_sigmastretch=findobj(SWfitter,'Tag','edit_sigmastretch');
sigmastretch=str2double(get(edit_sigmastretch,'String'));

%set parameters depending on the method
if strcmp(method,'LR_Stretch')
    SP=[sigma;delta;tau;zeta;theta;sigmastretch];
elseif strcmp(method,'LR')
    SP=[sigma;delta;tau;zeta;theta];
elseif strcmp(method,'Simple')
	SP=[sigma;delta;zeta];
end


function alignmaps(Molecule,i)
[SP,method]=getSP();
SWfitter=findobj('Tag','SWfitter');
%get stretch value
edit_stretch=findobj(SWfitter,'Tag','edit_stretch');
stretch=str2double(get(edit_stretch,'String'));

%load reference
text_refloaded=findobj(SWfitter,'Tag','text_refloaded');
Reference=get(text_refloaded,'Userdata');

%get value for radiobutton stretch range
radiobutton_stretch=findobj(SWfitter,'Tag','radiobutton_stretch');
radiobutton_val=get(radiobutton_stretch,'value');

if ~radiobutton_val
    %stretch molecule
    
    Moleculestretched=Molecule/(0.34*stretch);

    [Parameters,Sites]=Fitting(Moleculestretched,Reference,method,SP);
    
    Parameters=[stretch;Parameters];
    savedata(Parameters,Molecule,Sites,i)
else
    %get 2nd stretch value
    edit_stretchrange=findobj(SWfitter,'Tag','edit_stretchrange');
    stretchrange=str2double(get(edit_stretchrange,'string'));
    
    if stretchrange<stretch
        errordlg('set 2nd stretch value higher than first value');
    end
    
    Stretch_range_list=stretch:0.001:stretchrange;
    
    [Parameters,Sites]=Fittingstretchrange(Molecule,Stretch_range_list,Reference,method,SP);
    
    savedata(Parameters,Molecule,Sites,i)
end

function save_to_file()
SWfitter=findobj('Tag','SWfitter');
Data=get(SWfitter,'Userdata');

save('Data.mat','Data');


function savedata(Parameters,Molecule,Sites,i)
SWfitter=findobj('Tag','SWfitter');
Data=get(SWfitter,'Userdata');

Data(i).flt.Stretch=Parameters(1);
Data(i).flt.Offset=Parameters(2);
Data(i).flt.Score=Parameters(3);
Data(i).flt.Direction=Parameters(4);
Data(i).flt.numbersites=Parameters(5);
%Data(i).flt.score=Parameters(6);
Data(i).flt.sites=Sites;
Data(i).flt.Qualityscore=Parameters(7);
Data(i).flt.Qualityscore2=Parameters(8);


if Parameters(4)~=1
    Molecule=flipmap(Molecule);
end

%load reference
text_refloaded=findobj(SWfitter,'Tag','text_refloaded');
Reference=get(text_refloaded,'Userdata');
Data(1).reference=Reference;

[SP,method]=getSP();
Data(1).SP=SP;
Data(1).method=method;

Mlocation2=Molecule/(0.34*Parameters(1));
Mlocation2=Mlocation2+Parameters(2); %offset is already in bp

Data(i).Mlocation2=Mlocation2;

set(SWfitter,'Userdata',Data);

%load map
%Data=get(SWfitter,'Userdata');
%Molecule=Data.Mlocation;

function newmap=flipmap(map)
newmap=flipud(abs(map-max(map)));

% --- Executes on button press in radiobutton_stretch.
function radiobutton_stretch_Callback(hObject, eventdata, handles)
SWfitter=findobj('Tag','SWfitter');
edit_stretchrange=findobj(SWfitter,'Tag','edit_stretchrange');
radiobutton_stretch=findobj(SWfitter,'Tag','radiobutton_stretch');

radiobutton_val=get(radiobutton_stretch,'value');

if radiobutton_val
    set(edit_stretchrange,'visible','on');
else
    set(edit_stretchrange,'string','');
    set(edit_stretchrange,'visible','off');
end



function edit_stretchrange_Callback(hObject, eventdata, handles)


function pushbutton_plot_Callback(hObject, eventdata, handles)
