function [Parameters,Sites]=Fittingstretchrange(Data,Stretch_range_list,Reference,method,SP)

%set max score;
maxscore=0;
maxsites=0;
max_stretch=Stretch_range_list(1);
% v=VideoWriter('newfile.avi');
% Delta=10;
% x=2000;
% y=160;


for i=1:size(Stretch_range_list,2)
    
    DataMolecule=Data/(0.34*Stretch_range_list(i)); %from nanometer to bp
    %Calling the fitting function
    [Fittingscores,All_sites,~,m]=SWFitter('fittoreference',DataMolecule,Reference,method,SP);
%     last_scores=max(m(:,end-Delta:end),[],2);
% 
    stretchscore(i)=Fittingscores(5);
    %if size(All_sites,2)>maxsites
    %    maxsites=size(All_sites,2);
        if Fittingscores(5)>maxscore;
            maxscore=Fittingscores(5);
            max_offset=Fittingscores(3);
            max_stretch=Stretch_range_list(i);
        end
   % end
%     
%     figure(9)
%     plot(Reference,last_scores)
%     axis([0 40000, 0 200]);
%     str= ['Stretch: ', num2str(Stretch_range_list(i))];
%     text(x,y,str);
%     
%     A(i)=getframe;

end
% 
% open(v);
% writeVideo(v,A);
% close(v)

DataMolecule=Data/(0.34*max_stretch);

[Fittingscores,All_sites,All_scores,Scoring_matrix]=SWFitter('fittoreference',DataMolecule,Reference,method,SP);

%which SWFitter
disp(['used: ', num2str(size(All_sites,2)), ' sites of the map, with score:' num2str(Fittingscores(5)) 'and offset: ' , num2str(Fittingscores(3)) 'and stretch: ' , num2str(max_stretch)])

sitesref=All_sites(2,:); %locations of the reference are the 2nd row
efficiency=size(sitesref,2)/(sitesref(1)-sitesref(end));
disp(['efficiency: ', num2str(round(1000*efficiency)/10), '%'])

labels=size(Data,1);
disp(['extra labels: ' , num2str(labels-size(sitesref,2)), ' or ' num2str(1000*(labels-size(sitesref,2))/(Data(end)-Data(1))) ' per kbp' ])

locations=fliplr(All_sites)+1; %flip locations vector (note c++ starts from 0, not 1)

%the first matched site on reference and data
data_match1=DataMolecule(locations(1,1));
ref_match1=Reference(locations(2,1));

%difference between first site of reference and first matched site
refdiff=ref_match1-Reference(1);

%difference between first site of data and first matched site
datadiff=data_match1-DataMolecule(1);

%calculate offset when the first site is located at 0
max_offset=refdiff-datadiff+Reference(1);

Direction=Fittingscores(4);
number_used=size(All_sites,2);
score=Fittingscores(5);

Sites=All_sites+1; %because matlab scoring is from 1


Delta=SP(2); %Delta is 2nd parameter
%Calculate SNR
%all possible scores for alignment on the entire molecule
if Delta>=size(Scoring_matrix,2)
    Delta=size(Scoring_matrix,2)-1;
end
last_scores=max(Scoring_matrix(:,end-Delta:end),[],2);
    
 figure(2);plot(Reference,last_scores,'k');
 
%  if size(last_scores,1)>1
%  axis([0 max(Reference) round(min(last_scores)/10)*10 round(max(last_scores)/10)*10])
%  end

%calculate background
background=median(last_scores);

%calculate signal=(score-back)^2
Signal=(Fittingscores(5)-background)^2;
%calculate noise=mean((Noise-back)^2)
Noise=mean((last_scores-background).^2);
    
%calculate SNR
SNR=Signal/Noise;

if size(last_scores,1)>200;
    last_scores2=sort(last_scores);

    Noise2=mean((last_scores2(end-100:end)-background).^2);

    SNR2=Signal/Noise2;
else
    SNR2=SNR;
end


disp(['Quality score: ', num2str(SNR)])

Parameters=[max_stretch;max_offset;maxscore;Direction;number_used;score;SNR;SNR2];


% plot score

%figure(4)
%plot(Stretch_range_list,stretchscore)
% 
plotscore_v2(DataMolecule,Reference,All_sites,All_scores,Fittingscores(4))