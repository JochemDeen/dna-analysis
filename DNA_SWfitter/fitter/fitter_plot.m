function fitter_plot(Data,Reference)
Distr_sigma=70; %localization inaccuracy (bp)
Delta=10; %consecutive false positives
Tau=360; %mean fragment length (bp)
Zeta=0.3; %false positives per 1kbp
Theta=0.7; %labeling accuracy (between 0 and 1)
Stretch=0.1; %stretching variation (more than 0)

method='LR_Stretch'; %method LR or LR_Stretch

if strcmp(method,'LR_Stretch')
    SP=[Distr_sigma;Delta;Tau;Zeta;Theta;Stretch];
elseif strcmp(method,'LR')
    SP=[Distr_sigma;Delta;Tau;Zeta;Theta];
end

[X,Y,s,m]=SWFitter('fittoreference',Data,Reference,method,SP);
disp(['used: ', num2str(size(Y,2)), ' sites of the map, with score:' num2str(X(5)) 'and offset: ' , num2str(X(3))])

sitesref=Y(2,:); %locations of the reference are the 2nd row
efficiency=size(sitesref,2)/(sitesref(1)-sitesref(end));
disp(['efficiency: ', num2str(round(1000*efficiency)/10), '%'])

labels=size(Data,1);
disp(['extra labels: ' , num2str(labels-size(sitesref,2)), ' or ' num2str(1000*(labels-size(sitesref,2))/(Data(end)-Data(1))) ' per kbp' ])



%Calculate SNR
%all possible scores for alignment on the entire molecule
last_scores=max(m(:,end-Delta:end),[],2);
figure(2);plot(Reference,last_scores);
%calculate background
background=median(last_scores);

%calculate signal=(score-back)^2
Signal=(X(5)-background)^2;
%calculate noise=mean((Noise-back)^2)
Noise=mean((last_scores-background).^2);
%calculate SNR
SNR=Signal/Noise;

disp(['SNR: ', num2str(SNR)])


plotscore_v2(Data,Reference,Y,s,X(4))