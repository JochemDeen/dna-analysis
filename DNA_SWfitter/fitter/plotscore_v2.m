function plotscore_v2(map,reference,locations,score, orientation)
%plotscore_v2(map,reference,locations,score, orientation)

%if orientations is reversed, flip the map
if orientation~=1
    map=flipmap(map);
end

%some initialization values
diff=4;%difference in height reference vs data map
fill_ref_sites=false;

locations=fliplr(locations)+1; %flip locations vector (note c++ starts from 0, not 1)
score=fliplr(score); %flip score vector
sitesmap=locations(1,:); %locations of the map are the first row
sitesref=locations(2,:); %locations of the reference are the 2nd row

%set initial values
plotlocation=0;
list_of_plotlocations=0;

width=0; %value used when drawing boxes to indicate fragment, can be set to 0 now

figure(1);
clf;

%Plot all correctly alligned sites
%cycle over all sites in the map
for i=1:size(sitesmap,2)-1
    
    %index of correctly alligned site
    mapindex=sitesmap(i);
    %index of next correctly alligned site
    next_mapindex=sitesmap(i+1);
    
    %index of reference site corresponding to correctly alligned site
    refindex=sitesref(i);
    %index of reference site corresponding to next correctly alligned site
    next_refindex=sitesref(i+1);
    
    %calculate map size and reference size
    mapsize=map(next_mapindex)-map(mapindex);
    refsize=reference(next_refindex)-reference(refindex);
    
    %score for current allignment
    current_score=score(i+1);
    
    %draw correctly alligned site on first and second row
    drawline(plotlocation,diff,mapsize,'black')
    drawline(plotlocation,diff+5,0,'black')

    %draw reference site corresponding to correctly alligned site on first and second row
    drawline(plotlocation,-diff,refsize,'black')
    drawline(plotlocation,-diff-5,0,'black')

    %calculate middle of crrectly alligned fragment size
    x=plotlocation+max(mapsize,refsize)/2-5;
    y=5-diff+(2*diff-5)/2;
    
    %print score in between both sites
    string=sprintf('s: %0.1f',current_score);
    %text(x,y,['s: ' num2str(current_score)]);
    text(x,y,string);
    
    %set new location
    plotlocation=plotlocation+max(mapsize,refsize)+width;
    list_of_plotlocations=[list_of_plotlocations plotlocation];
end


%fill unmatched before the start
xwidth=0; %no width between red squares

%fill sites for map
if sitesmap(1)>1
   for i=1:sitesmap(1)-1
       %calculate distance from first aligned map site to first aligned site in map, use this as starting distance
       tostart=map(sitesmap(1))-map(i);
       
       %calculate current location for site
       location=0-tostart-xwidth*sitesmap(1)-1;
       
       %calculate mapsize
       mapsize=map(i+1)-map(i);
       
       %draw line for current map
       drawline(location,diff+5,0,'red')
   end
end

%fill sites for reference
if fill_ref_sites
    if sitesref(1)>1
       for i=1:sitesref(1)-1
            %calculate distance from first reference site to first aligned site in map, use this as starting distance
           tostart=reference(sitesref(1))-reference(i);
                  
           %calculate current location for site
           location=0-tostart-xwidth*sitesref(1)-1;

           %calculate ref size
           refsize=reference(i+1)-reference(i);

           %draw line for current map
           drawline(location,-diff-5,refsize,'red')
       end
    end
end

%fill rest unmatched locations
%map
for i=sitesmap(1):size(map,1)
   if any(sitesmap==i) 
       %if the current index is alligned

       %what is the index of the aligned site, to find back the location where it was plotted
       [~,index]=max(sitesmap==i);
       
       %set new plotting distance (from above)
       currentlocation=list_of_plotlocations(index);
   else
       %current index not alligned
       
       %calculate mapsize
        mapsize=map(i)-map(i-1);

        %draw a line for this match
        drawline(currentlocation+mapsize,diff+5,mapsize,'red')

        %increase currentlocation
        currentlocation=currentlocation+mapsize;
      
%       if i<size(map,1)
%         mapsize=map(i+1)-map(i);
%         drawsquare(currentlocation,diff+5,mapsize,'red')
%       end
      
   end
end
 
%reference
for i=sitesref(1):size(reference,1)
   if any(sitesref==i)
      %if the current index is alligned

       %what is the index of the aligned site, to find back the location where it was plotted
       [~,index]=max(sitesref==i);

      %set new plotting distance (from above)
       currentlocation=list_of_plotlocations(index);
   elseif i<max(sitesref) || fill_ref_sites
      %current index not alligned

       %calculate refsize
        refsize=reference(i)-reference(i-1);

        %draw a line for this match
        drawline(currentlocation+refsize,-diff-5,0,'red');

        %increase currentlocation
        currentlocation=currentlocation+refsize;

%       if i<size(reference,1)
%         refsize=reference(i+1)-reference(i);
%         drawsquare(currentlocation,-diff-5,refsize,'red')
%       end
   end
end




function newmap=flipmap(map)
newmap=flipud(abs(map-max(map)));


function drawsquare(location,height, mapsize,color)
figure(1)
rectangle('Position',[location height mapsize 5],'Curvature',0.2,'EdgeColor',color)
x1=location+mapsize/2;
y1=height+3;
text(x1,y1,['l: ' num2str(mapsize)]);


function drawline(location,height,mapsize,color)
figure(1)
X=[location location];
Y=[height height+4];
line(X,Y,'Color', color);
if mapsize>0
    text(location+mapsize/2-5,height+2, ['l: ' num2str(round(mapsize))]);
end

