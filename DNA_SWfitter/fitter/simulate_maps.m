function maps=simulate_maps(Sequence_map)

%Parameters for map stimulations
average_fragment_size= 50;  %Average size of fragments (in kbp)
spread_in_fragments=0;      %Spread on the average size(in kbp)
Label_eff=0.8;              %Labeling efficiency (between 0 and 1)
False_pos=0.1;                %Average false_positive (per 1 kbp)
Number_fragments=2;         %Number of fragments
Localization=50;            %Localization inacuraccy (Normal distribution width in bp)

max_sites=max(Sequence_map);

%Cut 'Number_fragments' from 'Sequence' with size 'fragment_size' and
%spread  'spread_in_fragments'

for x=1:Number_fragments
   %Randomly select begin site
   %calculate maximal starting site for map creation (leave some space for minimal map size)
   max_start=max_sites-average_fragment_size*1000+spread_in_fragments*1000;
   
   %If however the map is too short, start at 0
   if max_start>0
        begin_site=randi(max_start);
   else
       begin_site=0;
   end
   
   %Randomly select fragment size (normal distribution)
   fragment_size=normrnd(average_fragment_size*1000,spread_in_fragments*1000);
   
   end_site=begin_site+fragment_size;
   
   %Cut out map sites contained within the fragment size
   fragment=Sequence_map(Sequence_map>begin_site&Sequence_map<end_site);
   
   maps(x).fragment=fragment-fragment(1);
   maps(x).first_site=fragment(1);
   maps(x).size=fragment_size;
   
end


%LABEL EFFICIENCY
%Remove 1-Label_eff sites randomly from maps
for x=1:Number_fragments
    
    fragment=maps(x).fragment;
    
    %make logical vector if included
    labeled=(rand(size(fragment))<Label_eff);
    
    %add new maps to structure
    maps(x).fragment=fragment(labeled);
end

%Localization inaccuracy
for x=1:Number_fragments
    
    fragment=maps(x).fragment;
    
    %create matrix with size of fragment of inaccuracy with stand deviation 'Localization'
    inaccuracy=Localization.*randn(size(fragment));
    
    fragment=fragment+inaccuracy;
    maps(x).fragment=fragment-min(fragment);
end

%Add possion noise
for x=1:Number_fragments
    
    fragment=maps(x).fragment;
    fragment_size=maps(x).size;
    
    %calculate average size between false positives
    avsize_fp=1000/False_pos;
    
    %generate poisson random number with lambda size relative to avsize_fp
    R = poissrnd(fragment_size/avsize_fp);
    
    newfragments=randi(max(round(fragment)),R,1);
    
    %add new fragments and sort the fragment
    maps(x).molecule=sort(unique([fragment;newfragments]));
end


