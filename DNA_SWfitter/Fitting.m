function [Parameters,Sites]= Fitting(Data,Reference,method,SP)
%Calling the fitting function
[Fittingscores,All_sites,All_scores,Scoring_matrix]=SWFitter('fittoreference',Data,Reference,method,SP);
which SWFitter
disp(['used: ', num2str(size(All_sites,2)), ' sites of the map, with score:' num2str(Fittingscores(5)) ' and offset: ' , num2str(Fittingscores(3)) ' and direction: ' num2str(Fittingscores(4))])

max_offset=Fittingscores(3); %not used anymore, cannot be used
score=Fittingscores(5);
Direction=Fittingscores(4);
number_used=size(All_sites,2);

%correct representations of all sites
locations=fliplr(All_sites)+1; %flip locations vector (note c++ starts from 0, not 1)

%the first matched site on reference and data
data_match1=Data(locations(1,1));
ref_match1=Reference(locations(2,1));

%difference between first site of reference and first matched site
refdiff=ref_match1-Reference(1);

%difference between first site of data and first matched site
datadiff=data_match1-Data(1);

%calculate offset when the first site is located at 0
max_offset=refdiff-datadiff+Reference(1);

%locations=fliplr(All_sites)+1; %flip locations vector (note c++ starts from 0, not 1)
sitesref=All_sites(2,:); %locations of the reference are the 2nd row
efficiency=size(sitesref,2)/size(Reference,1);
disp(['efficiency: ', num2str(round(1000*efficiency)/10), '%'])

labels=size(Data,1);
disp(['extra labels: ' , num2str(labels-size(sitesref,2)), ' or ' num2str(1000*(labels-size(sitesref,2))/(Data(end)-Data(1))) ' per kbp' ])


Delta=SP(2); %Delta is 2nd parameter
%Calculate SNR
%all possible scores for alignment on the entire molecule

if Delta>=size(Scoring_matrix,2)
    Delta=size(Scoring_matrix,2)-1;
end
last_scores=max(Scoring_matrix(:,end-Delta:end),[],2);
figure(2);plot(Reference,last_scores);

%calculate background
background=median(last_scores);

%calculate signal=(score-back)^2
Signal=(Fittingscores(5)-background)^2;
%calculate noise=mean((Noise-back)^2)
Noise=mean((last_scores-background).^2);
%calculate SNR
SNR=Signal/Noise;


if size(last_scores,1)>200;
    last_scores2=sort(last_scores);

    Noise2=mean((last_scores2(end-100:end)-background).^2);

    SNR2=Signal/Noise2;
else
    SNR2=SNR;
end

disp(['Quality score: ', num2str(SNR)])

%plot score
plotscore_v2(Data,Reference,All_sites,All_scores,Fittingscores(4))

Sites=All_sites+1; %because matlab scoring is from 1

Parameters=[max_offset;score;Direction;number_used;0;SNR;SNR2];
