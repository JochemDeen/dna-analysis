function Data=getplotDNAM_Data()

fitter=findobj('tag','fitter');
if get(findobj(fitter,'tag','radio_rndm'),'value')==1
    rndmsites=findobj('Tag','randomsites');
    Data=get(rndmsites,'Userdata');
elseif size(findobj('tag','SWfitter'),1)>0
    SWfitter=findobj('tag','SWfitter');
    Data=get(SWfitter,'Userdata');
else
    Data=get(fitter,'Userdata');
end