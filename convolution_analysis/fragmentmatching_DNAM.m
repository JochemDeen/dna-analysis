function fragmentmatching_DNAM()
fitter=findobj('tag','fitter');

Data=getplotDNAM_Data();

plotter=findobj('tag','plotter');
edit_bin=findobj(plotter,'tag','edit_bin');
bin=str2double(get(edit_bin,'string')); %get value for bin size (in nm)


%M1=Data(1).reference*base_size;
sites=[];
nrlabels=[];
stretch=[];
total_num=[];
totalsites=0;
fullsites=[];
newsites=[];
newsites1=[];
newsites2=[];
newsites3=[];
score=[];
inc=0;

for i=1:size(Data,2) %Summ ALL the gaussians
    molecule=Data(i).Mlocation2;
    num_sites=Data(i).flt.numbersites;
    totalsites=[totalsites,num_sites];
    nrlabels=[nrlabels;size(molecule,1)];
        
    if  min(molecule)>-bin && max(molecule)<5000+bin && max(molecule)-min(molecule)>3500
        stretch=[stretch;Data(i).flt.Stretch];
        score=[score;Data(i).flt.Score];
        if  size(molecule,1)>3 && size(molecule,1)<18 && num_sites>4 && Data(i).flt.Score>0;% && Data(i).flt.Stretch>1.59 && Data(i).flt.Stretch<1.72 %&& size(molecule,1)<10      size(molecule,1)>7 && size(molecule,1)<13 &&
            sites=[sites;molecule];
            inc=inc+1;
            
            all_sites=Data(i).flt.sites;
            
            histogramdata=checkdualsites(molecule,all_sites,60);
            
            [newmolecule1,newmolecule2,newmolecule3]=createnewmolecule(molecule,all_sites,100);
            
            
            newsites1=[newsites1;newmolecule1];
            newsites2=[newsites2;newmolecule2];
            newsites3=[newsites3;newmolecule3];
            newmolecule=sort([newmolecule1;newmolecule2]);
            newsites=[newsites;newmolecule];

            
            fullsites=[fullsites;histogramdata'];
%             num=zeros(1,size(list,2));
%             for l=1:size(list,2)
%                 mind=list(l)-bin;
%                 maxd=list(l)+bin;
%                 
%                 num(l)=sum(molecule>mind&molecule<maxd);
%             end
%             total_num=[total_num;num];
            
        end
    end
end

xasd=0:10;totalsiteshist= hist(totalsites,xasd);
figure(80); hist(totalsites,xasd);
text(0,0.8*max(totalsiteshist), ['gem.: ' num2str(mean(totalsites))]);
disp(['mols: ' num2str(inc)])
labels=1:6;
for i=1:size(fullsites,2)
    sites_loc=fullsites(:,i);
    sites_loc=sites_loc(sites_loc>0);
    
    if i~=1 && i~=2 && i~=7 && i~=8
        x(:,i)=hist(sites_loc,labels);
    else
        q(:,i)=hist(sites_loc,labels);
    end
end


averagelabels=sum(x,2);
averagelabels=averagelabels./sum(averagelabels);

averagelabels2=sum(q,2);
averagelabels2=averagelabels2./sum(averagelabels2);

figure(300);bar(labels,averagelabels);
figure(301);bar(labels,averagelabels2);


% disp(['included: ' num2str(size(total_num,1))]);
% xlabels=0:25;
% histlabels=hist(nrlabels,xlabels);
% histlabels=histlabels./sum(histlabels);
% A=figure('color','white');bar(xlabels,histlabels,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
% axis([0 25 0 0.25])
% disp(['mean: ' num2str(mean(nrlabels))]);
% 
% 
% newX = linspace(0, 25, 25 * 10);
% y=poisspdf(xlabels,mean(nrlabels));
% newy = spline(xlabels, y, newX);
% 
% 
% hold on;plot(newX,newy,'k','LineWidth',2)
% hold off
% axis off
% print(A,'numberlabels','-dpng','-r600');

figure(30)
hist(stretch,20)

figure(40)
hist(score,40)

Xmin=round(min(sites))-bin;
Xmax=round(max(sites))+bin;

X=Xmin:bin:Xmax;
hist_total=hist(sites,X);
figure10=figure(10);%'color','white');
bar(X,hist_total,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
mx=round(max(hist(sites,X))/10)*10;
axis([-bin 5000+bin 0 mx]);
sizeallsites=size(sites,1)

Xmin=round(min(newsites))-bin;
Xmax=round(max(newsites))+bin;

X=Xmin:bin:Xmax;
new_hist_total=hist(newsites,X);

figure11=figure(11);%'color','white');
bar(X,new_hist_total,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
mx=round(max(hist(newsites,X))/10)*10;
axis([-bin 5000+bin 0 mx]);

Xmin=round(min(newsites2))-bin;
Xmax=round(max(newsites2))+bin;

X=Xmin:bin:Xmax;
new_hist_total=hist(newsites2,X);

figure12=figure(12);%'color','white');
bar(X,new_hist_total,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
mx=round(max(hist(newsites2,X))/10)*10;
axis([-bin 5000+bin 0 mx]);
sizenewsites2=size(newsites2,1)
perc2=sizenewsites2/sizeallsites



Xmin=round(min(newsites3))-bin;
Xmax=round(max(newsites3))+bin;

X=Xmin:bin:Xmax;
new_hist_total=hist(newsites3,X);

figure13=figure(13);%'color','white');
bar(X,new_hist_total,'FaceColor',[0.5 0.5 0.5],'EdgeColor','black')
mx=round(max(hist(newsites3,X))/10)*10;
axis([-bin 5000+bin 0 mx]);

sizenewsites3=size(newsites3,1)
perc3=sizenewsites3/sizeallsites
