function saveData(Data)
    fitter=findobj('tag','fitter');

if get(findobj(fitter,'tag','radio_rndm'),'value')==1
    rndmsites=findobj('Tag','randomsites');
    set(rndmsites,'Userdata',Data);
elseif size(findobj('tag','SWfitter'),1)>0
    SWfitter=findobj('tag','SWfitter');
    set(SWfitter,'Userdata',Data);
else

    set(fitter,'Userdata',Data);
end
