function varargout = DNA_analysis(varargin)


% Last Modified by GUIDE v2.5 10-Dec-2015 13:42:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DNA_analysis_OpeningFcn, ...
                   'gui_OutputFcn',  @DNA_analysis_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%gaussian=[frame, x, y, width x, width y, amp, back, total intensity, max of residual, mean of residual, std of residual];
%          1      2  3     4       5        6   7              8           9                10             11
%1 Intensity
%2 mean gaussian residual
%3 max of residual
%4 Gaussian residual std
%5 total intensity
%6 back
%7 Width x+y
%8 WidthX/Width Y

% --- Executes just before DNA_analysis is made visible.
function DNA_analysis_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DNA_analysis (see VARARGIN)

% Choose default command line output for DNA_analysis
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

DNA_analysis=findobj('Tag','DNA_analysis');
pushbutton_autolocate=findobj(DNA_analysis,'Tag','pushbutton_autolocate');

%cluster detection parameters
data.stretch_xaxis=10; 
data.density=8; %k
data.distance=20; %Eps
data.iterations=10;
data.thDist=2;
data.thInlrRatio=0.1;
data.min_distance_fromline=1.5;
data.min_molecule_length=8;
data.angle_deviation=10;

set(pushbutton_autolocate,'Userdata',data);


%add all subfolders to the general path
scriptName = mfilename('fullpath');
[currentpath, filename, fileextension]= fileparts(scriptName);

addpath(genpath(currentpath));


% 
% SPT2008_v4_1=findobj('Name','SPT2008_v4_1');
% main(hObject, handles);
%     
% if SPT2008_v4_1>0
%     close(findobj('Name','main2'));
%     hSPI=findobj('Tag', 'main');
%     data=get(hSPI, 'UserData');
%     if size(data,1)>0;
%         Data2.currentpath=data.currentpath;
%     else
%         Data2.currentpath='C:\';
%     end
%     Data2.zoom=[];
%     close(hSPI);
%     main(hObject, handles);
%     hSPI=findobj('Tag', 'main');
%     set(hSPI,'Userdata',Data2);
% else
%     data=[];
%     data.currentpath='C:\';
%     data.zoom=[];
%     hSPI=findobj('Tag', 'main');
%     set(hSPI,'Userdata',data);
% end

function varargout = DNA_analysis_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


function update_

%Menu file
%------------------------------------------------------------------------------------------------------------------------------------------------------
function menu_file_Callback(hObject, eventdata, handles)
function Loadf_Callback(~, ~, ~)
DNA_analysis=findobj('Tag', 'DNA_analysis');
listbox2=findobj(DNA_analysis,'tag','listbox2');
data=get(listbox2, 'UserData');

%Load filenames
[filenamemult,fp] = uigetfile('*.mat','multiselect','on');
if fp>0
    cd(fp);
    %save new file path in listbox2 userdata
    data.newfilepath=fp;
    
    %if only 1 file
    if size(char(filenamemult),1)==1;
        %save the new file name in listbox2 userdata
        fn=filenamemult;
        data.newfilename=fn;
        set(listbox2, 'UserData',data);
        
        %update the side list
        updategui('updatelist')
    
    elseif size(filenamemult,2)>1
            for i=1:size(filenamemult,2)%for all files
                %save the new file name in listbox2 userdata
                data=get(listbox2, 'UserData');
                data.newfilepath=fp;
                fn=char(filenamemult(i));
                data.newfilename=fn;
                set(listbox2, 'UserData',data);
                               
                %update the gui
                updategui('updatelist')

            end
    end
end
function Loadigorf_Callback(~, ~, ~)
DNA_analysis=findobj('Tag', 'DNA_analysis');
listbox2=findobj(DNA_analysis,'tag','listbox2');
data=get(listbox2, 'UserData');

%store filepath
cfp=pwd;

[fn2, fp2]=uigetfile('*.txt');

%create new data files

if fp2>0
    cd(fp2);
    %import data
    igData=importdata([ fp2 fn2]);

    %create new filename
    fn=[fn2(1:end-4) '_Igorfit.mat'];

    %input pixelsize
    pixelsize = inputdlg('pixelsize');
    %store flt data
    flt.pixelsize=str2num(pixelsize{1});
    flt.enfr=1;
    flt.igor=1;
    
    %store gaussians
    gaussians(:,1)=igData.data(2:end,1); %frame
    gaussians(:,2)=igData.data(2:end,4)+1; %x-location
    gaussians(:,3)=igData.data(2:end,5)+1; %y-location
    gaussians(:,4)=igData.data(2:end,3); %xwidth gaussian
    gaussians(:,5)=igData.data(2:end,3); %ywidth gaussian
    %create backup
    gaussiansbackup=gaussians;

    %set other values
    selected=[];
    CCDpath=fp2;
    CCDname=[];
    
    %save values
    eval(['save ' fn '  CCDpath CCDname flt gaussians gaussiansbackup selected'])
    disp(['saved as ' [fp2 fn]])
    
    %save the new filename in listbox 2 and updategui
    data.newfilepath=fp2;
    data.newfilename=fn;
    set(listbox2, 'UserData',data);
    
    %update the side list
    updategui('updatelist')
end
function loadnewfile_Callback(hObject, eventdata, handles)
DNA_analysis=findobj('Tag', 'DNA_analysis');
listbox2=findobj(DNA_analysis,'tag','listbox2');
data=get(listbox2, 'UserData');


[filenamemult,fp2] = uigetfile('*.*','Load Image','multiselect','on');
if fp2>0
    cd(fp2)
    
    %input pixelsize
    pixelsize = inputdlg('pixelsize');
    %store flt data
    flt.pixelsize=STR2DOUBLE(pixelsize{1});
    flt.enfr=1;
    flt.igor=1;
    gaussians=[]; %[0,1,2,3,4,5,6,7,8,9,10,11]; %fake gaussian
    gaussiansbackup=gaussians;
    selected=[];

    CCDpath=fp2;
    data.newfilepath=fp2;
    
    if size(char(filenamemult),1)==1;
        outfile=createname(filenamemult);
        data.newfilename=outfile;
        set(listbox2, 'UserData',data);

        
        eval(['save ' outfile '  CCDpath CCDname flt gaussians gaussiansbackup selected'])
        
        updategui('updatelist')
    elseif size(filenamemult,2)>1
            for i=1:size(filenamemult,2)
                outfile=createname(char(filenamemult(i)));
                data.newfilename=outfile;
                set(listbox2, 'UserData',data);

                eval(['save ' outfile '  CCDpath CCDname flt gaussians gaussiansbackup selected'])
                updategui('updatelist')
            end
    end
end
function imgchange_Callback(~, ~, ~)
data=DNA_analysis_load_selected_data();
if exist(data.CCDpath,'dir'); cd(data.CCDpath);end
[fn2, fp2]=uigetfile({'*.HIS;*.tif', 'All images (*.HIS, *.tif)';  '*.*', 'All files (*.*)'},'Select the image file');
if fp2>0
    load([data.fp data.fn]);
    CCDpath=fp2;
    CCDname=fn2;
    cd(data.fp)
eval(['save ' data.fn '  CCDpath CCDname flt gaussians gaussiansbackup selected'])
disp(['saved as ' [data.fp data.fn]])

DNA_analysis_drawimage
end

%------------------------------------------------------------------------------------------------------------------------------------------------------


%Menu Convolution alignment
%------------------------------------------------------------------------------------------------------------------------------------------------------
function Allignment_Callback(~, ~, handles)%##########
DNA_analysis=findobj('Tag', 'DNA_analysis');
listboxMlocation=findobj(DNA_analysis,'tag','listboxMlocation');
Mlocation_data=get(listboxMlocation,'Userdata');

x=who;
if size(Mlocation_data,1)>0 %sum(strcmp('Mlocation_data',Mloc)) &&
    if sum(strcmp(fieldnames(Mlocation_data),'Mloc'))
        fit_DNAM_2010;
        fitter=findobj('tag','fitter');
        data=[];
        for i=1:size(Mlocation_data.Mloc,2)
            data(i).Mlocation=Mlocation_data.Mloc(i).Mlocations;
        end
        set(fitter,'Userdata',data);
        radio_map=findobj(fitter,'tag','radio_map');
        set(radio_map,'value',0);
        set(radio_map,'visible','off');
    else
        msgbox('Load some molecules first')        
    end
else
    msgbox('Note: no molecules loaded, no functions available')
    fit_DNAM_2010;
    fitter=findobj('tag','fitter');
    radio_map=findobj(fitter,'tag','radio_map');
    set(radio_map,'value',0);
    set(radio_map,'visible','off');
end
function Loadmol_Callback(~, ~, handles)%##########
DNA_analysis=findobj('Tag', 'DNA_analysis');
listboxMlocation=findobj(DNA_analysis,'Tag', 'listboxMlocation');
Mlocations_Userdata=get(listboxMlocation,'Userdata');
Mlocations_Userdata=[]

[fn, fp]=uigetfile('*.mat');
newdata=load([fp fn]);

Mlocations_Userdata.newMlocations=[];
Mlocations_Userdata.Mloc=newdata.Mlocations_Userdata;
set(listboxMlocation, 'Userdata',Mlocations_Userdata);

updategui('updatelistMlocation')

function Savemol_Callback(~, ~, handles)%##########
DNA_analysis=findobj('Tag', 'DNA_analysis');
listboxMlocation=findobj(DNA_analysis,'Tag', 'listboxMlocation');
Mlocations_Userdata=get(listboxMlocation,'Userdata');

[Filename, Pathname]=uiputfile('*.mat','Save all the fitted molecules');
if Pathname>0
    cd(Pathname);
    Mlocations_Userdata=Mlocations_Userdata.Mloc;
    eval(['save ' Filename ' Mlocations_Userdata']);
end
%------------------------------------------------------------------------------------------------------------------------------------------------------


%Menu about
%------------------------------------------------------------------------------------------------------------------------------------------------------
function menu_about_Callback(~, ~, ~)
scnsize = get(0,'ScreenSize');
pos1  = [scnsize(3)/3, scnsize(4)*2/10, 522,650];
    %...
	%scnsize(3)/3, scnsize(4)*2/4];
L=imread('front.bmp');
logoFig = figure('Color',[0 0 0],'menubar',...
    'none','position', pos1, 'name', 'logo');
imshow(L)
set(logoFig , 'position', pos1)
function menu_changelog_Callback(~, ~, ~)
filepath=which('DNA_analysis');
filepath=filepath(1:end-14);
cd(filepath);
dos('notepad changelog.txt');
%------------------------------------------------------------------------------------------------------------------------------------------------------

%listbox
%------------------------------------------------------------------------------------------------------------------------------------------------------
function listbox2_Callback(hObject, eventdata, handles)
if ~strcmp(get(hObject,'string'),'')
    DNA_analysis=findobj('Tag', 'DNA_analysis');
    listbox2=findobj(DNA_analysis,'tag','listbox2');
    data=get(listbox2, 'UserData');
    
    %set image data.zoom to 0
    axes8=findobj(DNA_analysis,'tag','axes8');
    imagedata=get(axes8, 'UserData');
    imagedata.zoom=[];
    set(axes8,'Userdata',imagedata);
    

    set(findobj(DNA_analysis,'tag','radio_scan'),'value',0)

    updategui('radio_scan');
    
    DNA_analysis_drawimage
end

function listboxMlocation_Callback(hObject, eventdata, handles) %Selection from listbox of a molecule ###################
DNA_analysis=findobj('Tag', 'DNA_analysis');
listboxMlocation=findobj(DNA_analysis,'tag','listboxMlocation');
axes9=findobj(DNA_analysis,'tag','axes9');
axes8=findobj(DNA_analysis,'tag','axes8');


Mlocation_data=get(listboxMlocation,'Userdata');
i=get(listboxMlocation,'value');

if i>0
    bottom('on');
    selected=Mlocation_data.Mloc(i).selected_locations;
    %line=Mlocation_data.Mloc(i).line; %##############

    axes(axes9);cla;
    plot(selected(:,3),selected(:,4),'rx');
    title([num2str(size(selected,1)) ' molecules'],'fontsize',12, 'Fontname', 'candara');
    set(gca,'YDir','reverse');
    set(axes9,'tag','axes9')
    
    axes(axes8);cla;
end
%------------------------------------------------------------------------------------------------------------------------------------------------------

%Image functions
%------------------------------------------------------------------------------------------------------------------------------------------------------
function uipanel3_SelectionChangeFcn(hObject, eventdata, handles) %black, white, first, 2nd image
DNA_analysis_drawimage
function radiokeepscale_Callback(~, ~, ~)
DNA_analysis_drawimage
function editscalemax_Callback(hObject, ~, ~)
DNA_analysis=findobj('tag', 'DNA_analysis');
hed=findobj(DNA_analysis, 'Style', 'edit');

hsl=findobj(DNA_analysis, 'Style', 'slider');
hslmx=findobj(hsl, 'Tag', 'sliderscalemax'); %slider bar for max(im)

mx=str2num(get(hObject, 'string'));
set(hslmx, 'Value', mx)
DNA_analysis_drawimage
function editscalemin_Callback(hObject, ~, ~)
DNA_analysis=findobj('tag', 'DNA_analysis');

hsl=findobj(DNA_analysis, 'Style', 'slider');
hslmn=findobj(hsl, 'Tag', 'sliderscalemin'); %slier bar for min(im)

mn=str2num(get(hObject, 'string'));
set(hslmn, 'Value', mn)
DNA_analysis_drawimage
function sliderscalemax_Callback(hObject, ~, ~)
DNA_analysis=findobj('tag', 'DNA_analysis');
hed=findobj(DNA_analysis, 'Style', 'edit');
hmx=findobj(hed, 'Tag', 'editscalemax'); %edit box for max(im)

mx=round(get(hObject, 'value'));
set(hmx, 'String', num2str(mx))
DNA_analysis_drawimage
function sliderscalemin_Callback(hObject, ~, ~)
DNA_analysis_drawimage=findobj('tag', 'DNA_analysis');
hed=findobj(DNA_analysis_drawimage, 'Style', 'edit');
hmn=findobj(hed, 'Tag', 'editscalemin'); %edit box for min(im)

mn=round(get(hObject, 'value'));
set(hmn, 'String', num2str(mn))
DNA_analysis_drawimage
function edit_markersize_Callback(~, ~, ~)
DNA_analysis_drawimage
function edit_frames_Callback(~, ~, ~)
DNA_analysis_drawimage
function edit_first_frame_Callback(hObject, eventdata, handles) %Start frame
DNA_analysis_drawimage 
function slider_scan_Callback(hObject, ~, ~)
DNA_analysis=findobj('Tag','DNA_analysis');

sliderstep=get(hObject,'sliderstep');
value=get(hObject,'value')/sliderstep(1);
if value==0
    set(findobj(DNA_analysis,'tag','slider_scan'),'value',sliderstep(1));
    value=1;
end

set(findobj(DNA_analysis,'tag','edit_scan'),'string',num2str(round(value)))
DNA_analysis_drawimage
function radio_scan_Callback(~, ~, ~)
updategui('radio_scan')
DNA_analysis_drawimage
function edit_scan_Callback(~, ~, ~)
DNA_analysis=findobj('Tag','DNA_analysis');
slider_scan=findobj(DNA_analysis,'tag','slider_scan');
sliderstep=get(slider_scan,'sliderstep');


value=str2double(get(findobj(DNA_analysis,'tag','edit_scan'),'string'));
if value>0
    set(findobj(DNA_analysis,'tag','slider_scan'),'value',value*sliderstep(1));
end

DNA_analysis_drawimage
function edit_scanframes_Callback(~, ~, ~)
DNA_analysis=findobj('Tag','DNA_analysis');    
scanframes=round(str2double(get(findobj(DNA_analysis,'tag','edit_scanframes'),'string')));

data=DNA_analysis_load_selected_data();

%load the total number of frames to update the slider
if isfield(data.flt,'frames')
    frames=data.flt.frames;
else
    [~,frames]=readCCDim([data.CCDpath data.CCDname], 1,1);
end

set(findobj(DNA_analysis,'tag','slider_scan'),'sliderstep',[scanframes/frames 10*scanframes/frames])
set(findobj(DNA_analysis,'tag','slider_scan'),'value',(scanframes/frames));
set(findobj(DNA_analysis,'tag','edit_scan'),'string','1');
DNA_analysis_drawimage;
function edit_3d_Callback(~, ~, ~)
DNA_analysis_drawimage
function radio_black_Callback(~, ~, ~)
DNA_analysis=findobj('Tag','DNA_analysis');
radio_white=findobj(DNA_analysis,'tag','radio_white');
radio_first=findobj(DNA_analysis,'tag','radio_first');
radiobutton_image2=findobj(DNA_analysis,'tag','radiobutton_image2');
set(radio_white,'value',0);
set(radio_first,'value',0);
set(radiobutton_image2,'value',0);
drawimage
function push_zoom_Callback(~, ~, ~)
updategui('zoomimage')
DNA_analysis_drawimage
function pushzoomreset_Callback(~, ~, ~)
DNA_analysis=findobj('Tag','DNA_analysis');
axes8=findobj(DNA_analysis,'tag','axes8');
imagedata=get(axes8, 'UserData');

imagedata.zoom=[];
set(axes8,'Userdata',imagedata);
DNA_analysis_drawimage

%are these 2 functions necessary?
function radio_white_Callback(~, ~, ~)
DNA_analysis=findobj('Tag','DNA_analysis');
radio_black=findobj(DNA_analysis,'tag','radio_black');
radio_first=findobj(DNA_analysis,'tag','radio_first');
radiobutton_image2=findobj(DNA_analysis,'tag','radiobutton_image2');
set(radio_black,'value',0);
set(radio_first,'value',0);
set(radiobutton_image2,'value',0);
DNA_analysis_drawimage
function radio_first_Callback(~, ~, ~)
DNA_analysis=findobj('Tag','DNA_analysis');
radio_black=findobj(DNA_analysis,'tag','radio_black');
radio_white=findobj(DNA_analysis,'tag','radio_white');
radiobutton_image2=findobj(DNA_analysis,'tag','radiobutton_image2');
set(radio_black,'value',0);
set(radio_white,'value',0);
set(radiobutton_image2,'value',0);
DNA_analysis_drawimage

function radiobutton_image2_Callback(~, ~, ~)
data=DNA_analysis_load_selected_data();

DNA_analysis=findobj('Tag','DNA_analysis');
radio_black=findobj(DNA_analysis,'tag','radio_black');
radio_white=findobj(DNA_analysis,'tag','radio_white');
radio_first=findobj(DNA_analysis,'tag','radio_first');
set(radio_black,'value',0);
set(radio_white,'value',0);
set(radio_first,'value',0);


if isfield(data.flt,'image2')
    DNA_analysis_drawimage
else
    if exist(data.CCDpath,'dir'); cd(data.CCDpath);end
    [fn2, fp2]=uigetfile({'*.HIS;*.tif', 'All images (*.HIS, *.tif)';  '*.*', 'All files (*.*)'},'Select the image file');
    if fp2>0
        load([data.fp data.fn]);
        data.flt.image2=[fp2 fn2];
    cd(data.fp)
    flt=data.flt;
    DNA_analysis_save_selected_data(data)
    DNA_analysis_drawimage
    end
end

%------------------------------------------------------------------------------------------------------------------------------------------------------


%Analysis functions
%------------------------------------------------------------------------------------------------------------------------------------------------------
function radiobutton3_Callback(hObject, eventdata, handles) %Show advanced controls
updategui('showcontrols');
function pushbutton_reset_Callback(hObject, eventdata, handles) %Sets the gaussians back to backup (undo deleted molecules)
data=DNA_analysis_load_selected_data();
data.gaussians=data.gaussiansbackup;
DNA_analysis_save_selected_data(data)

DNA_analysis_drawimage
function push_consolidate_Callback(~, ~, ~)
DNA_analysis_molecule_selection('consolidate');
DNA_analysis_drawimage
%------------------------------------------------------------------------------------------------------------------------------------------------------


%DNA selection functions
%------------------------------------------------------------------------------------------------------------------------------------------------------
function pushbutton_locate_Callback(hObject, eventdata, handles) %Select a molecule
DNA_analysis_drawimage
DNA_analysis_molecule_selection('findmolecules')
bottom('on')
function pushbutton_delete_Callback(hObject, eventdata, handles) %Delete a location in axes8
DNA_analysis_molecule_selection('deletemolecules')
function pushbutton_fit_line_Callback(hObject, eventdata, handles) %Find the location of molecule and draw a straight line through it
%get current data
data=DNA_analysis_load_selected_data();
molecule=[data.selected(:,3), data.selected(:,4)];
DNA_analysis_add_fit_line(molecule)
DNA_analysis_drawimage

function push_delete_2_Callback(hObject, eventdata, handles) %Delete a molecule from selected in axes9
DNA_analysis_molecule_selection('deletemolecules_axes9');
DNA_analysis_drawimage
function pushbutton_delfit_Callback(hObject, eventdata, handles) %Delete a fit from Mlocation listbox
DNA_analysis=findobj('Tag', 'DNA_analysis');
listboxMlocation=findobj(DNA_analysis,'Tag', 'listboxMlocation');
Mlocations_Userdata=get(listboxMlocation,'Userdata');

del_i=get(listboxMlocation,'value');

Mlocations_Userdata.Mloc(del_i)=[];
set(listboxMlocation,'Userdata',Mlocations_Userdata);

updategui('updatelistMlocation')

function pushbutton_autolocate_Callback(hObject, eventdata, handles)
DNA_analysis_autodetect_molecules();
%------------------------------------------------------------------------------------------------------------------------------------------------------



%Gaussian fitting ############
%------------------------------------------------------------------------------------------------------------------------------------------------------
function radiobutton_analyse_Callback(hObject, eventdata, handles)
DNA_analysis=findobj('Tag','DNA_analysis');
radiobutton3=findobj(DNA_analysis,'tag','radiobutton3');
radiobutton_analyse=findobj(DNA_analysis,'tag','radiobutton_analyse');

if get(radiobutton3,'value')==1 && get(radiobutton_analyse,'value')
    set(findobj(DNA_analysis,'tag','uipanel_fitting'),'visible','on')
else
    set(findobj(DNA_analysis,'tag','uipanel_fitting'),'visible','off')
end
function edit_consolidate_Callback(~, ~, ~)
function fitting(~, ~, ~)
DNA_analysis=findobj('Tag','DNA_analysis');
image2=get(radiobutton_image2,'value');

begin=str2double(get(findobj(DNA_analysis,'tag','edit9'),'string'));
if get(findobj(DNA_analysis,'tag','radio_scan'),'value')==1
    scanframes=round(str2double(get(findobj(DNA_analysis,'tag','edit_scanframes'),'string')));
    start=round(str2double(get(findobj(DNA_analysis,'tag','edit_scan'),'string'))); 
    begin=begin+(start-1)*scanframes;
end

data=DNA_analysis_load_selected_data();

if (image2==1 && isfield(data.flt,'image2'))
    CCDfile=data.flt.image2;
elseif (radio_first==1 && exist(data.CCDpath,'file'))
    CCDfile=[data.CCDpath data.CCDname];
else
    return
end

if size(imread(CCDfile,1),3)>1
    Threedfr=round(str2double(get(findobj(DNA_analysis,'tag','edit_3d'),'string')));
else
    Threedfr=0;
end

DNA_analysis_frame_fitting(CCDfile,frame,Threedfr)
DNA_analysis_drawimage
function popupmenu_filter_Callback(hObject, eventdata, handles)
DNA_analysis=findobj('Tag','DNA_analysis');
matlabfitting=findobj(DNA_analysis,'Tag','matlabfitting');
cfitting=findobj(DNA_analysis,'Tag','cfitting');
if get(hObject,'value')==5
    set(matlabfitting,'visible','off')
    set(cfitting,'visible','on')
else
    set(matlabfitting,'visible','on')
    set(cfitting,'visible','off')
end
function radiobutton_onthefly_Callback(hObject, eventdata, handles)
function pushbutton_fitnow_Callback(hObject, eventdata, handles)
set(hObject,'enable','off');
fitting(hObject, eventdata, handles)
set(hObject,'enable','on');

%------------------------------------------------------------------------------------------------------------------------------------------------------


%Analyse gaussian fits######
%------------------------------------------------------------------------------------------------------------------------------------------------------

function pushbutton_fitdata_Callback(~, ~, handles) %gaussian fitting analysis program (amplitude etc...)
analyse_fit
anfit=findobj('tag', 'analyse_fit');
if ~isempty(anfit)
    DNA_analysis=findobj('Tag','DNA_analysis');
    pos=get(DNA_analysis, 'position');possub=get(anfit, 'position');
    possub(1)=pos(1)-20; %X position gaus - X width anfit + 17
    %set(anfit, 'position', possub)
    analysefit;
end
function push_remain_Callback(~, ~, ~)
% hObject    handle to push_remain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=DNA_analysis_load_selected_data();

image=readCCDim([data.CCDpath data.CCDname], 2,data.flt.enfr);
imsize=[size(image,1),size(image,2)];

substract_im=zeros(imsize(1),imsize(2)); %y and x
for G=1:1:size(data.gaussians,1) %Add gaussians found in 2 consecutive frames (within xx distance, see down).
                    amplitude=data.gaussians(G,6);
                    widthx=data.gaussians(G,4);
                    widthy=data.gaussians(G,5);
                    xlocation=data.gaussians(G,2);
                    ylocation=data.gaussians(G,3);
                    [x,y]=meshgrid(1:data.imsize(2),1:data.imsize(1)); %switch 2 and 4??
                    substract_im=substract_im+amplitude*exp(-((0.5*(x-xlocation).^2/widthx^2)+(0.5*(y-ylocation).^2/widthy^2)));
end
newimage=image-substract_im;
figure
imagesc(newimage);colormap bone;
%------------------------------------------------------------------------------------------------------------------------------------------------------

%Helper functions
%------------------------------------------------------------------------------------------------------------------------------------------------------
function outfile=createname(CCDname)
temp=CCDname;
temp(temp==','|temp==' ') = '';
outfile=[temp(1:end-4)  '_fit.mat'];
function bottom(value)
DNA_analysis=findobj('Tag','DNA_analysis');
set(findobj(DNA_analysis,'tag','push_delete_2'),'visible',value);
set(findobj(DNA_analysis,'tag','axes9'),'visible',value);
set(findobj(DNA_analysis,'tag','pushbutton_analyse'),'visible',value);
set(findobj(DNA_analysis,'tag','push_delete_2'),'visible',value);
%------------------------------------------------------------------------------------------------------------------------------------------------------

function toggle_fitting2_Callback(hObject, eventdata, handles)
%Test function, remove later or fix...
%Test function, remove later or fix...
function pushbutton_loadold_Callback(hObject, eventdata, handles)
hSPI=findobj('Tag', 'main');
data=get(hSPI, 'UserData');
cfp=pwd;
cd(data.currentpath)
[fn, fp]=uigetfile('gft_*.mat');
load([fp fn]);
subdata=get(handles.DNA_analysis, 'UserData');
subdata.gaussiansbackup=gaussiansbackup;
subdata.gaussians=gaussians;
subdata.CCDname=CCDname;
subdata.CCDpath=CCDpath;
subdata.fn=fn;
subdata.fp=fp;
subdata.flt=flt;
subdata.selected=selected;
set(handles.DNA_analysis, 'UserData', subdata);
double=findobj('tag','pushbutton_Double');
set(double,'visible','on');
set(double,'tag','pushbutton_Double');
text19=findobj('tag','text19');
set(text19,'String','File loaded');
Doublefitting;





subdata=get(handles.DNA_analysis, 'UserData');
x=who;
if sum(strcmp('subdata',x)) && size(subdata,1)>0
    if sum(strcmp(fieldnames(subdata),'Mloc'))
        data=subdata.Mloc;
        fit_DNAM_2010;
        fitter=findobj('tag','fitter');
        set(fitter,'Userdata',data);
        radio_map=findobj(fitter,'tag','radio_map');
        set(radio_map,'value',0);
        set(radio_map,'visible','off');
    else
        msgbox('Load some molecules first')        
    end
else
    fit_DNAM_2010;
    fitter=findobj('tag','fitter');
    radio_map=findobj(fitter,'tag','radio_map');
    set(radio_map,'value',0);
    set(radio_map,'visible','off');
end
function popupmenu_fitmethod_Callback(hObject, eventdata, handles)
function matlabfitting_SelectionChangeFcn(hObject, eventdata, handles)


% --------------------------------------------------------------------
function Set_Cluster_Parameters_Callback(hObject, eventdata, handles)
DNA_analysis_update_cluster_parameters
load_values;


function load_values()

DNA_analysis=findobj('Tag','DNA_analysis');
pushbutton_autolocate=findobj(DNA_analysis,'Tag','pushbutton_autolocate');
data=get(pushbutton_autolocate,'Userdata');

update_cluster_parameters=findobj('Tag','update_cluster_parameters');

edit_stretch_xaxis=findobj(update_cluster_parameters,'Tag','edit_stretch_xaxis');
edit_density=findobj(update_cluster_parameters,'Tag','edit_density');
edit_distance=findobj(update_cluster_parameters,'Tag','edit_distance');
edit_iterations=findobj(update_cluster_parameters,'Tag','edit_iterations');
edit_thDist=findobj(update_cluster_parameters,'Tag','edit_thDist');
edit_thInlrRatio=findobj(update_cluster_parameters,'Tag','edit_thInlrRatio');
edit_min_distance_fromline=findobj(update_cluster_parameters,'Tag','edit_min_distance_fromline');
edit_min_molecule_length=findobj(update_cluster_parameters,'Tag','edit_min_molecule_length');
edit_angle_deviation=findobj(update_cluster_parameters,'Tag','edit_angle_deviation');


set(edit_stretch_xaxis,'string',data.stretch_xaxis);
set(edit_density,'string',data.density);
set(edit_distance,'string',data.distance);
set(edit_iterations,'string',data.iterations);
set(edit_thDist,'string',data.thDist);
set(edit_thInlrRatio,'string',data.thInlrRatio);
set(edit_min_distance_fromline,'string',data.min_distance_fromline);
set(edit_min_molecule_length,'string',data.min_molecule_length);
set(edit_angle_deviation,'string',data.angle_deviation);


%Start SW (Smith-Waterman) alignment
function SWfitter_menu_Callback(hObject, eventdata, handles)
DNA_analysis=findobj('Tag', 'DNA_analysis');
listboxMlocation=findobj(DNA_analysis,'tag','listboxMlocation');
Mlocation_data=get(listboxMlocation,'Userdata');

if size(Mlocation_data,1)>0 %sum(strcmp('Mlocation_data',Mloc)) &&
    if sum(strcmp(fieldnames(Mlocation_data),'Mloc'))
        DNA_SWfitter;
        SWfitter=findobj('Tag','SWfitter');
        data=[];
        for i=1:size(Mlocation_data.Mloc,2)
            data(i).Mlocation=Mlocation_data.Mloc(i).Mlocations;
        end
        set(SWfitter,'Userdata',data);
    else
        msgbox('Load some molecules first')        
    end
end


%Start single SW (Smith-Waterman) alignment
% --------------------------------------------------------------------
function single_SW_alignment_Callback(hObject, eventdata, handles)
DNA_SWfitter;
DNA_analysis=findobj('Tag', 'DNA_analysis');
listboxMlocation=findobj(DNA_analysis,'tag','listboxMlocation');
Mlocation_data=get(listboxMlocation,'Userdata');
i=get(listboxMlocation,'value');

if i>0
    Data=[];
    Data.Mlocation=Mlocation_data.Mloc(i).Mlocations;
    SWfitter=findobj('Tag','SWfitter');
    set(SWfitter,'Userdata',Data);
end
